//
//  UCIXECSP.h
//  
//
//  Created by Will Kyle on 10/15/21.
//

//#include "UCIXECSP.h"
#ifndef UCIXECSP_h
#define UCIXECSP_h

class UCIXECSP{
public:
   Double_t fRangeInU;
   Double_t fRangeInV;
   Double_t fMinW;
   Double_t fMaxW;
   Double_t fMaxPoslfitReducedChiSq;
   Double_t fMinEGamma;
   Double_t fMaxEGamma;
   Double_t fMaxPMSolidAngleRatio;
   Double_t fMinPMSolidAngleRatio;
   Double_t fMinPMSolidAngle;
   Double_t fMaxIncidenceAngleOnPM;
   Double_t fMaxUVEventDistanceFromPM;
   Double_t fMaxPMSeperation;
   
   UCIXECSP(){};
   void SetCourseSelectionCriteria();
   void SetFineSelectionCriteria();
   //Bool_t IsEventWorthAnalysing(EventData* recData);
   //Bool_t IsPMWorthAnalysing(PMEventData* PMinQuestion);
   //Bool_t ShouldPMBeSaved(PMEventData* PMBeingChecked, PMEventData* anotherPM);
   
};

void UCIXECSP::SetCourseSelectionCriteria(){
   fRangeInU                  = 30;
   fRangeInV                  = 70;
   fMinW                      = 3;
   fMaxW                      = 16;
   fMaxPoslfitReducedChiSq    = 30;
   fMinEGamma                 = 0.036;
   fMaxEGamma                 = 0.055;
   fMaxPMSolidAngleRatio      = 1.1;
   fMinPMSolidAngleRatio      = 1./fMaxPMSolidAngleRatio;
   fMinPMSolidAngle           = 2.e-4;
   fMaxIncidenceAngleOnPM     = TMath::Pi()/2.;
   fMaxUVEventDistanceFromPM  = 12;
   fMaxPMSeperation           = 20;
}

void UCIXECSP::SetFineSelectionCriteria(){
   fRangeInU                  = 30;
   fRangeInV                  = 70;
   fMinW                      = 3;
   fMaxW                      = 16;
   fMaxPoslfitReducedChiSq    = 10;
   fMinEGamma                 = 0.036;
   fMaxEGamma                 = 0.055;
   fMaxPMSolidAngleRatio      = 1.1;
   fMinPMSolidAngleRatio      = 1./fMaxPMSolidAngleRatio;
   fMinPMSolidAngle           = 2.e-4;
   fMaxIncidenceAngleOnPM     = TMath::Pi()/4.;
   fMaxUVEventDistanceFromPM  = 12;
   fMaxPMSeperation           = 20;
}

/*
Bool_t UCIXECSP::IsEventWorthAnalysing(EventData* recData)   {
   if (recData->fEGamma<fMinEGamma || recData->fEGamma>fMaxEGamma)   return kFALSE;
   if (abs(recData->fFitUVW[0])>fRangeInU || abs(recData->fFitUVW[1])>fRangeInV) return kFALSE;
   if (recData->fFitUVW[2]<fMinW || recData->fFitUVW[2]>fMaxW) return kFALSE;
   if (recData->fPositionReducedChiSq[0]>fMaxPoslfitReducedChiSq) return kFALSE;
   if (recData->fPositionReducedChiSq[1]>fMaxPoslfitReducedChiSq) return kFALSE;
   
   return kTRUE;
}

Bool_t UCIXECSP::IsPMWorthAnalysing(PMEventData* PMinQuestion)  {
   if (PMinQuestion->fSolidAngle<fMinPMSolidAngle) return kFALSE;
   if (PMinQuestion->fIncidenceAngle>fMaxIncidenceAngleOnPM)   return kFALSE;
   
   return kTRUE;
}

Bool_t UCIXECSP::ShouldPMBeSaved(PMEventData* PMBeingChecked, PMEventData* anotherPM)   {
   if (sqrt(pow(PMBeingChecked->fUVW[0] -anotherPM->fUVW[0],2)+pow(PMBeingChecked->fUVW[1] -anotherPM->fUVW[1],2))>fMaxPMSeperation) return kFALSE;
   Double_t solidAngleRatioBetweenPMs = PMBeingChecked->fSolidAngle / anotherPM->fSolidAngle;
   if (solidAngleRatioBetweenPMs > fMaxPMSolidAngleRatio) return kFALSE;
   if (solidAngleRatioBetweenPMs < fMinPMSolidAngleRatio) return kFALSE;
   
   return kTRUE;
}
 */

#endif /* UCIXECSP_h */
