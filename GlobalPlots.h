//
//  GlobalPlots.h
//  
//
//  Created by Will Kyle on 12/15/21.
//

#ifndef GlobalPlots_h
#define GlobalPlots_h

class GlobalPlots
{
public:
   TH1D* hMinEnergy;
   TH1D* hMEGTriggerMask;
   TH1D* hURange;
   TH1D* hVRange;
   TH2D* hUVRange;
   TH1D* hWRange;
   TH1D* hShowerTime;
   TH1D* hIncidenceAngle;
   TH1D* hPMEnergy;
   TH1D* hNpho;
   TH1D* hNphe;
   TH1D* hSolidAngle;
   TH1D* hEventDistance;
   TH1D* hCFtimeFromShower;
   TH1D* hChiSqU;
   TH1D* hChiSqV;
   TH2D* hChiSq;
   
   TH2D* hSimilarAngle;
   TH2D* hSimilarSolidAngle;
   TH2D* hSimilarTime;
   TH2D* hSimilarDistance;
   
   TGraphErrors* hPredictedEGraph;
   
   
   TH1D* hSolidAngleRatio;
   TH2D* hSARvsSignalRatio;
   
   GlobalPlots(){};
   
   void InitializeHistograms(Int_t preOrPostSelection){
      hMinEnergy = new TH1D(Form("egamma%i",preOrPostSelection),"Egamma pre selection; E_{#gamma} [MeV]",400,30,70);
      hMEGTriggerMask = new TH1D(Form("trg%i",preOrPostSelection),"trg; Trigger Type",30,0,30);
      hURange = new TH1D(Form("Urange%i",preOrPostSelection),"U of events; U [cm]",280,-35,35);
      hVRange = new TH1D(Form("Vrange%i",preOrPostSelection),"V of events; V [cm]",600,-75,75);
      hUVRange= new TH2D(Form("UVrange%i",preOrPostSelection),"UV distribution of events; V [cm];U [cm]",600,-75,75,280,-35,35);
      hWRange = new TH1D(Form("w%i",preOrPostSelection),"w of events;W [cm]",300,0,30);
      hShowerTime = new TH1D(Form("time%i",preOrPostSelection),"event time; t [ns]",2000,-1000,0);
      hIncidenceAngle = new TH1D(Form("incidenceangle%i",preOrPostSelection),"incidence angle of PM; angle [rad]",315,0,TMath::Pi());
      hPMEnergy = new TH1D(Form("pmE%i",preOrPostSelection),"E in PM; E [MeV]",600,0,150);
      hNpho = new TH1D(Form("npho%i",preOrPostSelection),"npho;N_{#gamma}",1000,0,10000);
      hNphe = new TH1D(Form("nphe%i",preOrPostSelection),"nphe;N_{p.e.}",6000,-100,500);
      hSolidAngle = new TH1D(Form("solidangle%i",preOrPostSelection),"PM solid angle; Solid Angle [ster/4#pi]",5000,0,.005);
      hEventDistance = new TH1D(Form("distToEvent%i",preOrPostSelection),"Distance From Shower;Distance [cm]",500,0,50);
      hCFtimeFromShower = new TH1D(Form("timeoffset%i",preOrPostSelection),"CFTime - T_{Reco};CFTime - T_{Reco} [ns]",800,-200,200);
      hChiSqU = new TH1D(Form("chisqu%i",preOrPostSelection),"Reduced Chi Sq of U Position Fit;#chi^{2}",500,0,50);
      hChiSqV = new TH1D(Form("chisqv%i",preOrPostSelection),"Reduced Chi Sq of V Position Fit;#chi^{2}",500,0,50);
      hChiSq = new TH2D(Form("chisq%i",preOrPostSelection),"Reduced Chi Sq of U and V Position Fits;U #chi^{2};V #chi^{2}",500,0,50,500,0,50);
      hSimilarAngle = new TH2D(Form("angleboth%i",preOrPostSelection),"Difference in Angle;angle [rad]; angle [rad]",500,-TMath::Pi()/3,TMath::Pi()/3,500,-TMath::Pi()/3,TMath::Pi()/3);
      hSimilarSolidAngle = new TH2D(Form("solidangleboth%i",preOrPostSelection),"Difference In Solid Angle; SA [ster/4#pi]; SA [ster/4#pi]",1000,0,0.001,1000,0,0.001);
      hSimilarTime = new TH2D(Form("timeboth%i",preOrPostSelection),"Difference In Time; TReco-CFTime [ns];TReco-CFTime [ns]",400,-200,200,400,-200,200);
      hSimilarDistance = new TH2D(Form("distanceboth%i",preOrPostSelection),"Difference In Distance; Distance [cm]; Distance [cm]",500,0,50,500,0,50);
      
      hPredictedEGraph = new TGraphErrors();
      hPredictedEGraph->SetTitle("E_{#Gamma} From PM Signals vs E_{Reco}");
      hPredictedEGraph->GetXaxis()->SetTitle("E_{Reco} [MeV]");
      hPredictedEGraph->GetYaxis()->SetTitle("E_{PM Based} [MeV]");
      hPredictedEGraph->SetMarkerStyle(7);
      hPredictedEGraph->SetLineColor(kRed);
      
      hSolidAngleRatio = new TH1D(Form("solidangleratio%i",preOrPostSelection),"PM solid angle Ratio; Solid Angle Ratio",5000,.2,5);
      hSARvsSignalRatio = new TH2D(Form("solidangleratiosig%i",preOrPostSelection),"PM solid angle Ratio; Solid Angle Ratio; Signal Ratio",5000,.2,5,5000,.2,5);
      
   };
   
   void SavePlots(){
      hMinEnergy->Write();
      hMEGTriggerMask->Write();
      hURange->Write();
      hVRange->Write();
      hUVRange->Write();
      hWRange->Write();
      hShowerTime->Write();
      hIncidenceAngle->Write();
      hPMEnergy->Write();
      hNpho->Write();
      hNphe->Write();
      hSolidAngle->Write();
      hEventDistance->Write();
      hCFtimeFromShower->Write();
      hChiSqU->Write();
      hChiSqV->Write();
      hChiSq->Write();
      hSolidAngleRatio->Write();
      hSimilarTime->Write();
      hSimilarDistance->Write();
      hSimilarAngle->Write();
      hSimilarSolidAngle->Write();
      hSARvsSignalRatio->Write();
      hPredictedEGraph->Write("EPMvsRecoForEvents");
   };
   
};

#endif /* GlobalPlots_h */
