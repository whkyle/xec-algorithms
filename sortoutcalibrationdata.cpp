//
//  sortoutcalibrationdata.cpp
//  
//
//  Created by Will Kyle on 1/26/22.
//


#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"

using namespace std;
const int Nmppc = 4092;         //Total number of MPPC channels in XEC
std::vector<Double_t> energyspread_v;
std::vector<Double_t> energyspreadnc_v;
std::vector<Double_t> enespreadweights_v;
std::vector<Double_t> enespreadweightsnc_v;

void sortoutcalibrationdata(Int_t runNumber) {
   XECTOOLS::InitXECGeometryParameters(64.84, 106.27, 67.03, 96., 125.52, 0, 0, 0);
   gStyle->SetTitleOffset(1,"y");
   gStyle->SetTitleOffset(.9,"x");
   
   TFile* newCalibFile = new TFile("uciCalibrationResultTunedBTGradientDescent.root","READ");
   
   TTree* calresulttree = (TTree*)newCalibFile->Get("response");
   Double_t newgain,newgains[4092]{};
   Int_t gainindex;
   calresulttree->SetBranchAddress("signalsize",&newgain);
   calresulttree->SetBranchAddress("index",&gainindex);
   Int_t firstnewgainentry = calresulttree->GetEntries()-4092;
   for (int ign=0;ign<4092;ign++){
      calresulttree->GetEntry(firstnewgainentry + ign);
      newgains[gainindex] = newgain;
   }
   
   
   TFile* recfile = new TFile(Form("/meg/data1/offline/run/%03ixxx/rec%06i.root", runNumber/1000, runNumber), "READ");
   
   UCIXECSP* Cuts = new UCIXECSP();
   Cuts->SetCourseSelectionCriteria();
   EventData* inputData = new EventData();
   inputData->InitializeEventPlots();
   
   inputData->eventMeasurementArray.resize(200);
   inputData->nphovsPredictedEnergy.resize(200);
   inputData->timeoffsetArray.resize(200);
   inputData->effectiveVelocity.resize(200);
   
   inputData->predictedEneArray.resize(200);
   inputData->predictedEvsdU.resize(200);
   inputData->predictedEvsdV.resize(200);
   inputData->predictedEvsSA.resize(200);
   inputData->predictedEvsDist.resize(200);
   inputData->predictedEvsIncidenceAngle.resize(200);
   inputData->predictedEvsTimeOffset.resize(200);
   
   inputData->eventMeasurementArrayNewCalib.resize(200);
   inputData->nphovsPredictedEnergyNewCalib.resize(200);
   
   inputData->predictedEneArrayNewCalib.resize(200);
   inputData->predictedEvsdUNewCalib.resize(200);
   inputData->predictedEvsdVNewCalib.resize(200);
   inputData->predictedEvsSANewCalib.resize(200);
   inputData->predictedEvsDistNewCalib.resize(200);
   inputData->predictedEvsIncidenceAngleNewCalib.resize(200);
   inputData->predictedEvsTimeOffsetNewCalib.resize(200);
   
   TH1D* hEnergySpread = new TH1D("stdalgedisp", "Percent RMS of Single MPPC Energy Prediction In Events, Standard Algorithm; #sigma_{E from MPPCs}; Entries",400,0,.4);
   TH1D* hEnergySpreadNC = new TH1D("ucialgedisp", "Percent RMS of Single MPPC Energy Prediction In Events, UCI Algorithm; #sigma_{E from MPPCs}; Entries",400,0,.4);
   
   
   for (int iHist=0;iHist<200;iHist++){
      inputData->eventMeasurementArray[iHist] = new TH2D(Form("event%inpho",iHist*10),"Npho distribution;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      //inputData->nphovsPredictedEnergy[iHist] = new TH2D(Form("event%inphovssaprediction",iHist*10),"Predicted Energy vs Reconstructed Energy;Row;Column",93,-.5,92.5,44,-.5,43.5);
      inputData->nphovsPredictedEnergy[iHist] = new TH2D(Form("event%inphovssaprediction",iHist*10),"Predicted Energy vs Reconstructed Energy;Row;Column",50,0,2000,60,20,80);
      inputData->timeoffsetArray[iHist] = new TH2D(Form("event%itime",iHist*10),"TReco - CFTime distribution;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      inputData->effectiveVelocity[iHist] = new TH2D(Form("event%iveff",iHist*10),"Distance To shower divided by shower TReco - cftime;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      
      inputData->predictedEneArray[iHist] = new TH2D(Form("event%iPredictedEne",iHist*10),"Energy Predicted From PMs;Row;Column",93,-.5,92.5,44,-.5,43.5);
      inputData->predictedEvsdU[iHist] = new TH2D(Form("event%iEvsdU",iHist*10),"Energy Predicted Vs dU;dU [cm];E [MeV]",28,-21,21,60,20,80);
      inputData->predictedEvsdV[iHist] = new TH2D(Form("event%iEvsdV",iHist*10),"Energy Predicted Vs dV;dV [cm];E [MeV]",28,-21,21,60,20,80);
      inputData->predictedEvsSA[iHist] = new TH2D(Form("event%iEvsSA",iHist*10),"Energy Predicted Vs SA;SA [ster/4#pi];E [MeV]",100,0,5e-3,60,20,80);
      inputData->predictedEvsDist[iHist] = new TH2D(Form("event%iEvsDist",iHist*10),"Energy Predicted Vs Distance From Shower;Distance [cm];E [MeV]",20,0,20,60,20,80);
      inputData->predictedEvsIncidenceAngle[iHist] = new TH2D(Form("event%iEvsAngle",iHist*10),"Energy Predicted Vs Incidence Angle;Incidence Angle [rad];E [MeV]",10,0,1,60,20,80);
      inputData->predictedEvsTimeOffset[iHist] = new TH2D(Form("event%iEvsTime",iHist*10),"Energy Predicted Vs CFtime;CFtime [ns];E [MeV]",50,-100,-100,60,20,80);
      
      
      inputData->eventMeasurementArrayNewCalib[iHist] = new TH2D(Form("event%inphoNC",iHist*10),"Npho distribution, UCI Calib;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      inputData->nphovsPredictedEnergyNewCalib[iHist] = new TH2D(Form("event%inphovssapredictionNC",iHist*10),"Predicted Energy vs Reconstructed Energy, UCI Calib;Row;Column",50,0,2000,60,20,80);
      inputData->predictedEneArrayNewCalib[iHist] = new TH2D(Form("event%iPredictedEneNC",iHist*10),"Energy Predicted From PMs, UCI Calib;Row;Column",93,-.5,92.5,44,-.5,43.5);
      inputData->predictedEvsdUNewCalib[iHist] = new TH2D(Form("event%iEvsdUNC",iHist*10),"Energy Predicted Vs dU, UCI Calib;dU [cm];E [MeV]",28,-21,21,60,20,80);
      inputData->predictedEvsdVNewCalib[iHist] = new TH2D(Form("event%iEvsdVNC",iHist*10),"Energy Predicted Vs dV, UCI Calib;dV [cm];E [MeV]",28,-21,21,60,20,80);
      inputData->predictedEvsSANewCalib[iHist] = new TH2D(Form("event%iEvsSANC",iHist*10),"Energy Predicted Vs SA, UCI Calib;SA [ster/4#pi];E [MeV]",100,0,5e-3,60,20,80);
      inputData->predictedEvsDistNewCalib[iHist] = new TH2D(Form("event%iEvsDistNC",iHist*10),"Energy Predicted Vs Distance From Shower, UCI Calib;Distance [cm];E [MeV]",20,0,20,60,20,80);
      inputData->predictedEvsIncidenceAngleNewCalib[iHist] = new TH2D(Form("event%iEvsAngleNC",iHist*10),"Energy Predicted Vs Incidence Angle, UCI Calib;Incidence Angle [rad];E [MeV]",10,0,1,60,20,80);
      inputData->predictedEvsTimeOffsetNewCalib[iHist] = new TH2D(Form("event%iEvsTimeNC",iHist*10),"Energy Predicted Vs CFtime, UCI Calib;CFtime [ns];E [MeV]",50,-100,-100,60,20,80);
      
   }
   inputData->pmdatavec.resize(Nmppc);
   TClonesArray* pmrhArray = (TClonesArray*)recfile->Get("XECPMRunHeader");
   
   //Fill Calibration Value Plots
   TH2D* hGain = new TH2D("gainstd","Gains;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hCTAP = new TH2D("ctapstd","CTAP;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hQE = new TH2D("qestd","QE;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hWDGain = new TH2D("wdgainstd","WDGain;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hCE = new TH2D("cestd","CE;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hTotalCal = new TH2D("totalstd","Total Existing Calibration;U [cm];V [cm]",44,-33,33,92,-71,71);
   TH2D* hTotalCalUCI = new TH2D("totaluci","Total UCI Calibration;U [cm];V [cm]",44,-33,33,92,-71,71);
   
   TH1D* hGainLot[4];
   TH1D* hCTAPLot[4];
   TH1D* hQELot[4];
   TH1D* hWDGainLot[4];
   TH1D* hCELot[4];
   TH1D* hTotalCalLot[4];
   TH1D* hTotalCalLotUCI[4];
   for (int iLot=0; iLot<4;iLot++){
      hGainLot[iLot] = new TH1D(Form("gainlotstd%i",iLot),Form("Gains, Lot %i;Gain;Entries",iLot),500,0,1e7);
      hCTAPLot[iLot] = new TH1D(Form("ctaplotstd%i",iLot),Form("CTAP, Lot %i;CTAP;Entries",iLot),50,1,2);
      hQELot[iLot] = new TH1D(Form("qelotstd%i",iLot),Form("QE, Lot %i;QE;Entries",iLot),125,0,0.25);
      hWDGainLot[iLot] = new TH1D(Form("wdgainlotstd%i",iLot),Form("WDGain, Lot %i;WDGain;Entries",iLot),100,0,10);
      hCELot[iLot] = new TH1D(Form("celotstd%i",iLot),Form("CE, Lot %i;CE;Entries",iLot),100,0,2);
      hTotalCalLot[iLot] = new TH1D(Form("totallotstd%i",iLot),Form("Total Existing Calibration, Lot %i;Calibration;Entries",iLot),500,0,1e7);
      hTotalCalLotUCI[iLot] = new TH1D(Form("totallotuci%i",iLot),Form("Total UCI Calibration, Lot %i;Calibration;Entries",iLot),500,0,1e7);
   }
   
   for (Int_t iPM=0; iPM<Nmppc; iPM++){
      PMEventData* currentPM = new PMEventData(iPM);
      currentPM->LoadFromPMRH( (MEGXECPMRunHeader*)(pmrhArray->At(iPM)) );
      inputData->pmdatavec[iPM] = currentPM;
      
      hGain->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fGain);
      hCTAP->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fCTAP);
      hQE->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fQE);
      hCE->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fCE);
      hWDGain->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fWDGain);
      hTotalCal->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fGain*currentPM->fCTAP*currentPM->fQE*currentPM->fCE*currentPM->fWDGain);
      
      hGainLot[currentPM->fProductionLot]->Fill(currentPM->fGain);
      hCTAPLot[currentPM->fProductionLot]->Fill(currentPM->fCTAP);
      hQELot[currentPM->fProductionLot]->Fill(currentPM->fQE);
      hCELot[currentPM->fProductionLot]->Fill(currentPM->fCE);
      hWDGainLot[currentPM->fProductionLot]->Fill(currentPM->fWDGain);
      hTotalCalLot[currentPM->fProductionLot]->Fill(currentPM->fGain*currentPM->fCTAP*currentPM->fQE*currentPM->fCE*currentPM->fWDGain);
      
      if (newgains[currentPM->fChannelNumber]){
         hTotalCalUCI->Fill(currentPM->fUVW[0],currentPM->fUVW[1],currentPM->fGain *currentPM->fCTAP *currentPM->fQE *currentPM->fCE *currentPM->fWDGain *newgains[currentPM->fChannelNumber]);
         hTotalCalLotUCI[currentPM->fProductionLot]->Fill(currentPM->fGain *currentPM->fCTAP *currentPM->fQE *currentPM->fCE *currentPM->fWDGain *newgains[currentPM->fChannelNumber]);
      }
      
   }
   
   TTree* rec = (TTree*)recfile->Get("rec");
   TBranch*    bxecfastrec;
   TBranch*    bxecwfcl;
   TBranch*    bxeccl;
   TBranch*    breco;
   TBranch*    bposlres;
   TBranch*    binfo;
   TBranch*    beventheader;
   TClonesArray*     XECFastRecResult           = new TClonesArray("MEGXECFastRecResult");
   TClonesArray*     XECWaveformAnalysisResult  = new TClonesArray("MEGXECWaveformAnalysisResult");
   TClonesArray*     XECPMCluster               = new TClonesArray("MEGXECPMCluster");
   MEGRecData*       RecoData                   = new MEGRecData();
   TClonesArray*     poslres                    = new TClonesArray("MEGXECPosLocalFitResult");
   ROMETreeInfo*     runinfo                    = new ROMETreeInfo();
   MEGEventHeader*   eventheader                = new MEGEventHeader();
   
   bxecfastrec    = rec->GetBranch("xecfastrec");  // holds fastrec results for photon event
   bxecwfcl       = rec->GetBranch("xecwfcl");     // holds ana results for all the XEC pm channels' waveforms
   bxeccl         = rec->GetBranch("xeccl");       // WFanaRes converted to more physically meaningful quantities
   bposlres       = rec->GetBranch("xecposlfit");  // fitted XEC position results for local region of high signal MPPCs
   binfo          = rec->GetBranch("Info.");
   breco          = rec->GetBranch("reco.");
   beventheader   = rec->GetBranch("eventheader.");
   bxecfastrec    ->SetAddress(&XECFastRecResult);
   bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult);
   bxeccl         ->SetAddress(&XECPMCluster);
   bposlres       ->SetAddress(&poslres);
   binfo          ->SetAddress(&runinfo);
   breco          ->SetAddress(&RecoData);
   beventheader   ->SetAddress(&eventheader);
   
   // Make the output file
   TFile* outputFile = new TFile(Form("/meg/data1/shared/subprojects/xec/Gammamc/ReducedRecData/calibrationData%06i.root",runNumber),"RECREATE");
   TTree* tout = new TTree("tree","sorted data from rec files for UCI MPPC calibrations");
   
   // Npms: number of pms saved for event
   Int_t Npms, EventNumber, ProfitRange, NpmsInPredE;
   Double_t FitUVW[3], FitXYZ[3], PositionReducedChiSq[2], EGamma, TimeFit;
   tout->Branch("Npms",&Npms,"Npms/I");
   tout->Branch("NpmsInPredE",&NpmsInPredE,"NpmsInPredE/I");
   tout->Branch("runNumber",&runNumber,"runNumber/I");
   tout->Branch("event",&EventNumber,"eventNumber/I");
   tout->Branch("uvw",FitUVW,"uvw[3]/D");
   tout->Branch("xyz",FitXYZ,"xyz[3]/D");
   tout->Branch("reducedChiSq",PositionReducedChiSq,"reducedChiSq[2]/D");
   tout->Branch("fitRange",&ProfitRange,"fitRange/I");
   tout->Branch("EGamma",&EGamma,"EGamma/D");
   tout->Branch("TimeFit",&TimeFit,"TimeFit/D");
   
   // PM specific params
   Int_t ChannelNumber[4092], ProductionLot[4092], RowInFace[4092], NumberInRow[4092];
   Double_t Npho[4092], Nphe[4092], Charge[4092], PeakAmplitude[4092], CFtime[4092], PeakTime[4092], LeadingEdgeTime[4092], SolidAngle[4092], IncidenceAngle[4092],
            UDistance[4092], VDistance[4092], UVDistance[4092], DistanceToEvent[4092], PredictedEnergy[4092], WDGain[4092], Gain[4092], QE[4092], CE[4092],
            CTAP[4092], XYZPM[4092][3], UVWPM[4092][3], Direction[4092][3], CableTimeOffset[4092];
   Double_t NphoInner,NphoUnmeasureableAngle;
   tout->Branch("Channel",ChannelNumber,"Channel[Npms]/I");
   tout->Branch("Lot",ProductionLot,"Lot[Npms]/I");
   tout->Branch("Row",RowInFace,"Row[Npms]/I");
   tout->Branch("Column",NumberInRow,"Column[Npms]/I");
   tout->Branch("Npho",Npho,"Npho[Npms]/D");
   tout->Branch("Nphe",Nphe,"Nphe[Npms]/D");
   tout->Branch("Charge",Charge,"Charge[Npms]/D");
   tout->Branch("PeakAmplitude",PeakAmplitude,"PeakAmplitude[Npms]/D");
   tout->Branch("CFtime",CFtime,"CFtime[Npms]/D");
   tout->Branch("PeakTime",PeakTime,"PeakTime[Npms]/D");
   tout->Branch("LeadingEdgeTime",LeadingEdgeTime,"LeadingEdgeTime[Npms]/D");
   tout->Branch("SolidAngle",SolidAngle,"SolidAngle[Npms]/D");
   tout->Branch("IncidenceAngle",IncidenceAngle,"IncidenceAngle[Npms]/D");
   tout->Branch("UDistance",UDistance,"UDistance[Npms]/D");
   tout->Branch("VDistance",VDistance,"VDistance[Npms]/D");
   tout->Branch("UVDistance",UVDistance,"UVDistance[Npms]/D");
   tout->Branch("DistanceToEvent",DistanceToEvent,"DistanceToEvent[Npms]/D");
   tout->Branch("EGammaFromPM",PredictedEnergy,"EGammaFromPM[Npms]/D");
   tout->Branch("WDGain",WDGain,"WDGain[Npms]/D");
   tout->Branch("Gain",Gain,"Gain[Npms]/D");
   tout->Branch("QE",QE,"QE[Npms]/D");
   tout->Branch("CE",CE,"CE[Npms]/D");
   tout->Branch("CTAP",CTAP,"CTAP[Npms]/D");
   tout->Branch("XYZPM",XYZPM,"XYZPM[Npms][3]/D");
   tout->Branch("UVWPM",UVWPM,"UVWPM[Npms][3]/D");
   tout->Branch("PMDirection",Direction,"PMDirection[Npms][3]/D");
   tout->Branch("CableTimeOffset",CableTimeOffset,"CableTimeOffset[Npms]/D");
   
   
   Double_t rmspmenergystandard{};
   Double_t rmspmenergyuci{};
   Double_t meanpmenergystandard{};
   Double_t meanpmenergyuci{};
   Int_t usePMforDiagnostics[4092]{};
   
   tout->Branch("stdrmspmenergies",&rmspmenergystandard,"stdrmspmenergies/D");
   tout->Branch("ucirmspmenergies",&rmspmenergyuci,"ucirmspmenergies/D");
   tout->Branch("stdmeanpmenergies",&meanpmenergystandard,"stdmeanpmenergies/D");
   tout->Branch("ucimeanpmenergies",&meanpmenergyuci,"ucimeanpmenergies/D");
   tout->Branch("usepmineventdiagnostics",usePMforDiagnostics,"usepmineventdiagnostics[Npms]/I");
   
   tout->Branch("ninner",&NphoInner,"ninner/D");
   tout->Branch("indirectnpho",&NphoUnmeasureableAngle,"indirectnpho/D");
   
   Int_t nEvents = rec->GetEntries();
   for (Int_t iEvent=0; iEvent<nEvents; iEvent++){
      if (iEvent%100==0){
         cout << "On Event " << iEvent << endl;
      }
      Npms=0;
      NpmsInPredE=0;
      Bool_t saveThisPM[4092]{};
      for (int iPM=0; iPM<4092; iPM++){
         saveThisPM[iPM] = 0;
      }
      EventNumber = iEvent;
      rec->GetEntry(iEvent);
      beventheader->GetEntry(iEvent);
      Int_t triggerType = eventheader->Getmask();
      if (triggerType)  continue; // 0 == MEG trigger; skip other triggers
      bxecwfcl->GetEntry(iEvent);
      bxeccl->GetEntry(iEvent);
      bposlres->GetEntry(iEvent);
      binfo->GetEntry(iEvent);
      breco->GetEntry(iEvent);
      beventheader->GetEntry(iEvent);
      
      energyspread_v.clear();
      energyspreadnc_v.clear();
      enespreadweights_v.clear();
      enespreadweightsnc_v.clear();
      NphoInner=0;
      NphoUnmeasureableAngle=0;
      
      inputData->GetReconstructedQuantities(RecoData);
      inputData->fProfitRange = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->Getprofitrangeused();
      if (inputData->fProfitRange<0)   continue;
      inputData->fPositionReducedChiSq[0] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(inputData->fProfitRange,0);
      inputData->fPositionReducedChiSq[1] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(inputData->fProfitRange,1);
      inputData->fFitUVW[0] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,0);
      inputData->fFitUVW[1] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,1);
      inputData->fFitUVW[2] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,2);
      inputData->fFitXYZ[0] = XECTOOLS::UVW2X(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      inputData->fFitXYZ[1] = XECTOOLS::UVW2Y(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      inputData->fFitXYZ[2] = XECTOOLS::UVW2Z(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      
      if (!Cuts->IsEventWorthAnalysing(inputData)) continue;
      
      // Event wide data
      FitUVW[0] = (Double_t) inputData->fFitUVW[0];
      FitUVW[1] = (Double_t) inputData->fFitUVW[1];
      FitUVW[2] = (Double_t) inputData->fFitUVW[2];
      FitXYZ[0] = (Double_t) inputData->fFitXYZ[0];
      FitXYZ[1] = (Double_t) inputData->fFitXYZ[1];
      FitXYZ[2] = (Double_t) inputData->fFitXYZ[2];
      PositionReducedChiSq[0] = inputData->fPositionReducedChiSq[0];
      PositionReducedChiSq[1] = inputData->fPositionReducedChiSq[1];
      ProfitRange = (Int_t) inputData->fProfitRange;
      EGamma = inputData->fEGamma;
      TimeFit = inputData->fTimeFit;
      
      // loops through PMs; fills measured waveform quantities and parameters of PM relative to
      // the reconstructed parameters (e.g. SolidAngle of PM per fitted position of the shower); fills
      // array for which PMs to save data from using cuts that depend upon only a single MPPC.
      for (auto pm : inputData->pmdatavec){
         pm->GetWaveformMeasurements((MEGXECWaveformAnalysisResult*)
                                     (XECWaveformAnalysisResult->At(pm->fChannelNumber)));
         pm->GetConvertedWFQuantities((MEGXECPMCluster*)(XECPMCluster->At(pm->fChannelNumber)));
         pm->FillCalculatedQuantities(inputData->fFitXYZ,inputData->fEGamma);
         
         usePMforDiagnostics[pm->fChannelNumber] = 0;
         NphoInner+=pm->fNpho;
         if (pm->fIncidenceAngle>TMath::Pi()/2.){
            NphoUnmeasureableAngle+=pm->fNpho;
         }
         if ((EGamma/pm->fPredictedEnergy)>0 && (EGamma/pm->fPredictedEnergy)<10 && pm->fNphe>50 && pm->fIncidenceAngle<TMath::Pi()/3. && newgains[pm->fChannelNumber]){
            energyspread_v.push_back((Double_t) pm->fPredictedEnergy*1.e3);
            energyspreadnc_v.push_back((Double_t) pm->fPredictedEnergy*1.e3/newgains[pm->fChannelNumber]);
            enespreadweights_v.push_back((Double_t) pm->fNphe/pow(pm->fPredictedEnergy*1e3,2));
            enespreadweightsnc_v.push_back((Double_t) pm->fNphe/pow(pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],2));
            usePMforDiagnostics[pm->fChannelNumber] = 1;
            NpmsInPredE++;
         }
         if (iEvent%10==0 && iEvent<2000 ){
            int iHist = iEvent/10;
            inputData->eventMeasurementArray[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,pm->fNpho);
            if (pm->fNphe>50){
               inputData->timeoffsetArray[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,( pm->fCFtime - pm->fCableTimeOffset - TimeFit)*1.e9);
               inputData->effectiveVelocity[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,(pm->fDistanceToEvent)/((pm->fCFtime - pm->fCableTimeOffset - TimeFit )*1.e9));
            }
            if ((EGamma/pm->fPredictedEnergy)>0 && (EGamma/pm->fPredictedEnergy)<10 && pm->fNphe>50 && pm->fIncidenceAngle<TMath::Pi()/3. /*&& (pm->fCFtime - pm->fCableTimeOffset)<-590e-9 && (pm->fCFtime - pm->fCableTimeOffset)>-620e-9*/){
               
               
               
               //inputData->nphovsPredictedEnergy[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,( EGamma/pm->fPredictedEnergy));
               inputData->nphovsPredictedEnergy[iHist]->Fill(pm->fNpho,pm->fPredictedEnergy*1e3);
               
               inputData->predictedEneArray[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,pm->fPredictedEnergy*1.e3);
               inputData->predictedEvsdU[iHist]->Fill(pm->fUVW[0]-FitUVW[0],pm->fPredictedEnergy*1e3,pm->fNphe);
               inputData->predictedEvsdV[iHist]->Fill(pm->fUVW[1]-FitUVW[1],pm->fPredictedEnergy*1e3,pm->fNphe);
               inputData->predictedEvsSA[iHist]->Fill(pm->fSolidAngle,pm->fPredictedEnergy*1e3,pm->fNphe);
               inputData->predictedEvsDist[iHist]->Fill(pm->fDistanceToEvent,pm->fPredictedEnergy*1e3,pm->fNphe);
               inputData->predictedEvsIncidenceAngle[iHist]->Fill(pm->fIncidenceAngle,pm->fPredictedEnergy*1e3,pm->fNphe);
               inputData->predictedEvsTimeOffset[iHist]->Fill(( pm->fCFtime - pm->fCableTimeOffset - TimeFit)*1.e9, pm->fPredictedEnergy*1e3,pm->fNphe);
               
               
               if (newgains[pm->fChannelNumber]){
                  
                  inputData->nphovsPredictedEnergyNewCalib[iHist]->Fill(pm->fNpho/newgains[pm->fChannelNumber],pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber]);
                  
                  inputData->predictedEneArrayNewCalib[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,pm->fPredictedEnergy*1.e3/newgains[pm->fChannelNumber]);
                  inputData->predictedEvsdUNewCalib[iHist]->Fill(pm->fUVW[0]-FitUVW[0],pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
                  inputData->predictedEvsdVNewCalib[iHist]->Fill(pm->fUVW[1]-FitUVW[1],pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
                  inputData->predictedEvsSANewCalib[iHist]->Fill(pm->fSolidAngle,pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
                  inputData->predictedEvsDistNewCalib[iHist]->Fill(pm->fDistanceToEvent,pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
                  inputData->predictedEvsIncidenceAngleNewCalib[iHist]->Fill(pm->fIncidenceAngle,pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
                  inputData->predictedEvsTimeOffsetNewCalib[iHist]->Fill(( pm->fCFtime - pm->fCableTimeOffset - TimeFit)*1.e9, pm->fPredictedEnergy*1e3/newgains[pm->fChannelNumber],pm->fNphe);
               }
            }
         }
         
         saveThisPM[pm->fChannelNumber] = Cuts->IsPMWorthAnalysing(pm);
      }
      
      //Double_t enerms = TMath::RMS(energyspread_v.begin(),energyspread_v.end()++);  // ISSUE FOUND WITH TMath RMS: does mean of squares / (N-1)  if end iterator is
                                                                                    // just set to __.end(). added ++ fixed that issue.
      Double_t enemean = TMath::Mean(energyspread_v.begin(),energyspread_v.end()++,enespreadweights_v.begin());
      //Double_t enermsnc = TMath::RMS(energyspreadnc_v.begin(),energyspreadnc_v.end()++);
      Double_t enemeannc = TMath::Mean(energyspreadnc_v.begin(),energyspreadnc_v.end()++,enespreadweightsnc_v.begin());
      
      Double_t enerms=0;
      Double_t enermsnc=0;
      Double_t enermsweight=0;
      Double_t enermsweightnc=0;
      
      for (int iene=0;iene<energyspread_v.size();iene++){
         enerms+=enespreadweights_v[iene] * pow(energyspread_v[iene]-enemean,2);
         enermsweight += enespreadweights_v[iene];
      }
      enerms = sqrt(enerms/enermsweight);
      for (int iene=0;iene<energyspreadnc_v.size();iene++){
         enermsnc+=enespreadweightsnc_v[iene] * pow(energyspreadnc_v[iene]-enemeannc,2);
         enermsweightnc += enespreadweightsnc_v[iene];
      }
      enermsnc = sqrt(enermsnc/enermsweightnc);
      
      if (energyspread_v.size()>1){
         hEnergySpread->Fill(enerms/enemean);
         rmspmenergystandard = enerms/enemean;
         meanpmenergystandard = enemean;
      }
      else {
         rmspmenergystandard = 0;
         meanpmenergystandard = 0;
      }
      if (energyspreadnc_v.size()>1){
         hEnergySpreadNC->Fill(enermsnc/enemeannc);
         rmspmenergyuci = enermsnc/enemeannc;
         meanpmenergyuci = enemeannc;
      }
      else {
         rmspmenergyuci = 0;
         meanpmenergyuci = 0;
      }
      
      for (auto pmToCheck : inputData->pmdatavec){
         if (!saveThisPM[pmToCheck->fChannelNumber])   continue;
         for (auto otherPM : inputData->pmdatavec){
            if (pmToCheck->fChannelNumber == otherPM->fChannelNumber)   continue;
            if (!saveThisPM[otherPM->fChannelNumber])   continue;
            saveThisPM[pmToCheck->fChannelNumber]  = Cuts->ShouldPMBeSaved(pmToCheck, otherPM);
            if (saveThisPM[pmToCheck->fChannelNumber])   {
               Npms++;
               break;
            }
         }
      }
      
      // Fill Global Event Plots
      inputData->FillGlobalEventPlots();
      
      // Fill and save the chosen MPPCs to the output rec file.
      Int_t ixpms =0;
      for (auto pm : inputData->pmdatavec){
         if (saveThisPM[pm->fChannelNumber]){
            
            // Fill PM parameters specific to this event
            Npho[ixpms] = pm->fNpho;
            Nphe[ixpms] = pm->fNphe;
            Charge[ixpms] = pm->fCharge;
            PeakAmplitude[ixpms] = pm->fPeakAmplitude;
            CFtime[ixpms] = pm->fCFtime;
            PeakTime[ixpms] = pm->fPeakTime;
            LeadingEdgeTime[ixpms] = pm->fLeadingEdgeTime;
            SolidAngle[ixpms] = pm->fSolidAngle;
            IncidenceAngle[ixpms] = pm->fIncidenceAngle;
            UDistance[ixpms] = pm->fUDistance;
            VDistance[ixpms] = pm->fVDistance;
            UVDistance[ixpms]= pm->fUVDistance;
            DistanceToEvent[ixpms] = pm->fDistanceToEvent;
            PredictedEnergy[ixpms] = pm->fPredictedEnergy;
            
            // Fill PM parameters general to this run
            ChannelNumber[ixpms] = pm->fChannelNumber;
            ProductionLot[ixpms] = pm->fProductionLot;
            RowInFace[ixpms] = pm->fRowInFace;
            NumberInRow[ixpms] = pm->fNumberInRow;
            WDGain[ixpms] = pm->fWDGain;
            Gain[ixpms] = pm->fGain;
            CE[ixpms] = pm->fCE;
            QE[ixpms] = pm->fQE;
            CTAP[ixpms] = pm ->fCTAP;
            
            for (int iDir=0;iDir<3;iDir++)   {
               XYZPM[ixpms][iDir] = (Double_t) pm->fXYZ[iDir];
               UVWPM[ixpms][iDir] = (Double_t) pm->fUVW[iDir];
               Direction[ixpms][iDir] = (Double_t) pm->fDirection[iDir];
            }
            CableTimeOffset[ixpms] = pm->fCableTimeOffset;
            
            ixpms+=1;
         }
      }
      tout->Fill();
   }
   
   // Write out to file.
   outputFile->cd();
   tout->Write();
   inputData->WriteEventPlots();
   hEnergySpread->Write();
   hEnergySpreadNC->Write();
   hGain->Write();
   hCTAP->Write();
   hQE->Write();
   hCE->Write();
   hWDGain->Write();
   hTotalCal->Write();
   hTotalCalUCI->Write();
   for (int iLot=0;iLot<4;iLot++){
      hGainLot[iLot]->Write();
      hCTAPLot[iLot]->Write();
      hQELot[iLot]->Write();
      hCELot[iLot]->Write();
      hWDGainLot[iLot]->Write();
      hTotalCalLot[iLot]->Write();
      hTotalCalLotUCI[iLot]->Write();
   }
   
   for (int iHist=0;iHist<200;iHist++){
      if (inputData->eventMeasurementArray[iHist]->GetEntries()){
         inputData->eventMeasurementArray[iHist]->Write();
      }
      if (inputData->nphovsPredictedEnergy[iHist]->GetEntries()){
         inputData->nphovsPredictedEnergy[iHist]->Write();
      }
      if (inputData->timeoffsetArray[iHist]->GetEntries()){
         inputData->timeoffsetArray[iHist]->Write();
      }
      if (inputData->effectiveVelocity[iHist]->GetEntries()){
         inputData->effectiveVelocity[iHist]->Write();
      }
      
      if (inputData->predictedEneArray[iHist]->GetEntries()){
         inputData->predictedEneArray[iHist]->Write();
      }
      if (inputData->predictedEvsdU[iHist]->GetEntries()){
         inputData->predictedEvsdU[iHist]->Write();
      }
      if (inputData->predictedEvsdV[iHist]->GetEntries()){
         inputData->predictedEvsdV[iHist]->Write();
      }
      if (inputData->predictedEvsSA[iHist]->GetEntries()){
         inputData->predictedEvsSA[iHist]->Write();
      }
      if (inputData->predictedEvsDist[iHist]->GetEntries()){
         inputData->predictedEvsDist[iHist]->Write();
      }
      if (inputData->predictedEvsIncidenceAngle[iHist]->GetEntries()){
         inputData->predictedEvsIncidenceAngle[iHist]->Write();
      }
      if (inputData->predictedEvsTimeOffset[iHist]->GetEntries()){
         inputData->predictedEvsTimeOffset[iHist]->Write();
      }
      
      /*
      if (inputData->eventMeasurementArrayNewCalib[iHist]->GetEntries()){
         inputData->eventMeasurementArrayNewCalib[iHist]->Write();
      }
       */
      if (inputData->nphovsPredictedEnergyNewCalib[iHist]->GetEntries()){
         inputData->nphovsPredictedEnergyNewCalib[iHist]->Write();
      }
      /*
      if (inputData->timeoffsetArrayNewCalib[iHist]->GetEntries()){
         inputData->timeoffsetArrayNewCalib[iHist]->Write();
      }
      if (inputData->effectiveVelocityNewCalib[iHist]->GetEntries()){
         inputData->effectiveVelocityNewCalib[iHist]->Write();
      }*/
      
      if (inputData->predictedEneArrayNewCalib[iHist]->GetEntries()){
         inputData->predictedEneArrayNewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsdUNewCalib[iHist]->GetEntries()){
         inputData->predictedEvsdUNewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsdVNewCalib[iHist]->GetEntries()){
         inputData->predictedEvsdVNewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsSANewCalib[iHist]->GetEntries()){
         inputData->predictedEvsSANewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsDistNewCalib[iHist]->GetEntries()){
         inputData->predictedEvsDistNewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsIncidenceAngleNewCalib[iHist]->GetEntries()){
         inputData->predictedEvsIncidenceAngleNewCalib[iHist]->Write();
      }
      if (inputData->predictedEvsTimeOffsetNewCalib[iHist]->GetEntries()){
         inputData->predictedEvsTimeOffsetNewCalib[iHist]->Write();
      }
      
   }
   outputFile->Close();
   
   
   
   
   return;
}



