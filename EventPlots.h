//
//  EventPlots.h
//  
//
//  Created by Will Kyle on 12/8/21.
//

#include "PMPlots.h"
#ifndef EventPlots_h
#define EventPlots_h

class EventPlots
{
public:
   TH2D* arrayOfNpho;
   
   std::vector<PMPlots*> setofPMplots;
   
   EventPlots(){};
   
};

#endif /* EventPlots_h */
