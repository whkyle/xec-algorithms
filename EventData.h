//
//  EventData.h
//  
//
//  Created by Will Kyle on 10/15/21.
//
#include "PMEventData.h"
#ifndef EventData_h
#define EventData_h

class EventData
{
public:
   TVector3 fFitUVW;
   TVector3 fFitXYZ;
   Double_t fPositionReducedChiSq[2];
   Short_t  fProfitRange;
   Double_t fEGamma;
   Double_t fTimeFit;
   Int_t fTriggerType;
   
   std::vector<PMEventData*> pmdatavec;
   
   TH1D* eventTimes;
   TH1D* eventEnergies;
   TProfile* predictedEvsIncAngle;
   TProfile* predictedEvsSolidAngle;
   TProfile* predictedEvsDepth;
   TProfile* predictedEvsU;
   TProfile* predictedEvsV;
   
   std::vector<TH2D*> eventMeasurementArray;
   std::vector<TH2D*> nphovsPredictedEnergy;
   std::vector<TH2D*> timeoffsetArray;
   std::vector<TH2D*> effectiveVelocity;
   
   std::vector<TH2D*> predictedEneArray;
   std::vector<TH2D*> predictedEvsdU;
   std::vector<TH2D*> predictedEvsdV;
   std::vector<TH2D*> predictedEvsSA;
   std::vector<TH2D*> predictedEvsDist;
   std::vector<TH2D*> predictedEvsIncidenceAngle;
   std::vector<TH2D*> predictedEvsTimeOffset;
   
   std::vector<TH2D*> eventMeasurementArrayNewCalib;
   std::vector<TH2D*> nphovsPredictedEnergyNewCalib;
   std::vector<TH2D*> timeoffsetArrayNewCalib;
   std::vector<TH2D*> effectiveVelocityNewCalib;
   
   std::vector<TH2D*> predictedEneArrayNewCalib;
   std::vector<TH2D*> predictedEvsdUNewCalib;
   std::vector<TH2D*> predictedEvsdVNewCalib;
   std::vector<TH2D*> predictedEvsSANewCalib;
   std::vector<TH2D*> predictedEvsDistNewCalib;
   std::vector<TH2D*> predictedEvsIncidenceAngleNewCalib;
   std::vector<TH2D*> predictedEvsTimeOffsetNewCalib;
   
   EventData(){};
   
   void GetReconstructedQuantities(MEGRecData* Reco)  {
      fEGamma = Reco->GetEGammaAt(0);
      fFitXYZ[0] = Reco->GetXGammaAt(0);
      fFitXYZ[1] = Reco->GetYGammaAt(0);
      fFitXYZ[2] = Reco->GetZGammaAt(0);
      fFitUVW[0] = Reco->GetUGammaAt(0);
      fFitUVW[1] = Reco->GetVGammaAt(0);
      fFitUVW[2] = Reco->GetWGammaAt(0);
      fTimeFit   = Reco->GetTGammaAt(0);
   };
   
   void InitializeEventPlots(){
      eventTimes = new TH1D("eventTimes","Reconstructed Event Time;Time [ns]",1000,-1000,0);
      eventEnergies = new TH1D("eventEnergy","Reconstructed Event Energy; Energy [MeV]",300,30,60);
      predictedEvsIncAngle = new TProfile("pmEpredictionVsIncAngle","E Gamma From PM Measurement Vs Incidence Angle; Incidence Angle [rad]; E_{Reco}/E_{PM}",100,0,TMath::Pi()/2.);
      predictedEvsSolidAngle = new TProfile("pmEpredictionVsSolidAngle","E Gamma From PM Measurement Vs Solid Angle; Solid Angle [1/4#pi ster]; E_{Reco}/E_{PM}",500,0,5e-3);
      predictedEvsDepth = new TProfile("pmEpredictionVsDepth","E Gamma From PM Measurement Vs Depth;Depth [cm];E_{PM}/E_{Reco}",30,0,15);
      predictedEvsU = new TProfile("pmEpredictionVsU","E Gamma From PM Measurement Vs U;U [cm];E_{Reco}/E_{PM}",70,-35,35);
      predictedEvsV = new TProfile("pmEpredictionVsV","E Gamma From PM Measurement Vs V;V [cm];E_{Reco}/E_{PM}",150,-75,75);
   };
   
   void FillEventPlots(PMEventData* pm){
      predictedEvsIncAngle->Fill(pm->fIncidenceAngle,fEGamma/pm->fPredictedEnergy);
      predictedEvsSolidAngle->Fill(pm->fSolidAngle,fEGamma/pm->fPredictedEnergy);
      predictedEvsDepth->Fill(fFitUVW[2],fEGamma/pm->fPredictedEnergy);
      predictedEvsU->Fill(fFitUVW[0],fEGamma/pm->fPredictedEnergy);
      predictedEvsV->Fill(fFitUVW[1],fEGamma/pm->fPredictedEnergy);
   };
   
   void FillGlobalEventPlots(){
      eventTimes->Fill(fTimeFit*1.e9);
      eventEnergies->Fill(fEGamma*1.e3);
   };
   
   void WriteEventPlots(){
      eventTimes->Write();
      eventEnergies->Write();
      predictedEvsIncAngle->Write();
      predictedEvsSolidAngle->Write();
      predictedEvsDepth->Write();
      predictedEvsU->Write();
      predictedEvsV->Write();
   };
   
};
/*
void GetReconstructedQuantities(MEGRecData* Reco)  {
   fEGamma = Reco->GetEGamma();
   fFitXYZ[0] = Reco->GetXGamma();
   fFitXYZ[1] = Reco->GetYGamma();
   fFitXYZ[2] = Reco->GetZGamma();
   fFitUVW[0] = Reco->GetUGamma();
   fFitUVW[1] = Reco->GetVGamma();
   fFitUVW[2] = Reco->GetWGamma();
   fTimeFit   = Reco->GetTGamma();
}
*/
#endif /* EventData_h */
