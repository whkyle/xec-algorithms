//
//  PMEventData.h
//  
//
//  Created by Will Kyle on 10/15/21.
//
#include "UCIPMdata.h"
#include "CalibrationFunctions.h"
#ifndef PMEventData_h
#define PMEventData_h

class PMEventData: public UCIPMdata
{
public:
   Double_t fNpho;
   Double_t fNphe;
   Double_t fCharge;
   Double_t fPeakAmplitude;
   Double_t fCFtime;
   Double_t fPeakTime;
   Double_t fLeadingEdgeTime;
   Double_t fSolidAngle;
   Double_t fIncidenceAngle;
   Double_t fUDistance;
   Double_t fVDistance;
   Double_t fUVDistance;
   Double_t fDistanceToEvent;
   Double_t fPredictedEnergy;
   Double_t fPredictedEnergyUncertainty;
   
   TH1D* incAngles;
   TH1D* predEnergies;
   TH1D* nphoPlot;
   TH1D* chargeAmpRatio;
   
   
   PMEventData(){};
   PMEventData(Int_t channel){fChannelNumber = channel;};
   void GetWaveformMeasurements(MEGXECWaveformAnalysisResult* PMWaveformAnalysisResult)   {
      fCharge           = PMWaveformAnalysisResult->GetchargeAt(0);
      fPeakAmplitude    = PMWaveformAnalysisResult->GetheightAt(0);
      fPeakTime         = PMWaveformAnalysisResult->GetpktimeAt(0);
      fLeadingEdgeTime  = PMWaveformAnalysisResult->GetletimeAt(0);
      fCFtime           = PMWaveformAnalysisResult->GetcftimeAt(0);
   };
   void GetConvertedWFQuantities(MEGXECPMCluster* PMCluster)   {
      fNpho = PMCluster->GetnphoAt(0);
      fNphe = PMCluster->GetnpheAt(0);
   };
   
   void FillCalculatedQuantities(TVector3 showerLocation, Double_t GammaEnergy)   {
      fSolidAngle = CalibrationFunctions::MPPCSolidAngle(showerLocation, fXYZ, -fDirection);
      fIncidenceAngle   = CalibrationFunctions::IncidenceAngle(showerLocation, fXYZ, fDirection);
      fPredictedEnergy = 24.e-9 * fNpho / fSolidAngle;
      fPredictedEnergyUncertainty = fPredictedEnergy/sqrt(fNphe);
      fDistanceToEvent = sqrt(pow(fXYZ[0]-showerLocation[0],2)+pow(fXYZ[1]-showerLocation[1],2)+pow(fXYZ[2]-showerLocation[2],2));
   };
};
/*
void GetWaveformMeasurements(MEGXECWaveformAnalysisResult* PMWaveformAnalysisResult)   {
   fCharge           = PMWaveformAnalysisResult->GetchargeAt(0);
   fPeakAmplitude    = PMWaveformAnalysisResult->GetheightAt(0);
   fPeakTime         = PMWaveformAnalysisResult->GetpktimeAt(0);
   fLeadingEdgeTime  = PMWaveformAnalysisResult->GetletimeAt(0);
   fCFtime           = PMWaveformAnalysisResult->GetcftimeAt(0);
}

void GetConvertedWFQuantities(MEGXECPMCluster* PMCluster)   {
   fNpho = PMCluster->GetnphoAt(0);
   fNphe = PMCluster->GetnpheAt(0);
}

void FillCalculatedQuantities(TVector3 showerLocation)   {
   fSolidAngle = CalibrationFunctions::MPPCSolidAngle(showerLocation, fXYZ, -fDirection);
   fIncidenceAngle   = CalibrationFunctions::IncidenceAngle(showerLocation, fXYZ, fDirection);
};
*/
#endif /* PMEventData_h */
