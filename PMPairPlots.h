//
//  PMPairPlots.h
//  
//
//  Created by Will Kyle on 12/17/21.
//

#ifndef PMPairPlots_h
#define PMPairPlots_h
class PMPairPlots
{
public:
   Int_t fChannelNumber;
   Int_t fColumn;
   Int_t fRow;
   std::vector<TH2D*> hSimilarAngle;
   std::vector<TH2D*> hSimilarSolidAngle;
   std::vector<TH2D*> hSimilarTime;
   std::vector<TH2D*> hSimilarDistance;
   
   PMPairPlots(){};
   PMPairPlots(Int_t channel){
      fChannelNumber = channel;
      fColumn = channel%44;
      fRow = channel/44;
      hSimilarAngle.resize(81);
      hSimilarSolidAngle.resize(81);
      hSimilarTime.resize(81);
      hSimilarDistance.resize(81);
   };
   
   void InitializeHists(){
      Int_t arrayIndex = 0;
      for (int iRow=fRow-4;iRow<=fRow+4;iRow++){
         for (int iColumn=fColumn-4;iColumn<=fColumn+4;iColumn++){
            hSimilarAngle[arrayIndex] = new TH2D(Form("angle%ivs%i",fChannelNumber,44*iRow+iColumn),"Difference in Angle;angle [rad]; angle [rad]",500,-TMath::Pi/3,TMath::Pi()/3,500,-TMath::Pi/3,TMath::Pi()/3);
            hSimilarSolidAngle[arrayIndex] = new TH2D(Form("solidangle%ivs%i",fChannelNumber,44*iRow+iColumn),"Difference In Solid Angle; SA [ster/4#pi]; SA [ster/4#pi]",1000,0,0.001,1000,0,0.001);
            hSimilarTime[arrayIndex] = new TH2D(Form("time%ivs%i",fChannelNumber,44*iRow+iColumn),"Difference In Time; TReco-CFTime [ns];TReco-CFTime [ns]",400,-200,200,400,-200,200);
            hSimilarDistance[arrayIndex] = new TH2D(Form("distance%ivs%i",fChannelNumber,44*iRow+iColumn),"Difference In Distance; Distance [cm]; Distance [cm]",500,0,50,500,0,50);

            arrayIndex++;
         }
      }
   };
   
   void SaveHists(){
      
   };
   
};

#endif /* PMPairPlots_h */
