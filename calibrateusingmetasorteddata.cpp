//
//  calibrateusingmetasorteddata.cpp
//  
//
//  Created by Will Kyle on 11/12/21.
//

#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"
#include "UCIPMCalibration.h"
#include "UCICalibration.h"


void calibrateusingmetasorteddata() {
   
   // Open and setup file which contains the pairwise measurements
   TFile* pairwiseMeasurementFile = new TFile("metaSortedCalibrationData.root","READ");
   
   TTree* pairs = (TTree*)pairwiseMeasurementFile->Get("pairs");
   
   Int_t numeratorPM,denominatorPM;
   Int_t numeratorRow,numeratorColumn;
   Double_t PairMean,PairVariance;
   Int_t PairNmeasurements;
   
   pairs->SetBranchAddress("numeratorIndex",&numeratorPM);
   pairs->SetBranchAddress("denominatorIndex",&denominatorPM);
   pairs->SetBranchAddress("numeratorRow",&numeratorRow);
   pairs->SetBranchAddress("numeratorColumn",&numeratorColumn);
   pairs->SetBranchAddress("pairwiseMean",&PairMean);
   pairs->SetBranchAddress("pairwiseVariance",&PairVariance);
   pairs->SetBranchAddress("pairwiseN",&PairNmeasurements);
   
   // Make file to hold calibration results
   
   TFile* calibrationResult = new TFile("uciCalibrationResult.root","RECREATE");
   TTree* calibrationTree = new TTree("response","Results of uci calibration algorithm");
   
   Int_t pmChannel,pmRow,pmColumn;
   Double_t pmResponse;
   Int_t regressionIteration=0;
   
   calibrationTree->Branch("iteration",&regressionIteration,"iteration/I");
   calibrationTree->Branch("index",&pmChannel,"index/I");
   calibrationTree->Branch("signalsize",&pmResponse,"signalsize/D");
   calibrationTree->Branch("Row",&pmRow,"Row/I");
   calibrationTree->Branch("Column",&pmColumn,"Column/I");
   
   // Initialize structure which will hold all the calibration related data
   
   UCICalibration* Calibration = new UCICalibration();
   Calibration->InitialiseUCICalibrationSet();
   Calibration->SetOfPMs.resize(4092);
   
   for (Int_t iPM=0;iPM<4092;iPM++){
      UCIPMCalibration* aPM = new UCIPMCalibration(iPM);
      Calibration->SetOfPMs[iPM] = aPM;
   }
   
   // Loop through the entries in the pairwise file to fill the Calibration object
   
   Int_t NtotalPairwiseMeasurements = pairs->GetEntries();
   
   for (int iEntry=0;iEntry<NtotalPairwiseMeasurements;iEntry++)  {
      pairs->GetEntry(iEntry);
      
      Calibration->SetOfPMs[numeratorPM]->FillPairwiseArrays(denominatorPM, PairMean, PairVariance, PairNmeasurements);
      
   }
   
   Calibration->CalculateInitialValues();
   Calibration->CalculateChiSq();
   cout<< "Initialization complete, Chi Sq / NDF is " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   Double_t pastThreeChiSquareValues[3];
   pastThreeChiSquareValues[0] = Calibration->fChiSq;
   pastThreeChiSquareValues[1] = 2*Calibration->fChiSq;
   pastThreeChiSquareValues[2] = Calibration->fChiSq;
   for (int iIter=1;iIter<500;iIter++){
      Calibration->CalculateIteration();
      cout << "Finished Iteration "<<endl;
      Calibration->CalculateChiSq();
      pastThreeChiSquareValues[0] = pastThreeChiSquareValues[1];
      pastThreeChiSquareValues[1] = pastThreeChiSquareValues[2];
      pastThreeChiSquareValues[2] = Calibration->fChiSq;
      cout << iIter <<" iteration complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
      regressionIteration = Calibration->fCalibrationIteration;
      for (auto pm : Calibration->SetOfPMs){
         if (pm->fPMNDF){
            pmResponse = pm->fRelativeResponse;
            pmChannel = pm->fChannelNumber;
            pmRow = pm->fRow;
            pmColumn = pm->fColumn;
            calibrationTree->Fill();
         }
      }
      if (Calibration->fChiSq>pastThreeChiSquareValues[0] && Calibration->fChiSq>pastThreeChiSquareValues[1])  break;
   }
   /*
   Calibration->CalculateIteration();
   Calibration->CalculateChiSq();
   cout << "Two iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   Calibration->CalculateIteration();
   Calibration->CalculateChiSq();
   cout << "Two iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   Calibration->CalculateIteration();
   Calibration->CalculateChiSq();
   cout << "Two iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   Calibration->CalculateIteration();
   Calibration->CalculateChiSq();
   cout << "Two iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
    */
   calibrationResult->cd();
   calibrationTree->Write();
   calibrationResult->Close();
   
}
