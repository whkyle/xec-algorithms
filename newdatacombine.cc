
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
using namespace std;
const int Nmppc = 4092;         //Total number of MPPC channels in XEC

double get_cartesian_distance(double x1, double y1, double z1, double x2, double y2, double z2) {
   double dx = x1 - x2;
   double dy = y1 - y2;
   double dz = z1 - z2;

   return TMath::Sqrt(dx*dx + dy*dy + dz*dz);
}

/*------------------------------------------------------------------------------------------------*/
/*
 Author: Will Kyle
 
 OVERVIEW OF THIS CODE:
 
 Inputs:
   set of rec files output from meganalyzer
 
 Output:
   a root file with a tree for every mppc, each entry in the tree contains data for a single event
 which passed preliminary cuts to be used in calculating a gain ratio between a pair of mppcs.
 This includes both existing parameters directly copied out of the rec file (e.g. charge, amplitude)
 as well as converted parameters calculated from information within the rec file (e.g. incidence
 angle and solid angle of an MPPC viewed for light originating at the reconstructed event position)
 
 
*/
/*------------------------------------------------------------------------------------------------*/

// Declare supplemental functions that are defined at the end of this file
LongDouble_t SeparationU(LongDouble_t uf, LongDouble_t ui);
LongDouble_t SeparationV(LongDouble_t vf, LongDouble_t vi);
LongDouble_t SeparationOnUV(LongDouble_t ua, LongDouble_t va, LongDouble_t ub, LongDouble_t vb);
LongDouble_t IncidenceAngle(TVector3 view, TVector3 center,TVector3 normal);
LongDouble_t MPPCSolidAngle(TVector3 view, TVector3 center,TVector3 normal);

void newdatacombine(int runfirst,int runlast,int eventType,bool getTrueRatios,int recoAcc,int intTime, bool corrPos){
   
   cout<<"Started"<<endl;
   string datadir = "/meg/data1/shared/subprojects/xec/Gammamc";
   TString tdatadir= (string) datadir;
   string indirectory;
   string indirectorygem = Form("%s/barGainRecords",tdatadir.Data());
   TString inpathgem = (string) indirectorygem;
   double acc4out;
   switch (eventType) {
      case 400:
         indirectory = (string) "recSigERMDFull";
         break;
      case 401:
         indirectory = (string) "recSigERMDFull1reanaNoAngZCorr";
         break;
      case 402:
         indirectory = (string) "recSigERMDFull2reanaNoAngZCorr";
         break;
      case 403:
         indirectory = (string) "recSigERMDFull3reanaNoAngZCorr";
         break;
      case 404:
         indirectory = (string) "recSigERMDFull4reanaNoAngZCorr";
         break;
      case 405:
         indirectory = (string) "recSigERMDFull5reanaNoAngZCorr";
         break;
      case 406:
         indirectory = (string) "recSigERMDFull6reanaNoAngZCorr";
         break;
      case 451:
         indirectory = (string) "recSigERMDFull1reana45degZCorr";
         break;
      case 452:
         indirectory = (string) "recSigERMDFull2reana45degZCorr";
         break;
      case 453:
         indirectory = (string) "recSigERMDFull3reana45degZCorr";
         break;
      case 454:
         indirectory = (string) "recSigERMDFull4reana45degZCorr";
         break;
      case 455:
         indirectory = (string) "recSigERMDFull5reana45degZCorr";
         break;
      case 456:
         indirectory = (string) "recSigERMDFull6reana45degZCorr";
         break;
      case 457:
         indirectory = (string) "recSigERMDFull7reana45degZCorr";
         break;
      case 458:
         indirectory = (string) "recSigERMDFull8reana45degZCorr";
         break;
      case 500:
         indirectory = (string) "recSigERMDFullDrawn";
         break;
      case 510:
         indirectory = (string) "recSigERMDFullDrawn1reanaNoAng";
         break;
      case 520:
         indirectory = (string) "recSigERMDFullDrawn2reanaNoAng";
         break;
      case 530:
         indirectory = (string) "recSigERMDFullDrawn3reanaNoAng";
         break;
      case 540:
         indirectory = (string) "recSigERMDFullDrawn4reanaNoAng";
         break;
      case 550:
         indirectory = (string) "recSigERMDFullDrawn5reanaNoAng";
         break;
      case 560:
         indirectory = (string) "recSigERMDFullDrawn6reanaNoAng";
         break;
      case 570:
         indirectory = (string) "recSigERMDFullDrawn7reanaNoAng";
         break;
      case 580:
         indirectory = (string) "recSigERMDFullDrawn8reanaNoAng";
         break;
      case 610:
         indirectory = (string) "recSigERMDFullDrawn1reanaNoAngZCorr";
         break;
      case 620:
         indirectory = (string) "recSigERMDFullDrawn2reanaNoAngZCorr";
         break;
      case 630:
         indirectory = (string) "recSigERMDFullDrawn3reanaNoAngZCorr";
         break;
      case 640:
         indirectory = (string) "recSigERMDFullDrawn4reanaNoAngZCorr";
         break;
      case 650:
         indirectory = (string) "recSigERMDFullDrawn5reanaNoAngZCorr";
         break;
      case 660:
         indirectory = (string) "recSigERMDFullDrawn6reanaNoAngZCorr";
         break;
      case 670:
         indirectory = (string) "recSigERMDFullDrawn7reanaNoAngZCorr";
         break;
      case 680:
         indirectory = (string) "recSigERMDFullDrawn8reanaNoAngZCorr";
         break;
      case 690:
         indirectory = (string) "recSigERMDFullDrawn9reanaNoAngZCorr";
         break;
      case 691:
         indirectory = (string) "recSigERMDFullDrawn10reanaNoAngZCorr";
         break;
      case 692:
         indirectory = (string) "recSigERMDFullDrawn11reanaNoAngZCorr";
         break;
      case 693:
         indirectory = (string) "recSigERMDFullDrawn12reanaNoAngZCorr";
         break;
      case 694:
         indirectory = (string) "recSigERMDFullDrawn13reanaNoAngZCorr";
         break;
      case 601:
         indirectory = (string) "recSigERMDFullDrawn1reanaZCorr";
         break;
      case 602:
         indirectory = (string) "recSigERMDFullDrawn2reanaZCorr";
         break;
      case 603:
         indirectory = (string) "recSigERMDFullDrawn3reanaZCorr";
         break;
      case 604:
         indirectory = (string) "recSigERMDFullDrawn4reanaZCorr";
         break;
      case 605:
         indirectory = (string) "recSigERMDFullDrawn5reanaZCorr";
         break;
      case 606:
         indirectory = (string) "recSigERMDFullDrawn6reanaZCorr";
         break;
      case 607:
         indirectory = (string) "recSigERMDFullDrawn7reanaZCorr";
         break;
      case 608:
         indirectory = (string) "recSigERMDFullDrawn8reanaZCorr";
         break;
      case 501:
         indirectory = (string) "recSigERMDFullDrawn1reana";
         break;
      case 502:
         indirectory = (string) "recSigERMDFullDrawn2reana";
         break;
      case 503:
         indirectory = (string) "recSigERMDFullDrawn3reana";
         break;
      case 504:
         indirectory = (string) "recSigERMDFullDrawn4reana";
         break;
      case 505:
         indirectory = (string) "recSigERMDFullDrawn5reana";
         break;
      case 506:
         indirectory = (string) "recSigERMDFullDrawn6reana";
         break;
      case 507:
         indirectory = (string) "recSigERMDFullDrawn7reana";
         break;
      case 508:
         indirectory = (string) "recSigERMDFullDrawn8reana";
         break;
      case 2290:
         indirectory = (string) "recSigERMDFullDrawnAcc";
         break;
      case 381300:
         indirectory = (string) "pmcalib_PreEng2020/Gamma/recfile";
         tdatadir = "/meg/data1/shared/subprojects/xec";
         inpathgem = "/meg/data1/shared/subprojects/xec";
         break;
      case 2940:
      default:
         indirectory = (string) "recSigERMDFullOptacc";
         break;
   }
   TString intdir= (string) indirectory;
   string inpath = Form("%s/%s",tdatadir.Data(),intdir.Data());
   // TString anainpath = "/home/wolga/Downloads/homework/data/timefitrecfiles/";
   //TString anainpath = "/home/wolga/Downloads/homework/data/CEX2020Rec/CEX2020Rec/";
   TString anainpath = (string) inpath;
   int nruns = runlast-runfirst+1;
   
   
   // DEFINING VALUES TO USE FOR ALL CUTS ON EVENTS
   // Max UV distance of event to MPPC
   LongDouble_t maxUVeventDist,maxPMsepOnUV;
   maxUVeventDist = 12;
   maxPMsepOnUV   = (2*maxUVeventDist)-4;
   // range of reconstructed position variables [cm]
   LongDouble_t Urange,Vrange,Wmin,Wmax;
   Urange= 30;//26.5;
   Vrange= 70; //66;
   Wmin  = 3;
   Wmax  = 16; //16;
   // max Chi^2 for Position Fit
   LongDouble_t X2max;
   X2max = 30;
   // Minimum Reconstructed Energy [GeV]
   LongDouble_t Emin;
   Emin  = 0.03;
   // Cuts on position corrections [cm]
   LongDouble_t maxUcorr,maxVcorr,maxWcorr;
   Bool_t useCorrections,cutOnCorrections;
   useCorrections    =kFALSE;
   if (corrPos)   useCorrections = kTRUE;
   cutOnCorrections  =kFALSE;
   maxUcorr = 5;
   maxVcorr = 5;
   maxWcorr = 5;
   // Cuts on Expected Solid Angle Difference
   LongDouble_t dexpSARmax;
   dexpSARmax  = 1.05;
   // Thresholds on signal statistics for saving events
   Bool_t cutOnSignal;
   Int_t cutMinVar;
   LongDouble_t minNpho,minNphe,minQ,minH,minCut;
   cutMinVar= 5;
   minNpho  = 10;
   minNphe  = 0.5;
   minQ     = 1.e-3;
   minH     = 1.e-3;
   switch (cutMinVar) {
      case 0:
         minCut   =  minNpho;
         cutOnSignal = kTRUE;
         break;
      case 1:
         minCut   = minNphe;
         cutOnSignal = kTRUE;
         break;
      case 2:
         minCut   = minQ;
         cutOnSignal = kTRUE;
         break;
      case 3:
         minCut   = minH;
         cutOnSignal = kTRUE;
         break;
      case 4:
         minCut   = -1.e3;
         cutOnSignal = kFALSE;
      case 5:
      default:
         minCut   = 0.;
         cutOnSignal = kTRUE;
         break;
   }
   // Threshold SA to save event (SA range normalized to 0-1 rather than 0-4Ï€)
   LongDouble_t minSAval;
   minSAval = 2.e-4;
   // Number of measurements required to save an MPPC pair's ratio
   LongDouble_t minMeasToUse;
   minMeasToUse=1;
   // Maximum angle of incidence for either mppc, not used for now, final cut in optPropCorr.C
   LongDouble_t maxIncidenceAngle;
   maxIncidenceAngle = TMath::Pi()/2;
   // END CUT VALUE DEFINITIONS
   
   string outprefix = Form("newmethod%drunsof%d_%d%d_S%d-%.3f_W%.1f_Ac%.2f_Xsq%.2f_SAR%.3f_E%.3f_SA%.3f_dTQ%d_Ang%.2f_",nruns,eventType,
                           (int) useCorrections, (int) cutOnCorrections, (int) cutOnSignal,
                           (double) minCut, (double) Wmin, acc4out, (double) X2max,
                           (double) dexpSARmax, (double) Emin, (double) minSAval*100, intTime,(double) maxIncidenceAngle);
   TString outpprefix= (string) outprefix;
   
   // Get the true gem gains if asked to do so
   char simname[200];
   LongDouble_t simGain[Nmppc];
   LongDouble_t simCE[Nmppc];
   LongDouble_t simQE[Nmppc];
   LongDouble_t simWDGain[Nmppc];
   LongDouble_t simCTAP[Nmppc];
   LongDouble_t avgSimGain =0.;
   LongDouble_t nSimGains  =4092.;
   Bool_t isData = kFALSE;
   if (getTrueRatios){
      string barfilename;
      switch (eventType) {
         case 500:
         case 510:
         case 520:
         case 530:
         case 540:
         case 550:
         case 560:
         case 570:
         case 580:
         case 610:
         case 620:
         case 630:
         case 640:
         case 650:
         case 660:
         case 670:
         case 680:
         case 690:
         case 691:
         case 692:
         case 693:
         case 694:
         case 601:
         case 602:
         case 603:
         case 604:
         case 605:
         case 606:
         case 607:
         case 608:
         case 501:
         case 502:
         case 503:
         case 504:
         case 505:
         case 506:
         case 507:
         case 508:
         case 2290:
            barfilename = "500sim10000.root";
            break;
         case 400:
         case 401:
         case 402:
         case 403:
         case 404:
         case 405:
         case 406:
         case 451:
         case 452:
         case 453:
         case 454:
         case 455:
         case 456:
         case 457:
         case 458:
         case 100:
            barfilename = "100sim10000.root";
            break;
         case 381300:
            barfilename = "pmcalib_PreEng2020/Gamma/recfile/rec381300.root";
            isData = kTRUE;
            break;
         case 2040:
         case 2940:
         default:
            barfilename = "100sim10000.root";
            break;
      }
      TString barfiletname = (string) barfilename;
      sprintf(simname,"%s/%s",inpathgem.Data(),barfiletname.Data());
      TFile* barinfile;
      barinfile = new TFile(simname,"READ");
      TClonesArray* pmvArray;
      if (isData) {
         pmvArray = (TClonesArray*)barinfile->Get("XECPMRunHeader");
      }
      else {
         pmvArray= (TClonesArray*)barinfile->Get("BarXECPMRunHeader");
      }
      MEGBarXECPMRunHeader *pmvb;
      MEGXECPMRunHeader * pmva;
      if (isData){
         for (int iPM=0;iPM<Nmppc;iPM++){
            pmva           =(MEGXECPMRunHeader*)(pmvArray->At(iPM));
            simGain[iPM]   = pmva->GetGain();
            simCE[iPM]     = pmva->GetCE();
            simQE[iPM]     = pmva->GetQE();
            simCTAP[iPM]   = pmva->GetCTAP();
            simWDGain[iPM] = pmva->GetWDGain();
            if (simGain[iPM]>1e-3 && simCE[iPM]>1e-3 && simQE[iPM]>1e-3 && simCTAP[iPM]>1e-3 && simWDGain[iPM]>1e-3){
               avgSimGain   += (LongDouble_t) (simGain[iPM]*simQE[iPM]*simCE[iPM]*simCTAP[iPM]*simWDGain[iPM]);
            }
            else{
               nSimGains -=1;
            }
         }
         avgSimGain/=nSimGains;
         cout<<"Average MPPC Bartender Gain: "<<avgSimGain<<endl;
         barinfile->Close();
      }
      else {
         for (int iPM=0;iPM<Nmppc;iPM++){
            pmvb           =(MEGBarXECPMRunHeader*)(pmvArray->At(iPM));
            simGain[iPM]   = pmvb->GetGain();
            avgSimGain   += (LongDouble_t) (pmvb->GetGain())/nSimGains;
         }
         cout<<"Average MPPC Bartender Gain: "<<avgSimGain<<endl;
         barinfile->Close();
      }
   }
   else {
      for (int iPM=0;iPM<Nmppc;iPM++){
         simGain[iPM]   = 1000000.;
      }
      avgSimGain  = 1000000.;
   }
   
   // Get the initial file for header data
   char name[200];
   sprintf(name,"%s/rec%05d.root",anainpath.Data(),runfirst);
   TFile* recinputfile;
   TTree* rectree;
   recinputfile = new TFile(name,"READ");
   rectree = (TTree*)recinputfile->Get("rec");
   TBranch*    bxecfastrec;
   TBranch*    bxecwfcl;
   TBranch*    bxeccl;
   TBranch*    breco;
   TBranch*    bposlres;
   TBranch*    binfo;
   TClonesArray*  XECFastRecResult           = new TClonesArray("MEGXECFastRecResult");
   TClonesArray*  XECWaveformAnalysisResult  = new TClonesArray("MEGXECWaveformAnalysisResult");
   TClonesArray*  XECPMCluster               = new TClonesArray("MEGXECPMCluster");
   MEGRecData*    plfitr                     = new MEGRecData();
   TClonesArray*  poslres                    = new TClonesArray("MEGXECPosLocalFitResult");
   ROMETreeInfo*  runinfo                    = new ROMETreeInfo();
   bxecfastrec = rectree->GetBranch("xecfastrec");          //holds fastrec results for photon event
   bxecfastrec    ->SetAddress(&XECFastRecResult);
   bxecwfcl    = rectree->GetBranch("xecwfcl");             // holds ana results for all the XEC
   bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult); // pm channels' waveforms
   bxeccl      = rectree->GetBranch("xeccl");               // WFanaRes converted to more physically
   bxeccl         ->SetAddress(&XECPMCluster);              // meaningful quantities
   if (!isData)   {
      breco       = rectree->GetBranch("reco.");               // Final reconstruction variables
      breco          ->SetAddress(&plfitr);
   }
   bposlres    = rectree->GetBranch("xecposlfit");          // fitted XEC position results for local
   bposlres       ->SetAddress(&poslres);                   // region of high signal MPPCs
   binfo       = rectree->GetBranch("Info.");
   binfo          ->SetAddress(&runinfo);
   
   TClonesArray* pmrhArray = (TClonesArray*)recinputfile->Get("XECPMRunHeader");
   MEGXECPMRunHeader *pmrh;
   LongDouble_t pmu[Nmppc],pmv[Nmppc],pmw[Nmppc],pmx[Nmppc],pmy[Nmppc],pmz[Nmppc],pmphi[Nmppc],pmr[Nmppc];
   LongDouble_t pmxyz[Nmppc][3],pmnorm[Nmppc][3];
   LongDouble_t pmcharge[Nmppc],pmheight[Nmppc],pmsaexp[Nmppc],pmang[Nmppc],pmphe[Nmppc],pmpho[Nmppc];
   LongDouble_t pm_cftime[Nmppc], pm_pktime[Nmppc], pm_letime[Nmppc];
   LongDouble_t pm_tpm[Nmppc];
   Int_t pmlot[Nmppc];
   Bool_t pmused[Nmppc];
   // Read and save the xyz,uvw coordinates of all mppcs from the run header
   for (int iPM=0;iPM<Nmppc;iPM++){
      pmrh = (MEGXECPMRunHeader*)(pmrhArray->At(iPM));
      pmu[iPM]=pmrh->GetUVWAt(0);
      pmv[iPM]=pmrh->GetUVWAt(1);
      pmw[iPM]=pmrh->GetUVWAt(2);
      pmxyz[iPM][0]=pmrh->GetXYZAt(0);
      pmxyz[iPM][1]=pmrh->GetXYZAt(1);
      pmxyz[iPM][2]=pmrh->GetXYZAt(2);
      pmx[iPM] = pmxyz[iPM][0];
      pmy[iPM] = pmxyz[iPM][1];
      pmz[iPM] = pmxyz[iPM][2];
      pmnorm[iPM][0]=pmrh->GetDirectionAt(0);
      pmnorm[iPM][1]=pmrh->GetDirectionAt(1);
      pmnorm[iPM][2]=pmrh->GetDirectionAt(2);
      pmlot[iPM] = ((Int_t) pmrh->GetProductionLot())-65;
   }
   
   // File for output
   TFile* ratiofile = new TFile(Form("%sratios.root",outpprefix.Data()),"RECREATE");
   
   // Array of TTrees, each will hold any ratio meas between a particular pair of pms
   //TClonesArray* ratioIndArr  = new TClonesArray("TTree",Nmppc);
   TClonesArray* ratioArr     = new TClonesArray("TTree",Nmppc);
   Int_t ratioInd =0;
   Int_t ratioLotNum =0;
   Int_t ratioLotDen =0;
   
   Float_t ratioVal     =0;
   Float_t ratioValInv  =0;
   Float_t ratioSAexp   =0;
   Float_t ratioQa      =0;
   Float_t ratioQb      =0;
   Float_t ratioSAa     =0;
   Float_t ratioSAb     =0;
   Float_t ratioValH    =0;
   Float_t ratioValHInv =0;
   Float_t ratioHa      =0;
   Float_t ratioHb      =0;
   Float_t ratioeventU    =0;
   Float_t ratioeventV    =0;
   Float_t ratioeventW    =0;
   Float_t ratioeventUunc =0;
   Float_t ratioeventVunc =0;
   Float_t ratioeventWunc =0;
   Float_t ratioeventE  =0;
   Float_t ratioeventUrel  =0;
   Float_t ratioeventVrel  =0;
   Float_t ratioeventWrel  =0;
   Float_t ratioeventUrelb =0;
   Float_t ratioeventVrelb =0;
   Float_t ratioeventWrelb =0;
   Float_t ratioValSAcorr     =0;
   Float_t ratioValInvSAcorr  =0;
   Float_t ratioValHSAcorr    =0;
   Float_t ratioValHInvSAcorr =0;
   Float_t ratioeventX2 =0;
   Float_t ratioGa   =0;
   Float_t ratioGb   =0;
   Float_t ratioGexp =0;
   Float_t ratiocea  =0;
   Float_t ratioqea  =0;
   Float_t ratioctapa=0;
   Float_t ratiowdga =0;
   
   Float_t ratioanglenum   =0;
   Float_t ratioangleden   =0;
   
   Float_t rationphea      =0;
   Float_t rationpheb      =0;
   Float_t rationphoa      =0;
   Float_t rationphob      =0;

   Float_t cftime_diff = 0;
   Float_t pktime_diff = 0;
   Float_t letime_diff = 0;
   Float_t cftime_this = 0;
   Float_t pktime_this = 0;
   Float_t letime_this = 0;
   Float_t cftime_other = 0;
   Float_t pktime_other = 0;
   Float_t letime_other = 0;

   Float_t distance_this_to_event = 0;
   Float_t distance_other_to_event = 0;

   Int_t runnumval   =0;
   Int_t entrynumval =0;
   Int_t eventnumval =0;
   
   Float_t ratioeventUalt  =0;
   Float_t ratioeventValt  =0;
   Float_t ratioeventWalt  =0;
   Float_t ratioeventUuncalt =0;
   Float_t ratioeventVuncalt =0;
   Float_t ratioeventWuncalt =0;
   Float_t ratioeventUrelalt    =0;
   Float_t ratioeventVrelalt    =0;
   Float_t ratioeventUrelbalt   =0;
   Float_t ratioeventVrelbalt   =0;
   Bool_t savedForUncorr = 0;
   Bool_t savedForCorr   = 0;
   
   
   
   // Keeps a running measurement of the final sums needed to quickly get all the required means
   // from our sets of event based parameters
   LongDouble_t   sumRatios[Nmppc];
   LongDouble_t   sumInvRatios[Nmppc];
   LongDouble_t   sumRatiosSAcorr[Nmppc];
   LongDouble_t   sumInvRatiosSAcorr[Nmppc];
   LongDouble_t   sumHRatios[Nmppc];
   LongDouble_t   sumHInvRatios[Nmppc];
   LongDouble_t   sumHRatiosSAcorr[Nmppc];
   LongDouble_t   sumHInvRatiosSAcorr[Nmppc];
   LongDouble_t   sumTrueGainRatios[Nmppc];
   Int_t          nEventsInSum[Nmppc];
   
   LongDouble_t   altsumRatios[Nmppc];
   LongDouble_t   altsumInvRatios[Nmppc];
   LongDouble_t   altsumRatiosSAcorr[Nmppc];
   LongDouble_t   altsumInvRatiosSAcorr[Nmppc];
   LongDouble_t   altsumHRatios[Nmppc];
   LongDouble_t   altsumHInvRatios[Nmppc];
   LongDouble_t   altsumHRatiosSAcorr[Nmppc];
   LongDouble_t   altsumHInvRatiosSAcorr[Nmppc];
   LongDouble_t   altsumTrueGainRatios[Nmppc];
   Int_t          altnEventsInSum[Nmppc];
   
   
   for (int iArr=0;iArr<Nmppc;iArr++){
      new((*ratioArr)[iArr]) TTree(Form("pm%d",iArr),Form("Ratio of MPPC %d to other local MPPCs",iArr));
      ((TTree*)(*ratioArr)[iArr])->Branch("run",&runnumval,"run/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("entry",&entrynumval,"entry/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("event",&eventnumval,"event/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("index",&ratioInd,"index/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("lot",&ratioLotNum,"lot/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("lotden",&ratioLotDen,"lotden/I");
      ((TTree*)(*ratioArr)[iArr])->Branch("ratio",&ratioVal,"ratio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("inverse",&ratioValInv,"inverse/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAratio",&ratioSAexp,"SAratio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAcorrRatio",&ratioValSAcorr,"SAcorrRatio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAcorrInverse",&ratioValInvSAcorr,"SAcorrInverse/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Gratio",&ratioGexp,"Gratio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Gnum",&ratioGa,"Gnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("CEnum",&ratiocea,"CEnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("QEnum",&ratioqea,"QEnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("CTAPnum",&ratioctapa,"CTAPnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("WDGainnum",&ratiowdga,"WDGainnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Gden",&ratioGb,"Gden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("qnum",&ratioQa,"qnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("qden",&ratioQb,"qden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAnum",&ratioSAa,"SAnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAden",&ratioSAb,"SAden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pkratio",&ratioValH,"pkratio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pkinverse",&ratioValHInv,"pkinverse/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAcorrPkRatio",&ratioValHSAcorr,"SAcorrPkRatio/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("SAcorrPkInverse",&ratioValHInvSAcorr,"SAcorrPkInverse/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pknum",&ratioHa,"pknum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pkden",&ratioHb,"pkden/F");
      
      ((TTree*)(*ratioArr)[iArr])->Branch("Nphenum",&rationphea,"Nphenum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Npheden",&rationpheb,"Npheden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Nphonum",&rationphoa,"Nphonum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Nphoden",&rationphob,"Nphoden/F");

      ((TTree*)(*ratioArr)[iArr])->Branch("cftime_diff",&cftime_diff,"cftime_diff/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pktime_diff",&pktime_diff,"pktime_diff/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("letime_diff",&letime_diff,"letime_diff/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("cftime_this",&cftime_this,"cftime_this/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pktime_this",&pktime_this,"pktime_this/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("letime_this",&letime_this,"letime_this/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("cftime_other",&cftime_other,"cftime_other/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("pktime_other",&pktime_other,"pktime_other/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("letime_other",&letime_other,"letime_other/F");

      ((TTree*)(*ratioArr)[iArr])->Branch("distance_this_to_event",&distance_this_to_event,"distance_this_to_event/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("distance_other_to_event",&distance_other_to_event,"distance_other_to_event/F");
      
      ((TTree*)(*ratioArr)[iArr])->Branch("u",&ratioeventU,"u/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("v",&ratioeventV,"v/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("w",&ratioeventW,"w/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("uunc",&ratioeventUunc,"uunc/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("vunc",&ratioeventVunc,"vunc/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("wunc",&ratioeventWunc,"wunc/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("urelnum",&ratioeventUrel,"urelnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("vrelnum",&ratioeventVrel,"vrelnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("wrelnum",&ratioeventWrel,"wrelnum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("urelden",&ratioeventUrelb,"urelden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("vrelden",&ratioeventVrelb,"vrelden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("wrelden",&ratioeventWrelb,"wrelden/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("Egamma",&ratioeventE,"Egamma/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("ChiSq",&ratioeventX2,"ChiSq/F");
      
      ((TTree*)(*ratioArr)[iArr])->Branch("anglenum",&ratioanglenum,"anglenum/F");
      ((TTree*)(*ratioArr)[iArr])->Branch("angleden",&ratioangleden,"angleden/F");
      
      
      nEventsInSum[iArr]         =0;
      sumRatios[iArr]            =0;
      sumInvRatios[iArr]         =0;
      sumRatiosSAcorr[iArr]      =0;
      sumInvRatiosSAcorr[iArr]   =0;
      sumHRatios[iArr]           =0;
      sumHInvRatios[iArr]        =0;
      sumHRatiosSAcorr[iArr]     =0;
      sumHInvRatiosSAcorr[iArr]  =0;
      sumTrueGainRatios[iArr]    =0;
   }
   //cout<<"Max ratios per MPPC:  "<<maxNratios<<endl;
   
   Long64_t nEvent =0;
   nEvent = rectree->GetEntries();
   Int_t nEventsTotal=0;
   Int_t nEventsUsed=0;
   /*Start Loop over recfiles---------------------------------------------------------------------*/
   for (int ifile=0;ifile<nruns;++ifile) {
      if (ifile>0){
         recinputfile->Close();
         //delete recinputfile;
         //delete rectree;
         sprintf(name,"%s/rec%05d.root",anainpath.Data(),runfirst+ifile);
         recinputfile= new TFile(name,"READ");
         if (recinputfile->IsZombie())  continue;
         rectree     = (TTree*)recinputfile->Get("rec");
         if (rectree==NULL)  continue;
         bxecfastrec = rectree->GetBranch("xecfastrec");     //holds fastrec results for photon event
         bxecfastrec    ->SetAddress(&XECFastRecResult);
         bxecwfcl    = rectree->GetBranch("xecwfcl");           // holds data for mppc channels' waveforms
         bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult);
         bxeccl      = rectree->GetBranch("xeccl");
         bxeccl         ->SetAddress(&XECPMCluster);
         if (!isData)   {
            breco       = rectree->GetBranch("reco.");
            breco          ->SetAddress(&plfitr);
         }
         bposlres    = rectree->GetBranch("xecposlfit");
         bposlres       ->SetAddress(&poslres);
         binfo       = rectree->GetBranch("Info.");
         binfo          ->SetAddress(&runinfo);
      }
      cout<<"running file:  "<<runfirst+ifile<<endl;
      nEvent = rectree->GetEntries();
      cout<<nEvent<<" events"<<endl;
      nEventsTotal+= (Int_t) nEvent;

      // -- loop over each event in file, saving data from rec files
      for (int iEvent=0;iEvent<nEvent;iEvent++){
         if ((iEvent+1)%250<1){
            cout<<"Event:    "<<iEvent<<endl;
         }
         bxecfastrec ->GetEntry(iEvent);
         bxecwfcl    ->GetEntry(iEvent);
         if (!isData)   {
            breco       ->GetEntry(iEvent);
         }
         bposlres    ->GetEntry(iEvent);
         bxeccl      ->GetEntry(iEvent);
         binfo       ->GetEntry(iEvent);
         
         eventnumval = (Int_t) runinfo->GetEventNumber();
         
         // --- get reconstructed shower position and position uncertainty
         int lsRegion  = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetLSfitrangeused();
         if(lsRegion < 0) {
            continue;
         }

         LongDouble_t eventlsu = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,0);
         LongDouble_t eventlsv = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,1);
         LongDouble_t eventlsw = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,2);
         
         LongDouble_t eventUunc= ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidUncertAt(lsRegion,0);
         LongDouble_t eventVunc= ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidUncertAt(lsRegion,1);
         LongDouble_t eventWunc= ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidUncertAt(lsRegion,2);
         
         // --- reject events outside a range
         bool urej,vrej,wrej,uvwrej;
         urej  = abs(eventlsu)<Urange;
         vrej  = abs(eventlsv)<Vrange;
         wrej  = (eventlsw>0)&&(eventlsw<Wmax);
         uvwrej= urej && vrej && wrej;
         if (!uvwrej)   continue;
         
         // --- get energy and chi squared of event
         LongDouble_t eventX2 = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetchisqSolidAt(lsRegion);
         LongDouble_t eventE  = .75*((MEGXECFastRecResult*)(XECFastRecResult->At(0)))->Getenergy();  //plfitr->GetEGammaAt(0);

         // --- more rejection
         bool x2rej,Erej,urejcorr,vrejcorr,wrejcorr;
         x2rej    = eventX2<X2max;
         Erej     = eventE>Emin;
         if ((!x2rej)||(!Erej))  continue;
         
         LongDouble_t eventUcorr = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwAt(0); //plfitr->GetUGammaAt(0);
         LongDouble_t eventVcorr = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwAt(1);  //plfitr->GetVGammaAt(0);
         LongDouble_t eventWcorr = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwAt(2);  //plfitr->GetWGammaAt(0);
         urejcorr = cutOnCorrections ? abs(eventlsu-eventUcorr)<maxUcorr : kTRUE;
         vrejcorr = cutOnCorrections ? abs(eventlsv-eventVcorr)<maxVcorr : kTRUE;
         wrejcorr = cutOnCorrections ? abs(eventlsw-eventWcorr)<maxWcorr : kTRUE;
         if ((!urejcorr)||(!vrejcorr)||(!wrejcorr))   continue;
         
         LongDouble_t eventU  = useCorrections ? eventUcorr : eventlsu;
         LongDouble_t eventV  = useCorrections ? eventVcorr : eventlsv;
         LongDouble_t eventW  = useCorrections ? eventWcorr : eventlsw;
         bool wnrej  = (eventW>Wmin);
         if (!wnrej)    continue;
         nEventsUsed+=1;
         
         // R_MPPCs = 64.84 ; + MPPC Thickness = 0.13 ;
         LongDouble_t eventlsx = -1. * (eventlsw + 64.97) * TMath::Cos(eventlsv / 64.97);
         LongDouble_t eventlsy = (eventlsw + 64.97) * TMath::Sin(eventlsv / 64.97);
         LongDouble_t eventX= useCorrections ? ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetxyzAt(0) : eventlsx;
         LongDouble_t eventY= useCorrections ? ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetxyzAt(1) : eventlsy;
         LongDouble_t eventZ= useCorrections ? ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetxyzAt(2) : eventlsu;
         
         TVector3 xyzf(eventX, eventY, eventZ);
         
         // --- get useful info for selecting events to use
         for (int iPM=0;iPM<Nmppc;iPM++){


            Bool_t isCloseToPM;
            isCloseToPM = SeparationOnUV(eventU,eventV,pmu[iPM],pmv[iPM])<maxUVeventDist;
            if (isCloseToPM){

               // -- get charge and peak amplitude recorded on each mppc
               pmcharge[iPM]=(-(((MEGXECWaveformAnalysisResult*)
                                            (XECWaveformAnalysisResult->At(iPM)))->GetchargeAt(0)));
               pmheight[iPM]=(-(((MEGXECWaveformAnalysisResult*)
                                            (XECWaveformAnalysisResult->At(iPM)))->GetheightAt(0)));
               pmphe[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GetnpheAt(0);
               pmpho[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GetnphoAt(0);
               pm_tpm[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GettpmAt(0);

               pm_cftime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                            (XECWaveformAnalysisResult->At(iPM)))->GetcftimeAt(0)));
               pm_pktime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                            (XECWaveformAnalysisResult->At(iPM)))->GetpktimeAt(0)));
               pm_letime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                            (XECWaveformAnalysisResult->At(iPM)))->GetletimeAt(0)));

               // --- calculate angle and solid angle for mppcs
               // useful for timing
               LongDouble_t pmsolidangle,pmincangle;
               TVector3 pmxyzv(pmxyz[iPM][0],pmxyz[iPM][1],pmxyz[iPM][2]);
               TVector3 pmnormv(pmnorm[iPM][0],pmnorm[iPM][1],pmnorm[iPM][2]);
               pmsolidangle   = MPPCSolidAngle(xyzf, pmxyzv, -pmnormv);
               pmincangle  = IncidenceAngle(xyzf, pmxyzv, pmnormv);


               pmsaexp[iPM]=(pmsolidangle);
               pmang[iPM]=pmincangle;
               pmused[iPM]= (kTRUE);

               // -- switch for different measures of charge to select hits
               LongDouble_t measForMin;
               switch (cutMinVar) {
                  case 0:
                     measForMin  = pmpho[iPM];
                     break;
                  case 1:
                     measForMin  = pmphe[iPM];
                     break;
                  case 2:
                     measForMin  = pmcharge[iPM];
                     break;
                  case 3:
                     measForMin  = pmheight[iPM];
                     break;
                  case 4:
                     measForMin  = pmcharge[iPM];
                     break;
                  case 5:
                  default:
                     measForMin  = pmcharge[iPM];
                     break;
               }
               Bool_t badMeasurement;
               badMeasurement =  pmsolidangle<minSAval || pmcharge[iPM]<=0 || pmheight[iPM]<=0 || measForMin<=minCut;
               if (badMeasurement){
                  pmcharge[iPM]=(0);
                  pmheight[iPM]=(0);
                  pmsaexp[iPM]=(0);
                  pmpho[iPM]=(0);
                  pmphe[iPM]=(0);
                  pmused[iPM]=(kFALSE);
               }
            }
            else{
               // pm wasn't close to event, so dont do anything
               pmcharge[iPM]=(0);
               pmheight[iPM]=(0);
               pmsaexp[iPM]=(0);
               pmpho[iPM]=(0);
               pmphe[iPM]=(0);
               pmused[iPM]=(kFALSE);
            }
         } //end loop over PM
         
         //loop over pairs of PMs
         for (int iPM=0;iPM<Nmppc;iPM++){
            if (!pmused[iPM])          continue;
            for (int jPM=0;jPM<Nmppc;jPM++){

               if (iPM==jPM)              continue;
               if (!pmused[jPM])          continue;

               // cut if the pair is too far away
               // would fail angle cut later anyway
               LongDouble_t pmUVdist = SeparationOnUV(pmu[iPM],pmv[iPM],pmu[jPM],pmv[jPM]);
               if (pmUVdist>=maxPMsepOnUV)   continue;
               
               // beginning of selection of pairs and calculation of ratio
               // for timing --- constant fraction, fixed value, or peak time
               // below is for gain ratios, not timing.
               ratioVal=0;
               Bool_t saGood,anglesGood;
               LongDouble_t SAR,SARb;
               SAR   = (pmsaexp[iPM] / pmsaexp[jPM]);
               SARb  = (pmsaexp[jPM] / pmsaexp[iPM]);
               saGood= SAR<dexpSARmax && SARb<dexpSARmax;
               anglesGood = pmang[iPM]<maxIncidenceAngle && pmang[jPM]<maxIncidenceAngle;
               if (saGood&&anglesGood){
                  runnumval   = (Int_t) (runfirst+ifile);
                  entrynumval = (Int_t) iEvent;
                  ratioInd    = (Int_t) jPM;
                  ratioLotNum = (Int_t) pmlot[iPM];
                  ratioLotDen = (Int_t) pmlot[jPM];
                  ratioVal    = (Float_t) pmcharge[iPM]/pmcharge[jPM];
                  ratioValInv = (Float_t) pmcharge[jPM]/pmcharge[iPM];
                  ratioSAexp  = (Float_t) SAR;
                  ratioQa  = (Float_t) pmcharge[iPM];
                  ratioQb  = (Float_t) pmcharge[jPM];
                  ratioSAa = (Float_t) pmsaexp[iPM];
                  ratioSAb = (Float_t) pmsaexp[jPM];
                  ratioValH   = (Float_t) pmheight[iPM]/pmheight[jPM];
                  ratioValHInv= (Float_t) pmheight[jPM]/pmheight[iPM];
                  ratioHa  = (Float_t) pmheight[iPM];
                  ratioHb  = (Float_t) pmheight[jPM];
                  ratioeventU = (Float_t) eventU;
                  ratioeventV = (Float_t) eventV;
                  ratioeventW = (Float_t) eventW;
                  ratioeventE = (Float_t) eventE;
                  ratioValSAcorr    = (Float_t) (pmcharge[iPM]/pmcharge[jPM])/SAR;
                  ratioValInvSAcorr = (Float_t) (pmcharge[iPM]/pmcharge[jPM])/SARb;
                  ratioValHSAcorr   = (Float_t) (pmheight[iPM]/pmheight[jPM])/SAR;
                  ratioValHInvSAcorr= (Float_t) (pmheight[jPM]/pmheight[iPM])/SARb;
                  
                  rationphoa  = (Float_t) pmpho[iPM];
                  rationphob  = (Float_t) pmpho[jPM];
                  rationphea  = (Float_t) pmphe[iPM];
                  rationpheb  = (Float_t) pmphe[jPM];

                  cftime_diff = pm_cftime[iPM]-pm_cftime[jPM];
                  pktime_diff = pm_pktime[iPM]-pm_pktime[jPM];
                  letime_diff = pm_letime[iPM]-pm_letime[jPM];
                  cftime_this = pm_cftime[iPM];
                  pktime_this = pm_pktime[iPM];
                  letime_this = pm_letime[iPM];
                  cftime_other = pm_cftime[jPM];
                  pktime_other = pm_pktime[jPM];
                  letime_other = pm_letime[jPM];

                  if(cftime_diff < -0.5 || cftime_diff > 0.5) continue;
                  if(pktime_diff < -0.5 || pktime_diff > 0.5) continue;
                  if(letime_diff < -0.5 || letime_diff > 0.5) continue;

                  distance_this_to_event = get_cartesian_distance(eventX, eventY, eventZ, pmx[iPM], pmy[iPM], pmz[iPM]);
                  distance_other_to_event = get_cartesian_distance(eventX, eventY, eventZ, pmx[jPM], pmy[jPM], pmz[jPM]);

                  ratioeventUunc = (Float_t) eventUunc;
                  ratioeventVunc = (Float_t) eventVunc;
                  ratioeventWunc = (Float_t) eventWunc;
                  
                  ratioGa  = (Float_t) simGain[iPM];
                  ratioGb  = (Float_t) simGain[jPM];
                  ratioGexp= (Float_t) simGain[iPM]/simGain[jPM];
                  ratiocea = (Float_t) simCE[iPM];
                  ratioqea = (Float_t) simQE[iPM];
                  ratioctapa= (Float_t) simCTAP[iPM];
                  ratiowdga= (Float_t) simWDGain[iPM];
                  ratioeventX2   = (Float_t) eventX2;
                  ratioeventUrel = (Float_t) eventU-pmu[iPM];
                  ratioeventVrel = (Float_t) eventV-pmv[iPM];
                  ratioeventWrel = (Float_t) eventW-pmw[iPM];
                  ratioeventUrelb= (Float_t) eventU-pmu[jPM];
                  ratioeventVrelb= (Float_t) eventV-pmv[jPM];
                  ratioeventWrelb = (Float_t) eventW-pmw[jPM];
                  
                  ratioanglenum = (Float_t) pmang[iPM];
                  ratioangleden = (Float_t) pmang[jPM];
                  
                  nEventsInSum[iPM] += 1;
                  sumRatios[iPM]    += (LongDouble_t) pmcharge[iPM]/pmcharge[jPM];
                  sumInvRatios[iPM] += (LongDouble_t) pmcharge[jPM]/pmcharge[iPM];
                  sumRatiosSAcorr[iPM]    += (LongDouble_t) (pmcharge[iPM]/pmcharge[jPM])/SAR;
                  sumInvRatiosSAcorr[iPM] += (LongDouble_t) (pmcharge[jPM]/pmcharge[iPM])/SARb;
                  sumHRatios[iPM]   += (LongDouble_t) pmheight[iPM]/pmheight[jPM];
                  sumHInvRatios[iPM]+= (LongDouble_t) pmheight[jPM]/pmheight[iPM];
                  sumHRatiosSAcorr[iPM]   += (LongDouble_t) (pmheight[iPM]/pmheight[jPM])/SAR;
                  sumHInvRatiosSAcorr[iPM]+= (LongDouble_t) (pmheight[jPM]/pmheight[iPM])/SARb;
                  sumTrueGainRatios[iPM]  += (LongDouble_t) (simGain[iPM]/simGain[jPM]);
                  
                  ((TTree*)(*ratioArr)[iPM])->Fill();
                  
               }
            }
         }
         
      }
   }
   recinputfile->Close();
   //cout<<"Calculating Output Values"<<endl;
   ratiofile->cd();
   
   
   ratiofile->Write();
   ratiofile->Close();
   
   return;
}

LongDouble_t SeparationU(LongDouble_t uf, LongDouble_t ui)
{
   return uf-ui;
}

LongDouble_t SeparationV(LongDouble_t vf, LongDouble_t vi)
{
   return vf-vi;
}

// not exact euclidean distance, used for approximation for local distance
LongDouble_t SeparationOnUV(LongDouble_t ua, LongDouble_t va, LongDouble_t ub, LongDouble_t vb)
{
   LongDouble_t usep  = TMath::Abs(ua-ub);
   LongDouble_t vsep  = TMath::Abs(va-vb);
   LongDouble_t uvdist= TMath::Sqrt( usep*usep + vsep*vsep);
   return uvdist;
}

LongDouble_t IncidenceAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   
   TVector3 center_view = view - center;
   return normal.Angle(center_view);
   
}

LongDouble_t MPPCSolidAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   // This function returns solid angle of MPPC.
   
   // check direction.
   TVector3 center_view = center - view;
   if ((center_view) * normal <= 0)
      return 0;
   
   TVector3 center_chip;
   
   //Set U,V direction. Temporary hard corded.
   TVector3 unit[3]; // unit vectors.
   unit[0].SetXYZ(0,0,1); //U direction
   unit[1] = (unit[0].Cross(normal)).Unit();//V direction
   unit[2] = normal.Unit();//W direction
   
   //Calculate the Solidangle. Each chip is divided into  (nSegmentation)^2 in the calculation.
   LongDouble_t ChipDistance=0.05;
   LongDouble_t ChipSize=.590;
   LongDouble_t sin_a1;
   LongDouble_t sin_a2;
   LongDouble_t sin_b1;
   LongDouble_t sin_b2;
   LongDouble_t solid_total2=0;
   
   TVector3 vcorner1 = center + ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   TVector3 vcorner2 = vcorner1+ChipSize * unit[0] + ChipSize * unit[1];
   TVector3 v1       = view-vcorner1;
   TVector3 v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center - ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1-ChipSize * unit[0] + ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   + ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 + ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   - ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 - ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   return solid_total2;
}
