//
//  understandingTimeOffset.cpp
//  
//
//  Created by Will Kyle on 6/3/21.
//

#include <stdio.h>

//____________________________________________________________________________________
void MakeDB(string option, const int baseid, const int dbid) {
   
   // TSQLServer *db = ConnectSQLServer("MEG2DBro");
   // TSQLServer *db = TSQLServer::Connect("mysql://127.0.0.1:3306/MEG2", "root","Mu2e+gamma!");
   //TSQLServer *db = TSQLServer::Connect("mysql://172.26.32.1:3306/MEG2", "root","Mu2e+gamma!");
   //TSQLServer *db = TSQLServer::Connect("mysql://meg.sql.psi.ch:3306/MEG2", "meg_ro","readonly");
   TSQLServer *db = TSQLServer::Connect("mysql://meg.sql.psi.ch:3306/MEG2", "meg_ad","Mu2e+gamma!");
   if (option == "WalkMean") {
      for (auto &plot: vvPlot[0]) plot->MakeDB("WalkMean",baseid,dbid,db);
      // for (auto &plot: vvPlot[11]) plot->MakeDB("WalkMean",baseid,dbid,db);
   }
   if (option == "TPMUncert") {
      for (auto &plot: vvPlot[0]) plot->MakeDB("TPMUncert",baseid,dbid,db);
      //for (auto &plot: vvPlot[11]) plot->MakeDB("TPMUncert",baseid,dbid,db);
   }
   
   if (option == "XECTime") {
      if (!vPlotPM.size()) return;
      vector<vector<double>> ret;
      GetValues_SQL(Form("select timeoffsetCable, timeOffset from XECTime where id = %d",baseid),2,ret,db);
      string cmd;
      cmd = Form("Insert into XECTime values");
      for (auto plot: vPlotPM) {
         cmd += Form("(%d,%d,%.12f,%.12f,%.12f)",dbid,plot->fIndex,ret[plot->fIndex][0],plot->fOffset + ret[plot->fIndex][1],plot->fOffsetUncert);
         if (plot->fIndex != nPM-1) cmd += ",";
         else cmd += ";";
      }
      cout<<cmd<<endl;
   }
   
   if (option == "WalkSlope") {
      if (!vPlotPM.size()) return;
      vector<vector<double>> ret;
      //GetValues_SQL(Form("select timeoffsetCable, timeOffset from XECTime where id = %d",baseid),2,ret,db);
      string cmd;
      cmd = Form("Insert into XECTimeFitWalkSlope values");
      for (auto plot: vPlotPM) {
         cmd += Form("(%d,%d,%.12f,%.12f)",dbid,plot->fIndex,plot->fSlope,plot->fIntercept);
         if (plot->fIndex != nPM-1) cmd += ",";
         else cmd += ";";
      }
      cout<<cmd<<endl;
   }
   
   if(option == "Distance") {
      for (auto &plot: vvPlot[0]) plot->MakeDB("Distance",baseid,dbid,db);
   }
   if(option == "Incangle") {
      for (auto &plot: vvPlot[0]) plot->MakeDB("Incangle",baseid,dbid,db);
   }
   db->Close();
   return;
}


//____________________________________________________________________________________
void TimeFitPlot::MakeDB(string option, const int baseid, const int dbid, TSQLServer *db){
   if (option == "WalkMean") {
      if (!fDt2VsNphe) return;
      vector<vector<double>> ret;
      GetValues_SQL(Form("select * from XECTimeFitWalkMean where id = %d and idx= %d",baseid,fId),205,ret,db);
      string cmd;
      cmd = Form("Insert into XECTimeFitWalkMean values(%d,%d,%d,%.3f,%.3f",dbid,fId,fDt2VsNpheFit[1]->GetXaxis() ->GetNbins(), fDt2VsNpheFit[1]->GetXaxis()->GetXmin(),fDt2VsNpheFit[1]->GetXaxis()->GetXmax());
      for (int i = 0; i < 200;i++) {
         cmd += ", ";
         if (i < fDt2VsNpheFit[1]->GetXaxis()->GetNbins()) {
            double val = fDt2VsNpheFit[1]->GetBinContent(i+1);
            if(ret.size()>0 && ret[0].size()>i+5)
               cmd += Form("%.5e",val + ret[0][i+5]);
            else std::cout << "return from DB has not enough size!" << std::endl;
         } else {
            cmd += "0";
         }
      }
      cmd += ");";
      cout<<cmd<<endl;
   }
   
   if (option == "TPMUncert") {
      if (!fDt2VsNphe) return;
      string cmd;
      cmd = Form("Insert into XECTimeFitTPMUncert values(%d,%d,%d,%.3f,%.3f",dbid,fId,fDt2VsNpheFit[2]->GetXaxis() ->GetNbins(), fDt2VsNpheFit[2]->GetXaxis()->GetXmin(),fDt2VsNpheFit[2]->GetXaxis()->GetXmax());
      for (int i = 0; i < 200;i++) {
         cmd += ", ";
         if (i < fDt2VsNpheFit[2]->GetXaxis()->GetNbins()) {
            double val = fDt2VsNpheFit[2]->GetBinContent(i+1);
            val = max(val,1e-10);
            cmd += Form("%.5e",val);
         } else {
            cmd += "0";
         }
      }
      cmd += ");";
      cout<<cmd<<endl;
   }
   
   if (option == "Distance") {
      if (!fDt2VsDis) return;
      vector<vector<double>> ret;
      //GetValues_SQL(Form("select * from XECTimeFitWalkMean where id = %d and idx= %d",baseid,fId),205,ret,db);
      string cmd;
      cmd = Form("Insert into XECTimeFitDistance values(%d,%d,%d,%.3f,%.3f",dbid,fId,fDt2VsDisFit[1]->GetXaxis() ->GetNbins(), fDt2VsDisFit[1]->GetXaxis()->GetXmin(),fDt2VsDisFit[1]->GetXaxis()->GetXmax());
      for (int i = 0; i < 200;i++) {
         cmd += ", ";
         if (i < fDt2VsDisFit[1]->GetXaxis()->GetNbins()) {
            double val = fDt2VsDisFit[1]->GetBinContent(i+1);
            cmd += Form("%.5e",val);
            //if(ret.size()>0 && ret[0].size()>i+5)
            //   cmd += Form("%.5e",val + ret[0][i+5]);
            //else std::cout << "return from DB has not enough size!" << std::endl;
         } else {
            cmd += "0";
         }
      }
      cmd += ");";
      cout<<cmd<<endl;
   }
   
   if (option == "Incangle") {
      if (!fDt2VsIncangle) return;
      vector<vector<double>> ret;
      //GetValues_SQL(Form("select * from XECTimeFitWalkMean where id = %d and idx= %d",baseid,fId),205,ret,db);
      string cmd;
      cmd = Form("Insert into XECTimeFitIncangle values(%d,%d,%d,%.3f,%.3f",dbid,fId,fDt2VsIncangleFit[1]->GetXaxis()->GetNbins(), fDt2VsIncangleFit[1]->GetXaxis()->GetXmin(),fDt2VsIncangleFit[1]->GetXaxis()->GetXmax());
      for (int i = 0; i < 200;i++) {
         cmd += ", ";
         if (i < fDt2VsIncangleFit[1]->GetXaxis()->GetNbins()) {
            double val = fDt2VsIncangleFit[1]->GetBinContent(i+1);
            cmd += Form("%.5e",val);
            //if(ret.size()>0 && ret[0].size()>i+5)
            //   cmd += Form("%.5e",val + ret[0][i+5]);
            //else std::cout << "return from DB has not enough size!" << std::endl;
         } else {
            cmd += "0";
         }
      }
      cmd += ");";
      cout<<cmd<<endl;
   }
   return;
}


//____________________________________________________________________________________
void FillTree(string option) {
   if (!vRun.size()) return;
   printf("Read run from %d to %d.\n", vRun[0],vRun[vRun.size()-1]);
   
   // Def TChain
   TChain *rec = new TChain("rec");
   // for (auto run: vRun) rec->Add(Form("%s/%dxxx/rec%d.root",ImportPATH.c_str(),(int)run/1000,run));
   for (auto run: vRun) rec->Add(Form("%s/rec%05d.root",ImportPATH.c_str(),run));
   // for (auto run: vRun) rec->Add(Form("%s/CWrec%05d.root",ImportPATH.c_str(),run));
#ifdef ReadMCSim
   TChain *sim = new TChain("sim");
   for (auto run: vRun) sim->Add(Form("/meg/data1/shared/subprojects/rdc/datafile/timeoffset/bar/sim%05d.root",run));
   //for (auto run: vRun) sim->Add(Form("%s/sim%05d.root",ImportPATH.c_str(),run));
   //for (auto run: vRun) sim->Add(Form("%s/CWsim%05d.root",ImportPATH.c_str(),run));
   rec->AddFriend(sim);
#endif
   
   // Event selection
   string strsel = "ninner2>npmt2/6. && abs(uvwprofit[0][0])<9 && abs(uvwprofit[0][1]+17)<9";
   // string strsel = "ninner2>npmt2/6. && abs(uvwprofit[0][0])<12 && abs(uvwprofit[0][1]+17)<12 && uvwprofit[0][0] > 0";
   // strsel += " && uvwprofit[0][2]>0.2 && uvwprofit[0][2]<20";
   strsel += " && uvwprofit[0][2]>0.2 && uvwprofit[0][2]<12";
#ifdef ReadMCSim
   strsel += " && abs(inmaxuvw[0][0]*0.984 - mcxechit.xyz[0][2])<3 && abs(inmaxuvw[0][1] - (atan2(mcxechit.xyz[0][1],-mcxechit.xyz[0][0])*64.84))<3";
#endif
   //strsel += " && uvwprofitUncorr[0][3][0]>0";
   
   //comment out for the first iteration
   //strsel += " && xectimefit.timeUncert[0]<70e-12";
   
   // strsel += " && xectimefit.timeChisq[0] < 10";
   // if (vRun[0] == 75001) strsel += " && mcxechit@.GetEntries()>1 ";
   // strsel += Form(" && nsum2*%.4e>40 && nsum2*%.4e<54", gEScale, gEScale);
   //no energy cut, temporarly
#ifdef CW
   strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>15 && (nsum2+ninner2*%.4lf)*%.4e<19", gMPPCR, gEScale, gMPPCR, gEScale);
#else
#ifdef ReadMCSim
   //from Signal Gamma
   strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>50 && (nsum2+ninner2*%.4lf)*%.4e<55", gMPPCR, gEScale, gMPPCR, gEScale);
   //from BGGamma
   //strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>48 && (nsum2+ninner2*%.4lf)*%.4e<55", gMPPCR, gEScale, gMPPCR, gEScale);
#else
#ifdef UsePreshower
   strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>50 && (nsum2+ninner2*%.4lf)*%.4e<60", gMPPCR, gEScale, gMPPCR, gEScale);
   //strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>70 && (nsum2+ninner2*%.4lf)*%.4e<90", gMPPCR, gEScale, gMPPCR, gEScale);
   //strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>47 && (nsum2+ninner2*%.4lf)*%.4e<100", gMPPCR, gEScale, gMPPCR, gEScale);
   //strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>47 && (nsum2+ninner2*%.4lf)*%.4e<60", gMPPCR, gEScale, gMPPCR, gEScale);
   strsel += " && bgoresult.crystalCenterEnergy[0]>bgoresult.crystalEnergy[0]/2";
   strsel += " && bgoresult.crystalEnergy[0]+bgoresult.plateEnergy[0]>0.059";
   //strsel += " && bgoresult.crystalEnergy[0]+bgoresult.plateEnergy[0]<0.088";//83MeV
   strsel += " && abs(bgoresult.platePos[0][0])<4 && abs(bgoresult.platePos[0][1])<4";
#else
   //strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>47 && (nsum2+ninner2*%.4lf)*%.4e<51", gMPPCR, gEScale, gMPPCR, gEScale);
   strsel += Form(" && (nsum2+ninner2*%.4lf)*%.4e>47 && (nsum2+ninner2*%.4lf)*%.4e<100", gMPPCR, gEScale, gMPPCR, gEScale);
#endif
#endif
#endif
   
   
   cout<<strsel<<endl;
   cout<<rec->GetEntries()<<" events."<<endl;
   rec->Draw(">>elist",strsel.c_str(),"entrylist");
   TEntryList* elist = (TEntryList*)gROOT->FindObject("elist");
   rec->SetEntryList(elist);
   cout<<elist->GetN()<<" events read."<<endl;
   
   ReadFromTree(rec,option);
   
   return;
}

void ReadFromTree(TChain* rec,string option) {
   
   // Check flag
   const bool kNOPM = (option == "NOPM");
   
   // Read PMRH
   vector<TimeFitPMData*> vPMData(nPM);
   // TClonesArray* cl = (TClonesArray*)TFile(Form("%s/%dxxx/rec%d.root",ImportPATH.c_str(),(int)vRun[0]/1000,vRun[0])).Get("XECPMRunHeader");
   TClonesArray* cl = (TClonesArray*)TFile(Form("%s/rec%05d.root",ImportPATH.c_str(),vRun[0])).Get("XECPMRunHeader");
   //TClonesArray* cl = (TClonesArray*)TFile(Form("%s/CWrec%05d.root",ImportPATH.c_str(),vRun[0])).Get("XECPMRunHeader");
   if (!cl) return;
   for (int pm = 0; pm < nPM; pm++) {
      vPMData[pm] = new TimeFitPMData(pm);
      MEGXECPMRunHeader* pmrh = (MEGXECPMRunHeader*)cl->At(pm);
      vPMData[pm]->LoadFromPMRH(pmrh);
      //vPMData[pm]->fOffset = pmrh->GetDRSTimeOffset();
      vPMData[pm]->fOffset = pmrh->GetCableTimeOffset()+pmrh->GetDRSTimeOffset();
      // cout<<vPMData[pm]->fIsBad<<endl;
      vPMData[pm]->SetGroup();
      // std::cout << pm << ' ' << pmrh->GetCratename() << ' ' << pmrh->GetWDBname() << ' ' << pmrh->GetCableTimeOffset() << ' ' << pmrh->GetDRSTimeOffset() << std::endl;
   }
   
   // Prepare to read rec.
   TBranch *brInfo;
   ROMETreeInfo *pInfo = new ROMETreeInfo();
   rec->SetBranchAddress("Info.", &pInfo, &brInfo);
   TBranch *bPM;
   TClonesArray *pPMClusters = new TClonesArray("MEGXECPMCluster");
   MEGXECPMCluster *pPMCluster;
   if (!kNOPM) rec->SetBranchAddress("xeccl", &pPMClusters, &bPM);
   TBranch *bWFAna;
   TClonesArray *pWFAnas = new TClonesArray("MEGXECWaveformAnalysisResult");
   MEGXECWaveformAnalysisResult *pWFAna;
   if (!kNOPM && kCfs.size()) rec->SetBranchAddress("xecwfcl", &pWFAnas, &bWFAna);
   TBranch *bPoslfit;
   TClonesArray *pPoslfits = new TClonesArray("MEGXECPosLocalFitResult");
   MEGXECPosLocalFitResult *pPoslfit;
   rec->SetBranchAddress("xecposlfit", &pPoslfits, &bPoslfit);
   TBranch *bTimefit;
   TClonesArray *pTimefits = new TClonesArray("MEGXECTimeFitResult");
   MEGXECTimeFitResult *pTimefit;
   rec->SetBranchAddress("xectimefit", &pTimefits, &bTimefit);
   TBranch *bFastrec;
   TClonesArray *pFastrecs = new TClonesArray("MEGXECFastRecResult");
   MEGXECFastRecResult *pFastrec;
   rec->SetBranchAddress("xecfastrec", &pFastrecs, &bFastrec);
   
#ifdef UsePreshower
   TBranch *bBGOResult;
   TClonesArray* pBGOResults = new TClonesArray("MEGBGOResult");
   MEGBGOResult *pBGOResult;
   rec->SetBranchAddress("bgoresult", &pBGOResults, &bBGOResult);
   TBranch *bBGOCEXResult;
   TClonesArray* pBGOCEXResults = new TClonesArray("MEGBGOCEXResult");
   MEGBGOCEXResult *pBGOCEXResult;
   rec->SetBranchAddress("bgocexresult", &pBGOCEXResults, &bBGOCEXResult);
#endif
   
#ifdef ReadMCSim
   TBranch* bMCMix;
   MEGMCMixtureInfoEvent* pMCMix  = new MEGMCMixtureInfoEvent();
   rec->SetBranchAddress("mcmixevent.", &pMCMix, &bMCMix);
   TBranch* bMCKine;
   MEGMCKineEvent* pMCKine = new MEGMCKineEvent();
   rec->SetBranchAddress("mckine.", &pMCKine, &bMCKine);
   TBranch* bMCXECHit;
   TClonesArray* pMCXECHits = new TClonesArray("MEGMCXECHit");
   rec->SetBranchAddress("mcxechit", &pMCXECHits, &bMCXECHit);
   MEGMCXECHit* pMCXECHit;
   TBranch* bMCTriggerEvent;
   MEGMCTriggerEvent* pMCTriggerEvent = new MEGMCTriggerEvent();
   rec->SetBranchAddress("mctrigger.", &pMCTriggerEvent, &bMCTriggerEvent);
#endif
   
   rec->SetBranchStatus("*", 0);
   rec->SetBranchStatus("Info.", 1);
   rec->SetBranchStatus("Info.run", 1);
   rec->SetBranchStatus("Info.event", 1);
   rec->SetBranchStatus("xecposlfit", 1);
   rec->SetBranchStatus("xecposlfit.uvw[3]", 1);
   rec->SetBranchStatus("xecposlfit.uvwprofit[3]", 1);
   rec->SetBranchStatus("xecposlfit.uvwprofitUncorr*", 1);
   rec->SetBranchStatus("xectimefit", 1);
   rec->SetBranchStatus("xectimefit.*", 1);
   rec->SetBranchStatus("xecfastrec", 1);
   rec->SetBranchStatus("xecfastrec.qsum", 1);
   rec->SetBranchStatus("xecfastrec.qinner", 1);
   rec->SetBranchStatus("xecfastrec.qsum2", 1);
   rec->SetBranchStatus("xecfastrec.nsum2", 1);
   rec->SetBranchStatus("xecfastrec.npmt2", 1);
   rec->SetBranchStatus("xecfastrec.ninner2", 1);
   if (!kNOPM) {
      if (kCfs.size()) {
         rec->SetBranchStatus("xecwfcl", 1);
         rec->SetBranchStatus("xecwfcl.cftimeArray", 1);
      }
      rec->SetBranchStatus("xeccl", 1);
      rec->SetBranchStatus("xeccl.fsize", 1);
      rec->SetBranchStatus("xeccl.tpm", 1);
      rec->SetBranchStatus("xeccl.nphe", 1);
   }
   
#ifdef UsePreshower
   rec->SetBranchStatus("bgoresult", 1);
   rec->SetBranchStatus("bgoresult.*", 1);
   rec->SetBranchStatus("bgocexresult", 1);
   rec->SetBranchStatus("bgocexresult.*", 1);
#endif
   
#ifdef ReadMCSim
   // rec->SetBranchStatus("mcmixevent.", 1);
   // rec->SetBranchStatus("mcmixevent.Offset", 1);
   rec->SetBranchStatus("mckine.nprimary", 1);
   rec->SetBranchStatus("mckine.xvtx", 1);
   rec->SetBranchStatus("mckine.yvtx", 1);
   rec->SetBranchStatus("mckine.zvtx", 1);
   // rec->SetBranchStatus("mcxechit", 1);
   rec->SetBranchStatus("mcxechit.xyz[3]", 1);
   rec->SetBranchStatus("mcxechit.time", 1);
   rec->SetBranchStatus("mcxechit.energy", 1);
   rec->SetBranchStatus("mctrigger.DRSSamplingPhaseShift", 1);
#endif
   
   // Read rec.
   TEntryList *elist = rec->GetEntryList();
   vEvent.reserve(elist->GetN());
   for (int iev = 0; iev < elist->GetN(); iev++) {
      if (iev%300 == 0) cout<<iev<<endl;
      //int eve = elist->GetEntry(iev);
      //rec->GetEntry(eve);//this is bug!
      int treeNum;
      int treeEntry = elist->GetEntryAndTree(iev, treeNum);
      int chainEntry = treeEntry + rec->GetTreeOffset()[treeNum];
      rec->GetEntry(chainEntry);
      int run = pInfo->GetRunNumber();
      int event = pInfo->GetEventNumber();
      int evtime = pInfo->GetTimeStamp();
      /*
       if(run==365724 && event==1510) continue;//??
       if(run==365725 && event==1859) continue;//??
       if(run==365731 && event==2215) continue;//??
       if(run==365734 && event==1156) continue;//??
       if(run==365735 && event==1440) continue;//??
       if(run==365737 && event==2491) continue;//??
       if(run==365740 && event== 707) continue;//??
       if(run==365749 && event== 183) continue;//??
       if(run==365749 && event== 344) continue;//??
       if(run==365751 && event==1840) continue;//??
       if(run==365752 && event==1888) continue;//??
       if(run==365762 && event== 388) continue;//??
       if(run==365764 && event== 310) continue;//??
       if(run==365764 && event== 814) continue;//??
       if(run==365765 && event==2481) continue;//??
       if(run==365769 && event== 328) continue;//??
       if(run==365780 && event== 537) continue;//??
       if(run==365787 && event==1203) continue;//??
       if(run==365789 && event== 260) continue;//??
       if(run==365790 && event==1974) continue;//??
       if(run==365792 && event==1673) continue;//??
       if(run==365795 && event==1240) continue;//??
       if(run==365806 && event== 156) continue;//??
       if(run==365806 && event== 531) continue;//??
       if(run==365814 && event== 112) continue;//??
       if(run==365815 && event==1637) continue;//??
       if(run==365823 && event== 464) continue;//??
       if(run==365823 && event==1871) continue;//??
       if(run==365829 && event== 738) continue;//??
       if(run==365830 && event== 360) continue;//??
       if(run==365834 && event== 801) continue;//??
       if(run==365835 && event==2403) continue;//??
       if(run==365843 && event== 625) continue;//??
       if(run==366149 && event==1277) continue;//pileup
       if(run==365989 && event==2613) continue;//??
       if(run==365934 && event== 646) continue;//??
       if(run==365809 && event==2533) continue;//??
       */
      //std::cout << iev << ' ' << treeEntry << ' ' << chainEntry << ' ' << treeNum << std::endl;
      /* Rec event*/
      pFastrec = (MEGXECFastRecResult*)pFastrecs->At(0);
      pTimefit = (MEGXECTimeFitResult*)pTimefits->At(0);
      pPoslfit = (MEGXECPosLocalFitResult*)pPoslfits->At(0);
      
      float nsum2 = pFastrec->Getnsum2();
      float ninner2 = pFastrec->Getninner2();
      float npmt2 = pFastrec->Getnpmt2();
      double uvw[3],xyz[3], bgoxyz[3];
      // pPoslfit->GetuvwCopy(3,uvw);
      pPoslfit->GetuvwprofitCopy(3,uvw);
      uvw[0] = pPoslfit->GetuvwprofitUncorrAt(3,0);
      uvw[1] = pPoslfit->GetuvwprofitUncorrAt(3,1);
      uvw[2] = pPoslfit->GetuvwprofitUncorrAt(3,2);
      XECTOOLS::UVW2XYZ(uvw,xyz);
      // TRandom3 *fRndm = new TRandom3();
      // uvw[0] += fRndm->Gaus(0,0.5);
      // uvw[1] += fRndm->Gaus(0,0.5);
      // uvw[2] += fRndm->Gaus(0,0.5);
      
      double rectime = (double)pTimefit->Gettime();
      // double rectime = (double)pTimefit->GettimeDivAt(2);
      double timeUncert=pTimefit->GettimeUncert();
      double timeDiv[4] = {};
      double timeDivUncert[4] = {};
      for (int i = 0; i < min(4, pTimefit->GettimeDivSize());i++) {
         timeDiv[i] = (double)pTimefit->GettimeDivAt(i);
         timeDivUncert[i] = (double)pTimefit->GettimeDivUncertAt(i);
      }
      vEvent.push_back(new TimeFitEvent());
      vEvent.back()->fRun = run;
      vEvent.back()->fEvent = event;
      vEvent.back()->fTime = evtime;
      vEvent.back()->fNsum2 = nsum2;
      vEvent.back()->fNinner2 = ninner2;
      vEvent.back()->fNpmt2 = npmt2;
      vEvent.back()->fE = (nsum2 +ninner2 * gMPPCR)  * gEScale;
      for (int j = 0; j < 3;j++) vEvent.back()->fUVW[j] = uvw[j];
      for (int j = 0; j < 3;j++) vEvent.back()->fXYZ[j] = xyz[j];
      vEvent.back()->fRecTime = rectime;
      vEvent.back()->fRecTimeUncert = timeUncert;
      for (int j = 0; j < 4;j++) vEvent.back()->fTimeDiv[j] = timeDiv[j];
      for (int j = 0; j < 4;j++) vEvent.back()->fTimeDivUncert[j] = timeDivUncert[j];
      
#ifdef UsePreshower
      pBGOResult = (MEGBGOResult*)pBGOResults->At(0);
      pBGOCEXResult = (MEGBGOCEXResult*)pBGOCEXResults->At(0);
      for(int i=0; i<3; ++i) bgoxyz[i] = pBGOResult->GetplatePosAt(i);
      PlateLocal2Global(bgoxyz);
      float DOF = TMath::Sqrt(pow(bgoxyz[0] - vtxxyz[0], 2.) + pow(bgoxyz[1] - vtxxyz[1], 2.) + pow(bgoxyz[2] - vtxxyz[2], 2.));
      DOF -= TMath::Sqrt(pow(xyz[0] - vtxxyz[0], 2.) + pow(xyz[1] - vtxxyz[1], 2.) + pow(xyz[2] - vtxxyz[2], 2.));
      vEvent.back()->fRefTime = pBGOResult->GetplateTime() - DOF/kCLight + 20e-9;
      vEvent.back()->fRefTime_front = pBGOResult->GetplateTime() + pBGOResult->GetplateTimeDif()/2 - DOF/kCLight + 20e-9;
      vEvent.back()->fRefTime_back = pBGOResult->GetplateTime() - pBGOResult->GetplateTimeDif()/2 - DOF/kCLight + 20e-9;
      //std::cout << pBGOResult->GetplateTime() << ' ' << DOF/kCLight << ' ' << pBGOResult->GetplatePosAt(0) << ' ' << pBGOResult->GetplatePosAt(1)<< std::endl;
#endif
      
      /* Sim event*/
#ifdef ReadMCSim
      float vtx[3] = {pMCKine->GetxvtxAt(0), pMCKine->GetyvtxAt(0),pMCKine->GetzvtxAt(0)};
      float flightDistance = TMath::Sqrt(pow(xyz[0]-vtx[0],2.0) + pow(xyz[1]-vtx[1],2.0) + pow(xyz[2]-vtx[2],2.0));
      // pMCXECHit = (MEGMCXECHit*)pMCXECHits->At(0);
      // double xyzmc[3] = {pMCXECHit?pMCXECHit->GetxyzAt(0):0,pMCXECHit?pMCXECHit->GetxyzAt(1):0,pMCXECHit?pMCXECHit->GetxyzAt(2):0};
      // float flightDistance = TMath::Sqrt(pow(xyzmc[0]-vtx[0],2.0) + pow(xyzmc[1]-vtx[1],2.0) + pow(xyzmc[2]-vtx[2],2.0));
      vEvent.back()->fMCTime = flightDistance/kCLight + kMCTimeOffset + pMCTriggerEvent->GetDRSSamplingPhaseShift();
      // vEvent.back()->fMCTime -= 400e-12;
      vEvent.back()->fMCTime -= 800e-12;
      vEvent.back()->fMCTime += 1.07e-7;
      
      if (pMCXECHits->GetEntriesFast()>=2) {
         pMCXECHit = (MEGMCXECHit*)pMCXECHits->At(1);
         vEvent.back()->fMCPLEne = pMCXECHit->Getenergy();
         // cout<<vEvent.back()->fMCPLEne<<endl;
      } else vEvent.back()->fMCPLEne = 0;
#endif
      
      /* Rec PM*/
      if (kNOPM) continue;
      // vEvent.back()->vPMData.reserve(nPM);
      vEvent.back()->vPMData.resize(nPM);
      for (int iPM = 0; iPM < nPM;iPM++) {
         if (!vPMData[iPM]->fIsUsed) continue;
         // if (iPM<=2460) continue;
         // vEvent.back()->vPMData.push_back(new TimeFitPMData(*vPMData[iPM]));
         vEvent.back()->vPMData[iPM] = new TimeFitPMData(*vPMData[iPM]);
         TimeFitPMData* pmdata = vEvent.back()->vPMData[iPM];
         pPMCluster = (MEGXECPMCluster*)pPMClusters->At(iPM);
         pmdata->fTpm = pPMCluster->GettpmAt(0);
         float nphe = pPMCluster->GetnpheAt(0);
         pmdata->fNphe = nphe;
         pmdata->fSqrtnpheinv = (nphe<0)?1:1./ TMath::Sqrt(nphe);
         pmdata->fTdelay = pTimefit->GettdelayAt(iPM);
         pmdata->fTwalk = pTimefit->GettwalkAt(iPM);
         pmdata->fChisq = pTimefit->GetchisqclAt(iPM);
         pmdata->fDistance = pTimefit->GetdistanceAt(iPM);
         pmdata->fIncangle = pTimefit->GetincangleAt(iPM);
         //pmdata->fTindir = pTimefit->GettindirAt(iPM);
         //std::cout << pmdata->fTindir << ' ' << pmdata->fIncangle << std::endl;
         //std::cout << iPM << ' ' << pTimefit->GetchisqclAt(iPM) << ' ' << pTimefit->GetvalidAt(iPM) << std::endl;
         
         if (kCfs.size()) {
            pWFAna = (MEGXECWaveformAnalysisResult*)pWFAnas->At(iPM);
            pmdata->fCftimes.reserve(kCfs.size());
            for (int i = 0 ; i < (int)kCfs.size();i++) pmdata->fCftimes.push_back(pWFAna->GetcftimeArrayAt(i));
         }
      }
   }
   for (auto&pm: vPMData) delete pm;
   return;
}

//-----------------------------------------------------------------------------------------
void PlateLocal2Global(double *xyz){
   const double GeV = 1.;//
   const double MeV = GeV * 1e-3;//
   static const double centimeter  = 1.;
   static const double millimeter  = 1.e-1 * centimeter;
   static const double meter       = 1.e+2 * centimeter;
   static const double degree      = 1.;
   static const double radian      = degree / (3.14159265358979323846/180.0);
   static const double milliradian = 1.e-3*radian;
   
   const double radius   = 66.5 * centimeter + 17.5 * centimeter;
   const double phi      = 15.8739 * degree;
   //const double theta    = 84.117554 * degree;
   const double theta    = 85.371862 * degree;
   const double rotangle = 0 * degree;
   const double dsurf    = 17.5 * centimeter;
   
   double dsurfcos = (dsurf + 5*millimeter) * TMath::Cos(rotangle*TMath::DegToRad());
   double posx = (radius - dsurfcos - xyz[0] * TMath::Sin(rotangle*TMath::DegToRad())) * TMath::Cos(phi*TMath::DegToRad()) - xyz[1] * TMath::Sin(phi*TMath::DegToRad());
   double posy = (radius - dsurfcos - xyz[0] * TMath::Sin(rotangle*TMath::DegToRad())) * TMath::Sin(phi*TMath::DegToRad()) + xyz[1] * TMath::Cos(phi*TMath::DegToRad());
   double posz = 1e10;
   if (abs(theta-90)>0)
      posz = radius / TMath::Max(1e-20,TMath::Tan(theta*TMath::DegToRad())) + xyz[0] * TMath::Cos(rotangle*TMath::DegToRad());
   //if (theta<90.)
   //posz = (radius - dsurfcos) / TMath::Max(1e-20,TMath::Tan(theta*TMath::DegToRad())) + xyz[0] * TMath::Cos(rotangle*TMath::DegToRad());
   else if (theta>0.)
      posz = xyz[0] * TMath::Cos(rotangle*TMath::DegToRad());
   
   xyz[0] = posx;
   xyz[1] = posy;
   xyz[2] = posz;
}



//____________________________________________________________________________________
vector<TimeFitPlot*> FillPlot(int iCF) {
std:vector<TimeFitPlot*> vPlot;
   if (!(int)vGrName.size()) return vPlot;
   for (int i = 0; i < (int)vGrName.size();i++) vPlot.push_back(new TimeFitPlot(iCF,i,vGrName[i]));
   
   for (auto &plot: vPlot) {
      plot->fDtVsNphe = new TH2D(Form("fDtVsNphe_%d_%d",plot->fICF,plot->fId),Form("fDtVsNphe_%d_%d",plot->fICF,plot->fId),kNNpheBin,0,0.2,kNDt2Bin,kDtRange[0],kDtRange[1]);
      plot->fDt1VsNphe = new TH2D(Form("fDt1VsNphe_%d_%d",plot->fICF,plot->fId),Form("fDt1VsNphe_%d_%d",plot->fICF,plot->fId),kNNpheBin,0,0.2,kNDt2Bin,kDtRange[0],kDtRange[1]);
      plot->fDt2VsNphe = new TH2D(Form("fDt2VsNphe_%d_%d",plot->fICF,plot->fId),Form("fDt2VsNphe_%d_%d",plot->fICF,plot->fId),kNNpheBin,0,0.2,kNDt2Bin,kDtRange[0],kDtRange[1]);
      plot->fNPMVsNphe = new TH1D(Form("fNPMVsNphe_%d_%d",plot->fICF,plot->fId),Form("fNPMVsNphe_%d_%d",plot->fICF,plot->fId),kNNpheBin,0,0.2);
      plot->fDt2VsDis = new TH2D(Form("fDt2VsDis_%d_%d",plot->fICF,plot->fId),Form("fDt2VsDis_%d_%d",plot->fICF,plot->fId),30,0,80,kNDt2Bin,kDtRange[0],kDtRange[1]);
      plot->fDt2VsIncangle = new TH2D(Form("fDt2VsIncangle_%d_%d",plot->fICF,plot->fId),Form("fDt2VsIncangle_%d_%d",plot->fICF,plot->fId),30,0,1,kNDt2Bin,kDtRange[0],kDtRange[1]);
   }
   
   for (auto itr = vEvent.begin(); itr != vEvent.end();itr++) {
      // if (itr - vEvent.begin() !=0) continue;
      if(abs((*itr)->fRecTime - (*itr)->fRefTime)>15e-9) continue;
      //if(abs((*itr)->fRefTime_front - (*itr)->fRefTime_back + 7.4e-11)>80e-12*3) continue;
      for (int i = 0; i < nPM;i++) {
         TimeFitPMData* pmdata = (*itr)->vPMData[i];
         if (!pmdata || pmdata->fIsBad || !pmdata->fIsUsed) continue;
         if (pmdata->fTpm>0 || pmdata->fTpm<-1e-6) continue;
         if (pmdata->fSqrtnpheinv>0.5) continue;
         if (pmdata->fGroup<0) continue;
         //if (pmdata->fChisq>5) continue;
         vPlot[pmdata->fGroup]->fDtVsNphe->Fill(pmdata->fSqrtnpheinv, pmdata->fDt);
         vPlot[pmdata->fGroup]->fDt1VsNphe->Fill(pmdata->fSqrtnpheinv, pmdata->fDt1);
         vPlot[pmdata->fGroup]->fDt2VsNphe->Fill(pmdata->fSqrtnpheinv, pmdata->fDt2);
         vPlot[pmdata->fGroup]->fNPMVsNphe->Fill(pmdata->fSqrtnpheinv);
         //if (pmdata->fSqrtnpheinv>0.15) continue;
         vPlot[pmdata->fGroup]->fDt2VsDis->Fill(pmdata->fDistance, pmdata->fDt2);
         vPlot[pmdata->fGroup]->fDt2VsIncangle->Fill(pow(1.- pmdata->fIncangle, 2), pmdata->fDt2);
         //vPlot[pmdata->fGroup]->fDt2VsIncangle->Fill(pow(pmdata->fIncangle, pmdata->fDt2);
      }
   }
   for (auto &plot: vPlot) {
      std::cout << "FitSliceExpGaus" << std::endl;
      FitSliceExpGaus(plot->fDt2VsNphe,false);
      FitSliceExpGaus(plot->fDt2VsDis,false);
      FitSliceExpGaus(plot->fDt2VsIncangle,false);
      for (int j = 0; j < 4;j++) {
         plot->fDt2VsNpheFit[j] = (TH1D*)gROOT->FindObject(Form("%s_%d",plot->fDt2VsNphe->GetName(),j));
         plot->fDt2VsDisFit[j] = (TH1D*)gROOT->FindObject(Form("%s_%d",plot->fDt2VsDis->GetName(),j));
         plot->fDt2VsIncangleFit[j] = (TH1D*)gROOT->FindObject(Form("%s_%d",plot->fDt2VsIncangle->GetName(),j));
      }
   }
   for (auto &plot: vPlot) {
      if (vEvent.size()) plot->fNPMVsNphe->Scale(1./vEvent.size());
   }
   
   return vPlot;
}






//____________________________________________________________________________________
Double_t ExpGausTime(Double_t *x, Double_t *par) {
   //par[0] : height of Gaussian
   //par[1] : peak position
   //par[2] : sigma of Gaussian
   //par[3] : decay constant of exponential
   
   Double_t fitval;
   if(par[3]==0)par[3]=1e-12;
   if (x[0] < par[1] + par[2]*par[2]/par[3]) {
      if (par[2] != 0) {
         fitval = par[0] * TMath::Exp(-1 * (x[0] - par[1]) * (x[0] - par[1]) / 2 / par[2] / par[2]);
      } else {
         fitval = 0;
      }
   } else {
      if (par[2] != 0) {
         fitval = par[0] * TMath::Exp((par[2]*par[2] / 2.0/ par[3] - (x[0] - par[1]))/par[3]);
      } else {
         fitval = 0;
      }
   }
   
   return fitval;
}



