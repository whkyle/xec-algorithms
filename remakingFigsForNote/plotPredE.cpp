//
//  plotPredE.cpp
//  
//
//  Created by Will Kyle on 8/2/21.
//


#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "../common/include/xec/xectools.h"
using namespace std;
const int Nmppc = 4092;         //Total number of MPPC channels in XEC


#include <stdio.h>

// Declare supplemental functions that are defined at the end of this file
LongDouble_t SeparationU(LongDouble_t uf, LongDouble_t ui);
LongDouble_t SeparationV(LongDouble_t vf, LongDouble_t vi);
LongDouble_t SeparationOnUV(LongDouble_t ua, LongDouble_t va, LongDouble_t ub, LongDouble_t vb);
LongDouble_t IncidenceAngle(TVector3 view, TVector3 center,TVector3 normal);
LongDouble_t MPPCSolidAngle(TVector3 view, TVector3 center,TVector3 normal);

void plotPredE(){
   
   // Init the XEC Geometry
   XECTOOLS::InitXECGeometryParameters(64.84, 106.27, 67.03, 96., 125.52, 0, 0, 0);
   
   //TString anainpath = (string) "/meg/data1/shared/subprojects/xec/Gammamc/recSigERMDFull5reana45degZCorr";
   //TString anainpath = (string) "/meg/data1/shared/subprojects/xec/Gammamc/2020cexDefaultWithTimes";
   TString anainpath = (string) "/meg/data1/shared/subprojects/xec/pmcalib_PreEng2020/Gamma/recfile";
   
   Float_t X2max=20;//30
   Float_t Urange=25;
   Float_t Vrange=65;
   Float_t Wmax=16;
   Float_t Wmin=2;
   Float_t Emin=.03;//.03
   Float_t Emax=.06;
   Float_t zerocheck=1.e-3;
   //int runfirst = 10000;
   //int runlast = 10025;
   int runfirst = 381300;// 367470;
   int runlast = 381359;// 367495;
   int nruns = runlast - runfirst + 1;
   Float_t maxUVeventDist = 15;
   
   // Get the initial file for header data
   char name[200];
   sprintf(name,"%s/rec%05d.root",anainpath.Data(),runfirst);
   TFile* recinputfile;
   TTree* rectree;
   recinputfile = new TFile(name,"READ");
   rectree = (TTree*)recinputfile->Get("rec");
   TBranch*    bxecfastrec;
   TBranch*    bxecwfcl;
   TBranch*    bxeccl;
   TBranch*    breco;
   TBranch*    bposlres;
   TBranch*    binfo;
   TClonesArray*  XECFastRecResult           = new TClonesArray("MEGXECFastRecResult");
   TClonesArray*  XECWaveformAnalysisResult  = new TClonesArray("MEGXECWaveformAnalysisResult");
   TClonesArray*  XECPMCluster               = new TClonesArray("MEGXECPMCluster");
   MEGRecData*    plfitr                     = new MEGRecData();
   TClonesArray*  poslres                    = new TClonesArray("MEGXECPosLocalFitResult");
   ROMETreeInfo*  runinfo                    = new ROMETreeInfo();
   bxecfastrec = rectree->GetBranch("xecfastrec");          //holds fastrec results for photon event
   bxecfastrec    ->SetAddress(&XECFastRecResult);
   bxecwfcl    = rectree->GetBranch("xecwfcl");             // holds ana results for all the XEC
   bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult); // pm channels' waveforms
   bxeccl      = rectree->GetBranch("xeccl");               // WFanaRes converted to more physically
   bxeccl         ->SetAddress(&XECPMCluster);              // meaningful quantities
   //breco       = rectree->GetBranch("reco.");               // Final reconstruction variables
   //breco          ->SetAddress(&plfitr);
   bposlres    = rectree->GetBranch("xecposlfit");          // fitted XEC position results for local
   bposlres       ->SetAddress(&poslres);                   // region of high signal MPPCs
   binfo       = rectree->GetBranch("Info.");
   binfo          ->SetAddress(&runinfo);
   
   TClonesArray* pmrhArray = (TClonesArray*)recinputfile->Get("XECPMRunHeader");
   MEGXECPMRunHeader *pmrh;
   LongDouble_t pmu[Nmppc],pmv[Nmppc],pmw[Nmppc],pmx[Nmppc],pmy[Nmppc],pmz[Nmppc],pmphi[Nmppc],pmr[Nmppc];
   LongDouble_t pmxyz[Nmppc][3],pmnorm[Nmppc][3];
   LongDouble_t pmcharge[Nmppc],pmheight[Nmppc],pmsaexp[Nmppc],pmang[Nmppc],pmphe[Nmppc],pmpho[Nmppc];
   LongDouble_t pm_cftime[Nmppc], pm_pktime[Nmppc], pm_letime[Nmppc];
   LongDouble_t pm_tpm[Nmppc];
   Int_t pmlot[Nmppc];
   Bool_t pmused[Nmppc];
   // Read and save the xyz,uvw coordinates of all mppcs from the run header
   for (int iPM=0;iPM<Nmppc;iPM++){
      pmrh = (MEGXECPMRunHeader*)(pmrhArray->At(iPM));
      pmu[iPM]=pmrh->GetUVWAt(0);
      pmv[iPM]=pmrh->GetUVWAt(1);
      pmw[iPM]=pmrh->GetUVWAt(2);
      pmxyz[iPM][0]=pmrh->GetXYZAt(0);
      pmxyz[iPM][1]=pmrh->GetXYZAt(1);
      pmxyz[iPM][2]=pmrh->GetXYZAt(2);
      pmx[iPM] = pmxyz[iPM][0];
      pmy[iPM] = pmxyz[iPM][1];
      pmz[iPM] = pmxyz[iPM][2];
      pmnorm[iPM][0]=pmrh->GetDirectionAt(0);
      pmnorm[iPM][1]=pmrh->GetDirectionAt(1);
      pmnorm[iPM][2]=pmrh->GetDirectionAt(2);
      pmlot[iPM] = ((Int_t) pmrh->GetProductionLot())-65;
   }
   
   // Make file and tree to save the output data
   TFile* fout = new TFile("lookingAtEpredVsDistance2021gamma.root","RECREATE");
   TTree* tout = new TTree("et","et");
   
   // Variables to save output data
   Float_t npepm;
   Float_t nphopm;
   Float_t Epred,EpredCSP,EpredCFIP;
   Float_t recE;
   Int_t pmInd;
   Float_t pmUV[2];
   Float_t eventUVW[3];
   Float_t eventXYZ[3];
   Float_t pmSA,pmSAcfip,pmSAcsp;
   Float_t pmIncidenceAngle,pmIncAngCFIP,pmIncAngCSP;
   Int_t runNo;
   Int_t eventNo;
   Float_t uvwCFIP[3];
   Float_t uvwCSP[3];
   Float_t uvDistFromPM,uvDistFromPMcsp,uvDistFromPMcfip;
   Float_t SAfitchisq,profitchisq[2];
   
   tout->Branch("ipm",&pmInd,"ipm/I");
   tout->Branch("efrompm",&Epred,"efrompm/F");
   tout->Branch("efrompmcsp",&EpredCSP,"efrompmcsp/F");
   tout->Branch("efrompmcfip",&EpredCFIP,"efrompmcfip/F");
   tout->Branch("ereco",&recE,"ereco/F");
   tout->Branch("sa",&pmSA,"sa/F");
   tout->Branch("incang",&pmIncidenceAngle,"incang/F");
   tout->Branch("uvw",eventUVW,"uvw[3]/F");
   tout->Branch("uvpm",pmUV,"uvpm[2]/F");
   tout->Branch("npe",&npepm,"npe/F");
   tout->Branch("npho",&nphopm,"npho/F");
   tout->Branch("irun",&runNo,"irun/I");
   tout->Branch("ievent",&eventNo,"ievent/I");
   tout->Branch("uvwcfip",uvwCFIP,"uvwcfip[3]/F");
   tout->Branch("uvwcsp",uvwCSP,"uvwcsp[3]/F");
   tout->Branch("sacfip",&pmSAcfip,"sacfip/F");
   tout->Branch("sacsp",&pmSAcsp,"sacsp/F");
   tout->Branch("incangcfip",&pmIncAngCFIP,"incangcfip/F");
   tout->Branch("incangcsp",&pmIncAngCSP,"incangcsp/F");
   tout->Branch("dist",&uvDistFromPM,"dist/F");
   tout->Branch("distcsp",&uvDistFromPMcsp,"distcsp/F");
   tout->Branch("distcfip",&uvDistFromPMcfip,"distcfip/F");
   tout->Branch("sachisq",&SAfitchisq,"sachisq/F");
   tout->Branch("profitchisq",profitchisq,"profitchisq[2]/F");
   
   // Make the plots to be filled
   
   TProfile* angleprof = new TProfile("angleprof","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprof = new TProfile("saprof","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprof = new TProfile("wprof","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   TProfile* angleprofcorr = new TProfile("angleprofcorr","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprofcorr = new TProfile("saprofcorr","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprofcorr = new TProfile("wprofcorr","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   TProfile* angleprofcsp = new TProfile("angleprofcsp","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprofcsp = new TProfile("saprofcsp","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprofcsp = new TProfile("wprofcsp","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   TProfile* angleprofcspcorr = new TProfile("angleprofcspcorr","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprofcspcorr = new TProfile("saprofcspcorr","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprofcspcorr = new TProfile("wprofcspcorr","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   TProfile* angleprofcfip = new TProfile("angleprofcfip","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprofcfip = new TProfile("saprofcfip","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprofcfip = new TProfile("wprofcfip","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   TProfile* angleprofcfipcorr = new TProfile("angleprofcfipcorr","Predicted Energy vs Incidence Angle;Incidence Angle [rad];E_{Reco} / E_{PM Predicted}", 100,0,TMath::Pi()/2.);
   TProfile* saprofcfipcorr = new TProfile("saprofcfipcorr","Predicted Energy vs Solid Angle;SA_{i} / 4#pi;E_{Reco} / E_{PM Predicted}",100,0,5e-3);
   TProfile* wprofcfipcorr = new TProfile("wprofcfipcorr","Predicted Energy vs Depth;Depth [cm];E_{Reco} / E_{PM Predicted}",40,0,20);
   
   Long64_t nEvent =0;
   nEvent = rectree->GetEntries();
   Int_t nEventsTotal=0;
   Int_t nEventsUsed=0;
   /*Start Loop over recfiles---------------------------------------------------------------------*/
   
   for (int ifile=0;ifile<nruns;++ifile) {
      //if ((runfirst+ifile)==3814)
      if (ifile>0){
         recinputfile->Close();
         //delete recinputfile;
         //delete rectree;
         sprintf(name,"%s/rec%05d.root",anainpath.Data(),runfirst+ifile);
         recinputfile= new TFile(name,"READ");
         if (recinputfile->IsZombie())  continue;
         rectree     = (TTree*)recinputfile->Get("rec");
         if (rectree==NULL)  continue;
         bxecfastrec = rectree->GetBranch("xecfastrec");     //holds fastrec results for photon event
         bxecfastrec    ->SetAddress(&XECFastRecResult);
         bxecwfcl    = rectree->GetBranch("xecwfcl");           // holds data for mppc channels' waveforms
         bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult);
         bxeccl      = rectree->GetBranch("xeccl");
         bxeccl         ->SetAddress(&XECPMCluster);
         //breco       = rectree->GetBranch("reco.");
         //breco          ->SetAddress(&plfitr);
         bposlres    = rectree->GetBranch("xecposlfit");
         bposlres       ->SetAddress(&poslres);
         binfo       = rectree->GetBranch("Info.");
         binfo          ->SetAddress(&runinfo);
      }
      cout<<"running file:  "<<runfirst+ifile<<endl;
      nEvent = rectree->GetEntries();
      cout<<nEvent<<" events"<<endl;
      nEventsTotal+= (Int_t) nEvent;
      runNo = (Int_t) (runfirst+ifile);
      
      // -- loop over each event in file, saving data from rec files
      for (int iEvent=0;iEvent<nEvent;iEvent++){
         if ((iEvent+1)%250<1){
            cout<<"Event:    "<<iEvent<<endl;
         }
         bxecfastrec ->GetEntry(iEvent);
         bxecwfcl    ->GetEntry(iEvent);
         //breco       ->GetEntry(iEvent);
         bposlres    ->GetEntry(iEvent);
         bxeccl      ->GetEntry(iEvent);
         binfo       ->GetEntry(iEvent);
         
         Int_t eventnumval = (Int_t) runinfo->GetEventNumber();
         eventNo = eventnumval;
         
         // --- get reconstructed shower position and position uncertainty
         int lsRegion  = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetLSfitrangeused();
         if(lsRegion < 0) {
            continue;
         }
         
         LongDouble_t eventU = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,0);
         LongDouble_t eventV = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,1);
         LongDouble_t eventW = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwSolidAt(lsRegion,2);
         
         eventUVW[0] = (Float_t) eventU;
         eventUVW[1] = (Float_t) eventV;
         eventUVW[2] = (Float_t) eventW;
         
         int profitrangeused = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->Getprofitrangeused();
         if (profitrangeused<0)  continue;
         // XECTOOLS::UVW2X(0, 0, 0)
         Double_t CSPu = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(profitrangeused,0);
         Double_t CSPv = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(profitrangeused,1);
         Double_t CSPw = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(profitrangeused,2);
         
         Double_t CSPx = XECTOOLS::UVW2X(CSPu, CSPv, CSPw);
         Double_t CSPy = XECTOOLS::UVW2Y(CSPu, CSPv, CSPw);
         Double_t CSPz = XECTOOLS::UVW2Z(CSPu, CSPv, CSPw);
         
         
         Double_t CFIPu = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitAt(0);
         Double_t CFIPv = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitAt(1);
         Double_t CFIPw = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitAt(2);
         
         Double_t CFIPx = XECTOOLS::UVW2X(CFIPu, CFIPv, CFIPw);
         Double_t CFIPy = XECTOOLS::UVW2Y(CFIPu, CFIPv, CFIPw);
         Double_t CFIPz = XECTOOLS::UVW2Z(CFIPu, CFIPv, CFIPw);
         
         uvwCSP[0] = (Float_t) CSPu;
         uvwCSP[1] = (Float_t) CSPv;
         uvwCSP[2] = (Float_t) CSPw;
         
         uvwCFIP[0] = (Float_t) CFIPu;
         uvwCFIP[1] = (Float_t) CFIPv;
         uvwCFIP[2] = (Float_t) CFIPw;
         
         // --- reject events outside a range
         bool urej,vrej,wrej,uvwrej;
         urej  = abs(eventU)<Urange;
         vrej  = abs(eventV)<Vrange;
         wrej  = (eventW>0)&&(eventW<Wmax);
         uvwrej= urej && vrej;// && wrej;
         if (!uvwrej)   continue;
         
         // --- get energy and chi squared of event
         Double_t eventX2 = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetchisqSolidAt(lsRegion);
         Double_t eventE  = ((MEGXECFastRecResult*)(XECFastRecResult->At(0)))->Getenergy();  //plfitr->GetEGammaAt(0);
         recE = (Float_t) eventE;
         SAfitchisq = (Float_t) eventX2;
         
         Double_t CSPuX2  = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(profitrangeused,0);
         Double_t CSPvX2  = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(profitrangeused,1);
         profitchisq[0] = (Float_t) CSPuX2;
         profitchisq[1] = (Float_t) CSPvX2;
         
         // --- more rejection
         bool x2rej,Erej,urejcorr,vrejcorr,wrejcorr;
         x2rej    = eventX2<X2max;
         Erej     = eventE>Emin;
         if (/*(!x2rej)||*/(!Erej))  continue;
         
         bool cspx2rej = CSPuX2<X2max && CSPvX2<X2max;
         if ( !x2rej && !cspx2rej ) continue;
         
         bool wnrej  = (eventW>Wmin);
         //if (!wnrej)    continue;
         
         bool uvwInRange = eventW>=Wmin && eventW<=Wmax;
         bool uvwcspInRange = CSPw>=Wmin && CSPw<=Wmax;
         bool uvwcfipInRange = CFIPw>=Wmin && CFIPw<=Wmax;
         bool anyInRange = uvwInRange || uvwcspInRange || uvwcfipInRange;
         if (!anyInRange)  continue;
         
         Double_t eventX = -1. * (eventW + 64.97) * TMath::Cos(eventV / 64.97);
         Double_t eventY = (eventW + 64.97) * TMath::Sin(eventV / 64.97);
         Double_t eventZ = eventU;
         
         TVector3 xyzf(eventX, eventY, eventZ);
         TVector3 xyzfcsp(CSPx, CSPy, CSPz);
         TVector3 xyzfcfip(CFIPx, CFIPy, CFIPz);
         
         // --- get useful info for selecting events to use
         for (int iPM=0;iPM<Nmppc;iPM++){
            Bool_t isCloseToPM,isCloseToPMcsp,isCloseToPMcfip;
            isCloseToPM = SeparationOnUV(eventU,eventV,pmu[iPM],pmv[iPM])<maxUVeventDist;
            isCloseToPMcsp = SeparationOnUV(CSPu,CSPv,pmu[iPM],pmv[iPM])<maxUVeventDist;
            isCloseToPMcfip = SeparationOnUV(CFIPu,CFIPv,pmu[iPM],pmv[iPM])<maxUVeventDist;
            
            uvDistFromPM = (Float_t) SeparationOnUV(eventU,eventV,pmu[iPM],pmv[iPM]);
            uvDistFromPMcsp = (Float_t) SeparationOnUV(CSPu,CSPv,pmu[iPM],pmv[iPM]);
            uvDistFromPMcfip = (Float_t) SeparationOnUV(CFIPu,CFIPv,pmu[iPM],pmv[iPM]);
            
            if (isCloseToPM && isCloseToPMcsp && isCloseToPMcfip){
               // -- get charge and peak amplitude recorded on each mppc
               pmcharge[iPM]=(-(((MEGXECWaveformAnalysisResult*)
                                 (XECWaveformAnalysisResult->At(iPM)))->GetchargeAt(0)));
               pmheight[iPM]=(-(((MEGXECWaveformAnalysisResult*)
                                 (XECWaveformAnalysisResult->At(iPM)))->GetheightAt(0)));
               pmphe[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GetnpheAt(0);
               pmpho[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GetnphoAt(0);
               pm_tpm[iPM]=((MEGXECPMCluster*)(XECPMCluster->At(iPM)))->GettpmAt(0);
               
               pm_cftime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                 (XECWaveformAnalysisResult->At(iPM)))->GetcftimeAt(0)));
               pm_pktime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                 (XECWaveformAnalysisResult->At(iPM)))->GetpktimeAt(0)));
               pm_letime[iPM]=((((MEGXECWaveformAnalysisResult*)
                                 (XECWaveformAnalysisResult->At(iPM)))->GetletimeAt(0)));
               
               // --- calculate angle and solid angle for mppcs
               // useful for timing
               LongDouble_t pmsolidangle,pmincangle;
               TVector3 pmxyzv(pmxyz[iPM][0],pmxyz[iPM][1],pmxyz[iPM][2]);
               TVector3 pmnormv(pmnorm[iPM][0],pmnorm[iPM][1],pmnorm[iPM][2]);
               pmsolidangle   = MPPCSolidAngle(xyzf, pmxyzv, -pmnormv);
               pmincangle  = IncidenceAngle(xyzf, pmxyzv, pmnormv);
               
               pmSAcsp = (Float_t) MPPCSolidAngle(xyzfcsp, pmxyzv, -pmnormv);
               pmSAcfip= (Float_t) MPPCSolidAngle(xyzfcfip, pmxyzv, -pmnormv);
               pmIncAngCSP = (Float_t) IncidenceAngle(xyzfcsp, pmxyzv, pmnormv);
               pmIncAngCFIP= (Float_t) IncidenceAngle(xyzfcfip, pmxyzv, pmnormv);
               
               if ( pmsolidangle<=0 || pmincangle>(TMath::Pi()/2.))   continue;
               if ( pmSAcsp<=0 || pmIncAngCSP>(TMath::Pi()/2.))   continue;
               if ( pmSAcfip<=0 || pmIncAngCFIP>(TMath::Pi()/2.))   continue;
               
               pmsaexp[iPM]=(pmsolidangle);
               pmang[iPM]=pmincangle;
               pmused[iPM]= (kTRUE);
               npepm = (Float_t) pmphe[iPM];
               nphopm = (Float_t) pmpho[iPM];
               
               pmUV[0] = pmu[iPM];
               pmUV[1] = pmv[iPM];
               Epred = (Float_t) pmpho[iPM] * 2.4e-8 / pmsolidangle;
               pmInd = (Int_t) iPM;
               pmSA = (Float_t) pmsolidangle;
               pmIncidenceAngle = (Float_t) pmincangle;
               
               EpredCSP = (Float_t) pmpho[iPM] * 2.4e-8 / pmSAcsp;
               EpredCFIP = (Float_t) pmpho[iPM] * 2.4e-8 / pmSAcfip;
               
               
               if (eventE>Emin && eventE<Emax ){
                  if (Epred>zerocheck && uvwInRange && x2rej /*&& abs(eventU)<10 && abs(eventV+17)<10*/){
                     angleprof->Fill(pmincangle,eventE/Epred);
                     saprof->Fill(pmsolidangle,eventE/Epred);
                     wprof->Fill(eventW,eventE/Epred);
                  }
                  if (cspx2rej){
                     if (EpredCSP>zerocheck && uvwcspInRange /*&& abs(CSPu)<10 && abs(CSPv+17)<10*/){
                        angleprofcsp->Fill(pmIncAngCSP,eventE/EpredCSP);
                        saprofcsp->Fill(pmSAcsp,eventE/EpredCSP);
                        wprofcsp->Fill(CSPw,eventE/EpredCSP);
                     }
                     if (EpredCFIP>zerocheck && uvwcfipInRange /*&& abs(CFIPu)<10 && abs(CFIPv+17)<10*/){
                        angleprofcfip->Fill(pmIncAngCFIP,eventE/EpredCFIP);
                        saprofcfip->Fill(pmSAcfip,eventE/EpredCFIP);
                        wprofcfip->Fill(CFIPw,eventE/EpredCFIP);
                     }
                  }
               }
               
               
               tout->Fill();
               
            }
            
            
            
         }
         
      }
   }
   
   //TF1* angfit = new TF1("angfit","[0]*(TMath::Cos([1]*x))^2",0,TMath::Pi()/4);
   //angfit->SetParameters(1,1);
   angleprof->Fit("pol2","R","",0,TMath::Pi()/4);
   TF1* angfit = angleprof->GetFunction("pol2");
   Double_t angfita = angfit->GetParameter(0);
   Double_t angfitb = angfit->GetParameter(1);
   Double_t angfitc = angfit->GetParameter(2);
   
   angleprofcsp->Fit("pol2","R","",0,TMath::Pi()/4);
   TF1* angfitcsp = angleprofcsp->GetFunction("pol2");
   Double_t angfitacsp = angfitcsp->GetParameter(0);
   Double_t angfitbcsp = angfitcsp->GetParameter(1);
   Double_t angfitccsp = angfitcsp->GetParameter(2);
   
   angleprofcfip->Fit("pol2","R","",0,TMath::Pi()/4);
   TF1* angfitcfip = angleprofcfip->GetFunction("pol2");
   Double_t angfitacfip = angfitcfip->GetParameter(0);
   Double_t angfitbcfip = angfitcfip->GetParameter(1);
   Double_t angfitccfip = angfitcfip->GetParameter(2);
   
   fout->cd();
   //tout->Write();
   TTree* tou = (TTree*)fout->Get("et");
   Int_t nentries = tout->GetEntries();
   cout<<"entries were "<<nentries<<endl;
   Float_t angcorr;
   Float_t regetangle,regetsa,regetuvw[3],regetepred,regetrece;
   Float_t regetanglecsp,regetsacsp,regetuvwcsp[3],regetepredcsp;
   Float_t regetanglecfip,regetsacfip,regetuvwcfip[3],regetepredcfip;
   Float_t regetchisq,regetprofitchisq[2];
   
   tou->SetBranchAddress("uvw",regetuvw);
   tou->SetBranchAddress("efrompm",&regetepred);
   tou->SetBranchAddress("ereco",&regetrece);
   tou->SetBranchAddress("incang",&regetangle);
   tou->SetBranchAddress("sa",&regetsa);
   
   tou->SetBranchAddress("uvwcsp",regetuvwcsp);
   tou->SetBranchAddress("efrompmcsp",&regetepredcsp);
   tou->SetBranchAddress("incangcsp",&regetanglecsp);
   tou->SetBranchAddress("sacsp",&regetsacsp);
   tou->SetBranchAddress("uvwcfip",regetuvwcfip);
   tou->SetBranchAddress("efrompmcfip",&regetepredcfip);
   tou->SetBranchAddress("incangcfip",&regetanglecfip);
   tou->SetBranchAddress("sacfip",&regetsacfip);
   tou->SetBranchAddress("sachisq",&regetchisq);
   tou->SetBranchAddress("profitchisq",&regetprofitchisq);
   
   for (int ival=0;ival<nentries;ival++){
      tou->GetEntry(ival);
      if (ival==0) cout<<regetangle<<endl;
      if (regetangle<(TMath::Pi()/4.) && regetepred>zerocheck && regetuvw[2]>=Wmin && regetuvw[2]<=Wmax && regetchisq<X2max /*&& abs(regetuvw[0])<10 && abs(regetuvw[1]+17)<10*/){
         angcorr = angfita + angfitb*regetangle + angfitc*pow(regetangle,2);
         if (angcorr!=0){
         angleprofcorr->Fill(regetangle, (regetrece/regetepred)/angcorr);
         saprofcorr->Fill(regetsa, (regetrece/regetepred)/angcorr);
         wprofcorr->Fill(regetuvw[2], (regetrece/regetepred)/angcorr);
         }
      }
      if (regetprofitchisq[0]<X2max && regetprofitchisq[1]<X2max){
         if (regetanglecsp<(TMath::Pi()/4.) && regetepredcsp>zerocheck && regetuvwcsp[2]>=Wmin && regetuvwcsp[2]<=Wmax /*&& abs(regetuvwcsp[0])<10 && abs(regetuvwcsp[1]+17)<10*/){
            angcorr = angfitacsp + angfitbcsp*regetanglecsp + angfitccsp*pow(regetanglecsp,2);
            if (angcorr!=0){
               angleprofcspcorr->Fill(regetanglecsp, (regetrece/regetepredcsp)/angcorr);
               saprofcspcorr->Fill(regetsacsp, (regetrece/regetepredcsp)/angcorr);
               wprofcspcorr->Fill(regetuvwcsp[2], (regetrece/regetepredcsp)/angcorr);
            }
         }
         if (regetanglecfip<(TMath::Pi()/4.) && regetepredcfip>zerocheck && regetuvwcfip[2]>=Wmin && regetuvwcfip[2]<=Wmax /*&& abs(regetuvwcfip[0])<10 && abs(regetuvwcfip[1]+17)<10*/){
            angcorr = angfitacfip + angfitbcfip*regetanglecfip + angfitccfip*pow(regetanglecfip,2);
            if (angcorr!=0){
               angleprofcfipcorr->Fill(regetanglecfip, (regetrece/regetepredcfip)/angcorr);
               saprofcfipcorr->Fill(regetsacfip, (regetrece/regetepredcfip)/angcorr);
               wprofcfipcorr->Fill(regetuvwcfip[2], (regetrece/regetepredcfip)/angcorr);
            }
         }
      }
   }
   
   fout->cd();
   fout->Write();
   return;
}



LongDouble_t SeparationU(LongDouble_t uf, LongDouble_t ui)
{
   return uf-ui;
}

LongDouble_t SeparationV(LongDouble_t vf, LongDouble_t vi)
{
   return vf-vi;
}

// not exact euclidean distance, used for approximation for local distance
LongDouble_t SeparationOnUV(LongDouble_t ua, LongDouble_t va, LongDouble_t ub, LongDouble_t vb)
{
   LongDouble_t usep  = TMath::Abs(ua-ub);
   LongDouble_t vsep  = TMath::Abs(va-vb);
   LongDouble_t uvdist= TMath::Sqrt( usep*usep + vsep*vsep);
   return uvdist;
}

LongDouble_t IncidenceAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   
   TVector3 center_view = view - center;
   return normal.Angle(center_view);
   
}

LongDouble_t MPPCSolidAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   // This function returns solid angle of MPPC.
   
   // check direction.
   TVector3 center_view = center - view;
   if ((center_view) * normal <= 0)
      return 0;
   
   TVector3 center_chip;
   
   //Set U,V direction. Temporary hard corded.
   TVector3 unit[3]; // unit vectors.
   unit[0].SetXYZ(0,0,1); //U direction
   unit[1] = (unit[0].Cross(normal)).Unit();//V direction
   unit[2] = normal.Unit();//W direction
   
   //Calculate the Solidangle. Each chip is divided into  (nSegmentation)^2 in the calculation.
   LongDouble_t ChipDistance=0.05;
   LongDouble_t ChipSize=.590;
   LongDouble_t sin_a1;
   LongDouble_t sin_a2;
   LongDouble_t sin_b1;
   LongDouble_t sin_b2;
   LongDouble_t solid_total2=0;
   
   TVector3 vcorner1 = center + ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   TVector3 vcorner2 = vcorner1+ChipSize * unit[0] + ChipSize * unit[1];
   TVector3 v1       = view-vcorner1;
   TVector3 v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center - ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1-ChipSize * unit[0] + ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   + ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 + ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   - ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 - ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   return solid_total2;
}


