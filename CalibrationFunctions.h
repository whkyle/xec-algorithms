//
//  CalibrationFunctions.h
//  
//
//  Created by Will Kyle on 10/17/21.
//

#ifndef CalibrationFunctions_h
#define CalibrationFunctions_h

namespace CalibrationFunctions
{
Double_t SeparationU(Double_t uf, Double_t ui)
{
   return uf-ui;
}

Double_t SeparationV(Double_t vf, Double_t vi)
{
   return vf-vi;
}

// not exact euclidean distance, used for approximation for local distance
Double_t SeparationOnUV(Double_t ua, Double_t va, Double_t ub, Double_t vb)
{
   Double_t usep  = TMath::Abs(ua-ub);
   Double_t vsep  = TMath::Abs(va-vb);
   Double_t uvdist= TMath::Sqrt( usep*usep + vsep*vsep);
   return uvdist;
}

Double_t IncidenceAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   
   TVector3 center_view = view - center;
   return normal.Angle(center_view);
   
}

Double_t MPPCSolidAngle(TVector3 view, TVector3 center,TVector3 normal)
{
   // This function returns solid angle of MPPC.
   
   // check direction.
   TVector3 center_view = center - view;
   if ((center_view) * normal <= 0)
      return 0;
   
   TVector3 center_chip;
   
   //Set U,V direction. Temporary hard corded.
   TVector3 unit[3]; // unit vectors.
   unit[0].SetXYZ(0,0,1); //U direction
   unit[1] = (unit[0].Cross(normal)).Unit();//V direction
   unit[2] = normal.Unit();//W direction
   
   //Calculate the Solidangle. Each chip is divided into  (nSegmentation)^2 in the calculation.
   Double_t ChipDistance=0.05;
   Double_t ChipSize=.590;
   Double_t sin_a1;
   Double_t sin_a2;
   Double_t sin_b1;
   Double_t sin_b2;
   Double_t solid_total2=0;
   
   TVector3 vcorner1 = center + ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   TVector3 vcorner2 = vcorner1+ChipSize * unit[0] + ChipSize * unit[1];
   TVector3 v1       = view-vcorner1;
   TVector3 v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center - ChipDistance/2.0* unit[0] + ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1-ChipSize * unit[0] + ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   + ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 + ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   vcorner1 = center   - ChipDistance/2.0* unit[0] - ChipDistance/2.0* unit[1];
   vcorner2 = vcorner1 - ChipSize        * unit[0] - ChipSize * unit[1];
   v1       = view-vcorner1;
   v2       = view-vcorner2;
   sin_a1 = v1.Dot(unit[0]) / sqrt(pow(v1.Dot(unit[0]),2) + pow(v1.Dot(unit[2]),2));
   sin_a2 = v2.Dot(unit[0]) / sqrt(pow(v2.Dot(unit[0]),2) + pow(v2.Dot(unit[2]),2));
   sin_b1 = v1.Dot(unit[1]) / sqrt(pow(v1.Dot(unit[1]),2) + pow(v1.Dot(unit[2]),2));
   sin_b2 = v2.Dot(unit[1]) / sqrt(pow(v2.Dot(unit[1]),2) + pow(v2.Dot(unit[2]),2));
   solid_total2 += TMath::Abs(asin(sin_a1*sin_b1) + asin(sin_a2*sin_b2) - asin(sin_a1*sin_b2) - asin(sin_b1*sin_a2))/(4*TMath::Pi());
   
   return solid_total2;
}

std::vector<Int_t> GetRunList(){
   std::vector<Int_t> RunList;
   
   std::ifstream runfile("runlist.csv");
   
   Int_t currentLine=0;
   vector<string> row;
   std::string line, word, temp;
   Int_t runval;
   
   while (std::getline(runfile,line)){
      row.clear();
      std::istringstream iss(line);
      while (getline(iss,word,',')){
         row.push_back(word);
      }
      runval = (Int_t) stoi(row[0]);
      RunList.push_back(runval);
   }
   
   return RunList;
   
}

};
#endif /* CalibrationFunctions_h */
