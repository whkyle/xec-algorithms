//
//  UCICalibration.h
//  
//
//  Created by Will Kyle on 11/12/21.
//
#include "UCIPMCalibration.h"
#ifndef UCICalibration_h
#define UCICalibration_h

class UCICalibration
{
public:
   Int_t fCalibrationIteration;
   Double_t fChiSq;
   Double_t fPreviousChiSq;
   Int_t fNDF;
   Double_t LocalColumnCorr[44];
   Double_t NinLocalColumnCorr[44];
   Double_t LocalRowCorr[93];
   Double_t NinLocalRowCorr[93];
   Double_t fsquaredGradient;
   Double_t fLineSearchStepSize;
   
   std::vector<UCIPMCalibration*> SetOfPMs;
   std::vector<UCIPMCalibration*> LineSearchSetOfPMs;
   
   UCICalibration(){};
   void InitialiseUCICalibrationSet(){
      fCalibrationIteration = 0;
      fChiSq =0;
      fNDF = 0;
      ClearColumnCorrection();
   };
   void ClearColumnCorrection(){
      for (int iCol=0;iCol<44;iCol++){
         LocalColumnCorr[iCol] = 0;
         NinLocalColumnCorr[iCol] = 0;
      }
   };
   void ClearRowCorrection(){
      for (int iRow=0;iRow<93;iRow++){
         LocalRowCorr[iRow] = 0;
         NinLocalRowCorr[iRow] = 0;
      }
   };
   void CalculateInitialValues();
   void CalculateIteration();
   void CalculateChiSq();
   void CalculateGradientDescent();
   void CalculateGradient();
   void CalculateStepSize();
   void CalculateBackTrackingLineSearch();
   void CalculateColumnCorrection();
   void CalculateRowCorrection();
   void CalculateUncertainty();
};

void UCICalibration::CalculateInitialValues(){
   for (auto pm : SetOfPMs){
      pm->fSumWeightForCorrection = 0;
      pm->fNumberOfPairwiseMeasurements = pm->PairwiseMeans.size();
      if (pm->fNumberOfPairwiseMeasurements==0) continue;
      //cout << "number of pairwise measurements " << pm->fNumberOfPairwiseMeasurements << endl;
      
      for (int ipair=0; ipair< pm->fNumberOfPairwiseMeasurements; ipair++) {
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         Double_t weight = /*1;*/((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         //cout << "weight for pm is " << weight << endl;
         if (weight<=0 || weight>1e9)  continue;
         pm->fSumWeightForCorrection += weight;
         pm->fRelativeResponse += weight * pm->PairwiseMeans[ipair];
         pm->fPMNDF+=1;
         
         //cout<<pm->PairwiseMeans[ipair]<<endl;
      }
      //cout<< "Ninpair " << pm->fNumberOfPairwiseMeasurements << endl;
      //cout<< "Sumweight "<< pm->fSumWeightForCorrection << endl;
      //cout<< "Relative Response " << pm->fRelativeResponse << endl;
      if (pm->fSumWeightForCorrection>0){
         pm->fRelativeResponse /= pm->fSumWeightForCorrection;
         
         
         
      }
      else{
         pm->fRelativeResponse = 0;
      }
      //cout<< "after division " << pm->fRelativeResponse << endl;
      /*if (pm->fRelativeResponse<=0 || pm->fRelativeResponse>10){
         
      }*/
   }
   //CalculateChiSq();
}

void UCICalibration::CalculateBackTrackingLineSearch(){
   CalculateGradient();
   CalculateStepSize();
}

void UCICalibration::CalculateStepSize(){
   Double_t findingStepSize = 1e-6;
   fLineSearchStepSize = findingStepSize;
   
   // condition for step size. Starts at max step size then steps back by half until conditions are satisfied.
   CalculateChiSq();
   fPreviousChiSq = fChiSq;
   for (auto pm : SetOfPMs){
      pm->fRelativeResponse -= fLineSearchStepSize * pm->fUpdateToRelativeResponse;
   }
   CalculateChiSq();
   if (fLineSearchStepSize > 2 * (fPreviousChiSq - fChiSq)/fsquaredGradient){
      fLineSearchStepSize *= 0.5;
      for (int iStep=0; iStep<20;iStep++){
         
         for (auto pm : SetOfPMs){
            pm->fRelativeResponse += fLineSearchStepSize * pm->fUpdateToRelativeResponse;
         }
         CalculateChiSq();
         
         if (fLineSearchStepSize <= 2 * (fPreviousChiSq - fChiSq)/fsquaredGradient)   break;
         else{
            fLineSearchStepSize *= 0.5;
         }
      }
   }
}

void UCICalibration::CalculateGradient(){
   fCalibrationIteration++;
   
   // Clear gradient updates.
   for (auto pm : SetOfPMs){
      pm->fUpdateToRelativeResponse = 0;
      pm->fSumWeightForCorrection = 0;
      pm->fLocalCorrectionMeasuredAgainst = 0;
      pm->fSumWeightForLocalCorr = 0;
   }
   
   for (auto pm : SetOfPMs){
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         Int_t otherPMpairIndex=-1;
         
         Double_t weight = ((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         if (pm->fRelativeResponse<1e-3 || pm->fRelativeResponse>10 || otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   continue;
         otherPM->fUpdateToRelativeResponse += (weight*(pm->PairwiseMeans[ipair]-(pm->fRelativeResponse/otherPM->fRelativeResponse)) *(pm->fRelativeResponse/pow(otherPM->fRelativeResponse,2)));//*stepsize;
         pm->fUpdateToRelativeResponse -= (weight*(pm->PairwiseMeans[ipair]-(pm->fRelativeResponse/otherPM->fRelativeResponse)) /otherPM->fRelativeResponse);//*stepsize;
      }
   }
   
   fsquaredGradient=0;
   for (auto pm : SetOfPMs){
      pm->fGradient = pm->fUpdateToRelativeResponse;
      fsquaredGradient += pow(pm->fGradient,2);
   }
}

void UCICalibration::CalculateGradientDescent(){
   fCalibrationIteration++;
   
   Double_t stepsize = 1.e-8;
   
   // Clear gradient updates.
   for (auto pm : SetOfPMs){
      pm->fUpdateToRelativeResponse = 0;
      pm->fSumWeightForCorrection = 0;
      pm->fLocalCorrectionMeasuredAgainst = 0;
      pm->fSumWeightForLocalCorr = 0;
   }
   for (auto pm : SetOfPMs){
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         
         Double_t weight = ((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         if (pm->fRelativeResponse<1e-3 || pm->fRelativeResponse>10 || otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   continue;
         otherPM->fUpdateToRelativeResponse += (weight*(pm->PairwiseMeans[ipair]-(pm->fRelativeResponse/otherPM->fRelativeResponse)) *(pm->fRelativeResponse/pow(otherPM->fRelativeResponse,2)));
         pm->fUpdateToRelativeResponse -= (weight*(pm->PairwiseMeans[ipair]-(pm->fRelativeResponse/otherPM->fRelativeResponse)) /otherPM->fRelativeResponse);
      }
   }
   for (auto pm : SetOfPMs){
      pm->fRelativeResponse -= stepsize * pm->fUpdateToRelativeResponse;
   }
}

void UCICalibration::CalculateColumnCorrection(){
   fCalibrationIteration++;
   
   Double_t averageCalibration = 0;
   Double_t nInAverageCalibration = 0;
   
   for (auto pm : SetOfPMs){
      pm->fLocalCorrectionMeasuredAgainst = 0;
      pm->fSumWeightForLocalCorr = 0;
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         Double_t weight = ((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         if (otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   {
            //cout << " other pms response " << otherPM->fRelativeResponse << endl;
            continue;
         }
         pm->fLocalCorrectionMeasuredAgainst += otherPM->fRelativeResponse * pm->NinPairwiseMeasurement[ipair];
         pm->fSumWeightForLocalCorr += pm->NinPairwiseMeasurement[ipair];
      }
      if (pm->fSumWeightForLocalCorr){
         pm->fLocalCorrectionMeasuredAgainst /= pm->fSumWeightForLocalCorr;
      }
      else {
         pm->fLocalCorrectionMeasuredAgainst = 0;
         pm->fSumWeightForLocalCorr = 0;
      }
   }
   ClearColumnCorrection();
   
   for (auto pm : SetOfPMs){
      if (pm->fLocalCorrectionMeasuredAgainst>0){
         LocalColumnCorr[pm->fColumn] += pm->fLocalCorrectionMeasuredAgainst;
         NinLocalColumnCorr[pm->fColumn]++;
      }
   }
   for (int iColumn=0;iColumn<44;iColumn++){
      if (NinLocalColumnCorr[iColumn]>0){
         LocalColumnCorr[iColumn] /= NinLocalColumnCorr[iColumn];
         //cout << "LocalColCorr for "<<iColumn<<" is " << LocalColumnCorr[iColumn] << endl;
      }
   }
   
   for (auto pm : SetOfPMs){
      if (LocalColumnCorr[pm->fColumn]>0 && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9){
         averageCalibration += pm->fRelativeResponse / LocalColumnCorr[pm->fColumn];
         nInAverageCalibration +=1;
      }
   }
   
   averageCalibration /= nInAverageCalibration;
   for (auto pm : SetOfPMs)   {
      if (LocalColumnCorr[pm->fColumn]>0  && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9) {
         pm->fRelativeResponse /= LocalColumnCorr[pm->fColumn];
         pm->fRelativeResponse /= averageCalibration;
      }
      else{
         pm->fRelativeResponse = 0;
      }
   }
   
}

void UCICalibration::CalculateRowCorrection(){
   //fCalibrationIteration++;
   
   //Double_t averageCalibration = 0;
   //Double_t nInAverageCalibration = 0;
   
   for (auto pm : SetOfPMs){
      pm->fLocalCorrectionMeasuredAgainst = 0;
      pm->fSumWeightForLocalCorr = 0;
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         Double_t weight = ((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         if (otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   {
            //cout << " other pms response " << otherPM->fRelativeResponse << endl;
            continue;
         }
         pm->fLocalCorrectionMeasuredAgainst += otherPM->fRelativeResponse * pm->NinPairwiseMeasurement[ipair];
         pm->fSumWeightForLocalCorr += pm->NinPairwiseMeasurement[ipair];
      }
      if (pm->fSumWeightForLocalCorr){
         pm->fLocalCorrectionMeasuredAgainst /= pm->fSumWeightForLocalCorr;
      }
      else {
         pm->fLocalCorrectionMeasuredAgainst = 0;
         pm->fSumWeightForLocalCorr = 0;
      }
   }
   ClearRowCorrection();
   
   for (auto pm : SetOfPMs){
      if (pm->fLocalCorrectionMeasuredAgainst>0){
         LocalRowCorr[pm->fRow] += pm->fLocalCorrectionMeasuredAgainst;
         NinLocalRowCorr[pm->fRow]++;
      }
   }
   for (int iRow=0;iRow<93;iRow++){
      if (NinLocalRowCorr[iRow]>0){
         LocalRowCorr[iRow] /= NinLocalRowCorr[iRow];
         //cout << "LocalColCorr for "<<iColumn<<" is " << LocalColumnCorr[iColumn] << endl;
      }
   }
   
   for (auto pm : SetOfPMs)   {
      if (LocalRowCorr[pm->fRow]>0  && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9) {
         pm->fRelativeResponse /= LocalRowCorr[pm->fRow];
      }
      else{
         pm->fRelativeResponse = 0;
      }
   }
   
}

void UCICalibration::CalculateUncertainty(){
   for (auto pm : SetOfPMs){
      pm->fRelativeResponseUncertainty = 0;
      pm->fRelativeResponseUncertaintyWeight = 0;
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         Double_t weight = ((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         
         if (pm->fRelativeResponse<1e-3 || pm->fRelativeResponse>10 || otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   continue;
         
         pm->fRelativeResponseUncertainty += pow(((pm->fRelativeResponse / otherPM->fRelativeResponse) - pm->PairwiseMeans[ipair]),2)*weight;
         pm->fRelativeResponseUncertaintyWeight += weight;
         
      }
      if (pm->fRelativeResponseUncertaintyWeight){
         pm->fRelativeResponseUncertainty /= pm->fRelativeResponseUncertaintyWeight;
         pm->fRelativeResponseUncertainty = sqrt(pm->fRelativeResponseUncertainty);
      }
      else {
         pm->fRelativeResponseUncertainty = 0;
         pm->fRelativeResponseUncertaintyWeight = 0;
      }
   }
}

void UCICalibration::CalculateIteration(){
   fCalibrationIteration++;
   
   Double_t averageCalibration = 0;
   Double_t nInAverageCalibration = 0;
   
   for (auto pm : SetOfPMs){
      pm->fUpdateToRelativeResponse = 0;
      pm->fSumWeightForCorrection = 0;
      pm->fLocalCorrectionMeasuredAgainst = 0;
      pm->fSumWeightForLocalCorr = 0;
      for (int ipair=0;ipair<pm->fNumberOfPairwiseMeasurements;ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=0 || pm->PairwiseMeans[ipair]>10)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         /*Int_t otherPMpairIndex=-1;
         for (int jpair=0; jpair<otherPM->fNumberOfPairwiseMeasurements;jpair++){
            if (otherPM->ChannelsMeasuredAgainst[jpair]==pm->fChannelNumber){
               otherPMpairIndex = (Int_t) jpair;
               break;
            }
         }
         if (otherPMpairIndex<0) continue;*/
         Double_t weight = /*1;*/((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         /*if (otherPM->PairwiseVariances[otherPMpairIndex]<=1e-9 || otherPM->PairwiseVariances[otherPMpairIndex]>10 || otherPM->NinPairwiseMeasurement[otherPMpairIndex]<3 || otherPM->PairwiseMeans[otherPMpairIndex]<=1e-3 || otherPM->PairwiseMeans[otherPMpairIndex]>10)   continue;*/
         if (otherPM->fRelativeResponse<1e-3 || otherPM->fRelativeResponse>10)   {
            //cout << " other pms response " << otherPM->fRelativeResponse << endl;
            continue;
         }
         pm->fUpdateToRelativeResponse += weight * pm->PairwiseMeans[ipair] *  otherPM->fRelativeResponse / pm->fRelativeResponse;
         pm->fSumWeightForCorrection += weight;
         pm->fLocalCorrectionMeasuredAgainst += otherPM->fRelativeResponse * pm->NinPairwiseMeasurement[ipair];
         pm->fSumWeightForLocalCorr += pm->NinPairwiseMeasurement[ipair];
         //cout << "PM: "<< pm->fChannelNumber<<endl;
      }
      if (pm->fSumWeightForCorrection>0) {
         pm->fUpdateToRelativeResponse /= pm->fSumWeightForCorrection;
         pm->fLocalCorrectionMeasuredAgainst /= pm->fSumWeightForLocalCorr;
      }
      else {
         pm->fUpdateToRelativeResponse = 0;
         pm->fRelativeResponse = 0;
      }
      /*if (pm->fUpdateToRelativeResponse>0 && pm->fUpdateToRelativeResponse<1e9 && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9){
         averageCalibration += pm->fRelativeResponse * pm->fUpdateToRelativeResponse;
         nInAverageCalibration +=1;
      }*/
      
   }
   ClearColumnCorrection();
   
   for (auto pm : SetOfPMs){
      if (pm->fLocalCorrectionMeasuredAgainst>0){
         LocalColumnCorr[pm->fColumn] += pm->fLocalCorrectionMeasuredAgainst;
         NinLocalColumnCorr[pm->fColumn]++;
      }
   }
   for (int iColumn=0;iColumn<44;iColumn++){
      if (NinLocalColumnCorr[iColumn]>0){
         LocalColumnCorr[iColumn] /= NinLocalColumnCorr[iColumn];
         cout << "LocalColCorr for "<<iColumn<<" is " << LocalColumnCorr[iColumn] << endl;
      }
   }
   
   for (auto pm : SetOfPMs){
      if (LocalColumnCorr[pm->fColumn]>0 && pm->fUpdateToRelativeResponse>0 && pm->fUpdateToRelativeResponse<1e9 && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9){
         averageCalibration += pm->fRelativeResponse * pm->fUpdateToRelativeResponse / LocalColumnCorr[pm->fColumn];
         nInAverageCalibration +=1;
      }
   }
   
   averageCalibration /= nInAverageCalibration;
   for (auto pm : SetOfPMs)   {
      if (LocalColumnCorr[pm->fColumn]>0 && pm->fUpdateToRelativeResponse>0 && pm->fUpdateToRelativeResponse<1e9 && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9) {
         pm->fRelativeResponse *= pm->fUpdateToRelativeResponse;
         pm->fRelativeResponse /= LocalColumnCorr[pm->fColumn];
         pm->fRelativeResponse /= averageCalibration;
      }
      else{
         pm->fRelativeResponse = 0;
      }
   }
   
   cout << " average calibration " << averageCalibration << " " << nInAverageCalibration << endl;
   
   //delete [] LocalColumnCorr;
   //delete [] NinLocalColumnCorr;
   
   //CalculateChiSq();
   
}

void UCICalibration::CalculateChiSq(){
   fChiSq=0;
   fNDF=0;
   for (auto pm : SetOfPMs){
      pm->fPMNDF = 0;
      pm->fChiSqContribution = 0;
      //cout << "Looking At PM "<<pm->fChannelNumber<<endl;
      if (pm->fNumberOfPairwiseMeasurements==0) continue;
      for (int ipair=0; ipair < pm->fNumberOfPairwiseMeasurements; ipair++){
         if (pm->PairwiseVariances[ipair]<=1e-9 || pm->PairwiseVariances[ipair]>10 || pm->NinPairwiseMeasurement[ipair]<10 || pm->PairwiseMeans[ipair]<=1e-3 || pm->PairwiseMeans[ipair]>10 || pm->fRelativeResponse>1e9)   continue;
         UCIPMCalibration* otherPM = SetOfPMs[pm->ChannelsMeasuredAgainst[ipair]];
         if (otherPM->fRelativeResponse<=1e-3 || otherPM->fRelativeResponse>1e9) continue;
         Double_t fitOffset = (pm->fRelativeResponse / otherPM->fRelativeResponse) - pm->PairwiseMeans[ipair];
         Double_t weight = /*1;*/((Double_t) pm->NinPairwiseMeasurement[ipair]) / pm->PairwiseVariances[ipair];
         if (weight<=0 || weight>1e9)  continue;
         if (pow(fitOffset,2)*weight > 0){
            pm->fChiSqContribution += .5 * pow(fitOffset,2) * weight;
            pm->fPMNDF++;
            //cout << "X2 contribution " << pm->fChiSqContribution << endl;
         }
         /*else {
            cout << pm->PairwiseVariances[ipair] << ' ' << pm->PairwiseMeans[ipair] << ' ' << pm->fRelativeResponse << ' ' << otherPM->fRelativeResponse << ' ' << pm->NinPairwiseMeasurement[ipair] << endl;
         }*/
      }
      if (abs(pm->fUpdateToRelativeResponse)>=0 && abs(pm->fUpdateToRelativeResponse)<1e9 && pm->fRelativeResponse>0 && pm->fRelativeResponse<1e9) {
         fChiSq += pm->fChiSqContribution;
         fNDF += pm->fPMNDF-1;
      }
      else {
         pm->fPMNDF = 0;
      }
   }
}

#endif /* UCICalibration_h */
