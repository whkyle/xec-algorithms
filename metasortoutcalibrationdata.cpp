//
//  metasortoutcalibrationdata.cpp
//  
//
//  Created by Will Kyle on 1/26/22.
//

#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"
#include "PMPlots.h"
#include "EventPlots.h"
#include "GlobalPlots.h"

Double_t SumOfWeights[4092][4092]{};
Double_t MeanRatio[4092][4092]{};
Int_t nRatioMeasurements[4092][4092]{};
Int_t indexOfOtherPM[4092][4092]{};
Double_t VarianceOfRatioDistribution[4092][4092]{};

void metasortoutcalibrationdata(){
   Int_t firstRun = 406113;//405110;
   Int_t lastRun = 406760;//405191;
   gStyle->SetTitleOffset(1,"y");
   gStyle->SetTitleOffset(.9,"x");
   
   // Get calibration values for plots
   TFile* newCalibFile = new TFile("uciCalibrationResultTunedBTGradientDescent.root","READ");
   
   TTree* calresulttree = (TTree*)newCalibFile->Get("response");
   Double_t newgain,newgains[4092]{};
   Int_t gainindex;
   calresulttree->SetBranchAddress("signalsize",&newgain);
   calresulttree->SetBranchAddress("index",&gainindex);
   Int_t firstnewgainentry = calresulttree->GetEntries()-4092;
   for (int ign=0;ign<4092;ign++){
      calresulttree->GetEntry(firstnewgainentry + ign);
      newgains[gainindex] = newgain;
   }
   
   //build chain
   TChain* fChain = new TChain("tree","sorted data from rec files for UCI MPPC calibrations");
   for (int iRun=firstRun;iRun<=lastRun;iRun++){
      fChain->Add(Form("/meg/data1/shared/subprojects/xec/Gammamc/ReducedRecData/calibrationData%06i.root",iRun));
   }
   Int_t NinChain = fChain->GetEntries();
   
   //Set cuts
   UCIXECSP* Cuts = new UCIXECSP();
   Cuts->SetCourseSelectionCriteria();
   
   //Variables to get/govern getting of data
   TTree* tree;
   Int_t numInChain=0;
   Int_t currenttree=-1;
   Int_t lastEntryForEvent = -1;
   Int_t runID = -1;
   Int_t runNumber;
   
      // Global event data
   Int_t EventNumber, nPMsInEvent, ProfitRange, NpmsInPredE;
   Double_t FitUVW[3], FitXYZ[3], PositionReducedChiSq[2], EGamma, TimeFit;
   
      // PM specific data
   Int_t ChannelNumber[4092], ProductionLot[4092], RowInFace[4092], NumberInRow[4092];
   Double_t Npho[4092], Nphe[4092], Charge[4092], PeakAmplitude[4092], CFtime[4092], PeakTime[4092], LeadingEdgeTime[4092], SolidAngle[4092], IncidenceAngle[4092], UDistance[4092], VDistance[4092], UVDistance[4092], DistanceToEvent[4092], PredictedEnergy[4092];
   Double_t WDGain[4092], Gain[4092], QE[4092], CE[4092], CTAP[4092], XYZPM[4092][3], UVWPM[4092][3], Direction[4092][3], CableTimeOffset[4092];
   
   Double_t stdepmpredicted;
   Double_t uciepmpredicted;
   Double_t stdepmpredictedmean;
   Double_t uciepmpredictedmean;
   Double_t NphoInner;
   Double_t NphoUnmeasureableAngle;
   
   // PM Plots
   EventPlots* diagnosticPlots = new EventPlots();
   diagnosticPlots->setofPMplots.resize(4092);
   for (int iPM=0;iPM<4092;iPM++){
      PMPlots* pm = new PMPlots(iPM);
      pm->InitializeIndividualPlots();
      pm->pairwiseRatioDistributions.resize(400);
      diagnosticPlots->setofPMplots[iPM] = pm;
   }
   
   // Plots Of Selection Crit. One prior to the final pairwise check before inclusion in calculating mean ratio.
   GlobalPlots* preSelectionPlots = new GlobalPlots();
   preSelectionPlots->InitializeHistograms(0);
   TH2D* eneVsVtest = new TH2D("energyVsV","EReco vs V;V [cm];E_{Reco} [MeV]",150,-70,70,100,30,60);
   TH2D* eneVsUtest = new TH2D("energyVsU","EReco vs U;U [cm];E_{Reco} [MeV]",75,-35,35,100,30,60);
   
   TH2D* anglecompare = new TH2D("angleboth","Angle in Both PMs of a pair;Angle PMa [rad];Angle PMb [rad]",1500,0,TMath::Pi()/2.,1500,0,TMath::Pi()/2.);
   TH2D* distancecompare = new TH2D("distanceboth","Distance To Both PMs of a pair;Distance PMa [cm];Distance PMb [cm]",300,0,30,300,0,30);
   TH2D* timecompare = new TH2D("timeboth","Time In Both PMs of a pair;CFTime PMa [ns];CFTime PMb [ns]",2000,-1000,0,2000,-1000,0);
   
   TH1D* hStdEneSpread = new TH1D("epmspreadstandard","spread of PM predicted energy with existing algorithms; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}; Entries",50,0,.5);
   TH1D* hUCIEneSpread = new TH1D("epmspreaduci","spread of PM predicted energy with UCI algorithm; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}; Entries",50,0,.5);
   
   TH1D* hStdEne = new TH1D("epmstandard","mean PM predicted energy with existing algorithms; <Npho_{i}/(d#Omega_{i}/4#pi)>_{i} MeV; Entries",60,20,80);
   TH1D* hUCIEne = new TH1D("epmuci","mean PM predicted energy with UCI algorithm; <Npho_{i}/(d#Omega_{i}/4#pi)>_{i} MeV; Entries",60,20,80);
   
   // chisq
   TH2D* hStdEneSpreadVsChiSqU = new TH2D("epmspreadstdvsuchi","spread of PM predicted energy with existing algorithms, vs (#chi^{2}/NDF)_{U} ; (#chi^{2}/NDF)_{U}; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",50,0,10,50,0,.5);
   TH2D* hStdEneSpreadVsChiSqV = new TH2D("epmspreadstdvsvchi","spread of PM predicted energy with existing algorithms, vs (#chi^{2}/NDF)_{V} ; (#chi^{2}/NDF)_{V}; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",50,0,10,50,0,.5);
   TH2D* hUCIEneSpreadVsChiSqU = new TH2D("epmspreaducivsuchi","spread of PM predicted energy with uci algorithm, vs (#chi^{2}/NDF)_{U} ; (#chi^{2}/NDF)_{U}; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",50,0,10,50,0,.5);
   TH2D* hUCIEneSpreadVsChiSqV = new TH2D("epmspreaducivsvchi","spread of PM predicted energy with uci algorithm, vs (#chi^{2}/NDF)_{V} ; (#chi^{2}/NDF)_{V}; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",50,0,10,50,0,.5);
   
   //uvw
   TH2D* hStdEneSpreadVsU = new TH2D("epmspreadstdvsu","spread of PM predicted energy with existing algorithms, vs U; U [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",60,-30,30,50,0,.5);
   TH2D* hStdEneSpreadVsV = new TH2D("epmspreadstdvsv","spread of PM predicted energy with existing algorithms, vs V; V [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",140,-70,70,50,0,.5);
   TH2D* hStdEneSpreadVsW = new TH2D("epmspreadstdvsw","spread of PM predicted energy with existing algorithms, vs W; W [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",30,0,30,50,0,.5);
   TH2D* hUCIEneSpreadVsU = new TH2D("epmspreaducivsu","spread of PM predicted energy with uci algorithm, vs U; U [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",60,-30,30,50,0,.5);
   TH2D* hUCIEneSpreadVsV = new TH2D("epmspreaducivsv","spread of PM predicted energy with uci algorithm, vs V; V [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",140,-70,70,50,0,.5);
   TH2D* hUCIEneSpreadVsW = new TH2D("epmspreaducivsw","spread of PM predicted energy with uci algorithm, vs W; W [cm]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",30,0,30,50,0,.5);
   
   TH2D* hStdEneSpreadMuVsU = new TH2D("epmmustdvsu","mean PM predicted energy with existing algorithms, vs U;U [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",60,-30,30,60,20,80);
   TH2D* hStdEneSpreadMuVsV = new TH2D("epmmustdvsv","mean PM predicted energy with existing algorithms, vs V;V [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",140,-70,70,60,20,80);
   TH2D* hStdEneSpreadMuVsW = new TH2D("epmmustdvsw","mean PM predicted energy with existing algorithms, vs W;W [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",30,0,30,60,20,80);
   TH2D* hUCIEneSpreadMuVsU = new TH2D("epmmuucivsu","mean PM predicted energy with uci algorithm, vs U;U [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",60,-30,30,60,20,80);
   TH2D* hUCIEneSpreadMuVsV = new TH2D("epmmuucivsv","mean PM predicted energy with uci algorithm, vs V;V [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",140,-70,70,60,20,80);
   TH2D* hUCIEneSpreadMuVsW = new TH2D("epmmuucivsw","mean PM predicted energy with uci algorithm, vs W;W [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",30,0,30,60,20,80);
   
   // Same plots vs UVW but after applying a correction to the mean energy based on the shower position
   TH2D* hStdEneSpreadMuVsUCorr = new TH2D("epmmustdvsuvc","mean PM predicted energy with existing algorithms, vs U, Corr;U [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",60,-30,30,60,20,80);
   TH2D* hStdEneSpreadMuVsVCorr = new TH2D("epmmustdvsvvc","mean PM predicted energy with existing algorithms, vs V, Corr;V [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",140,-70,70,60,20,80);
   TH2D* hStdEneSpreadMuVsWCorr = new TH2D("epmmustdvswvc","mean PM predicted energy with existing algorithms, vs W, Corr;W [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",30,0,30,60,20,80);
   TH2D* hUCIEneSpreadMuVsUCorr = new TH2D("epmmuucivsuvc","mean PM predicted energy with uci algorithm, vs U, Corr;U [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",60,-30,30,60,20,80);
   TH2D* hUCIEneSpreadMuVsVCorr = new TH2D("epmmuucivsvvc","mean PM predicted energy with uci algorithm, vs V, Corr;V [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",140,-70,70,60,20,80);
   TH2D* hUCIEneSpreadMuVsWCorr = new TH2D("epmmuucivswvc","mean PM predicted energy with uci algorithm, vs W, Corr;W [cm];<Npho_{i}/(d#Omega_{i}/4#pi)>_{i} [MeV]",30,0,30,60,20,80);
   
   //egamma
   TH2D* hStdEneSpreadVsEgamma = new TH2D("epmspreadstdvsegamma","spread of PM predicted energy with existing algorithms, vs E_{#gamma}; E_{#gamma} [MeV]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",30,20,80,50,0,.5);
   TH2D* hUCIEneSpreadVsEgamma = new TH2D("epmspreaducivsegamma","spread of PM predicted energy with uci algorithm, vs E_{#gamma}; E_{#gamma} [MeV]; #sigma_{Npho_{i}/(d#Omega_{i}/4#pi)}/<Npho_{i}/(d#Omega_{i}/4#pi)>_{i}",30,20,80,50,0,.5);
   
   // Plotting normalized PM parameters across events
   TH2D* hIncidenceAngleNormed = new TH2D("epmvsanglenorm","Normalized E_{#gamma}^{PMi} vs Incidence Angle;Incidence Angle [rad];E_{#gamma}^{PMi}/<E_{#gamma}>",20,0,1,50,0,2);
   TH2D* hTimeOffsetNormed = new TH2D("epmvsdtnorm","Normalized E_{#gamma}^{PMi} vs CFtime-T_{#gamma};CFtime-T_{#gamma} [ns];E_{#gamma}^{PMi}/<E_{#gamma}>",200,-50,50,50,0,2);
   TH2D* hDistanceNormed = new TH2D("epmvsdistnorm","Normalized E_{#gamma}^{PMi} vs Distance;Distance [cm];E_{#gamma}^{PMi}/<E_{#gamma}>",20,0,20,50,0,2);
   TH2D* hDUNormed = new TH2D("epmvsdunorm","Normalized E_{#gamma}^{PMi} vs #DeltaU;#DeltaU [cm];E_{#gamma}^{PMi}/<E_{#gamma}>",28,-21,21,50,0,2);
   TH2D* hDVNormed = new TH2D("epmvsdvnorm","Normalized E_{#gamma}^{PMi} vs #DeltaV;#DeltaV [cm];E_{#gamma}^{PMi}/<E_{#gamma}>",28,-21,21,50,0,2);
   TH2D* hSANormed = new TH2D("epmvssanorm","Normalized E_{#gamma}^{PMi} vs Solid Angle;d#Omega/4#pi [ster/4#pi];E_{#gamma}^{PMi}/<E_{#gamma}>",100,0,5e-3,50,0,2);
   
   // Plotting Epms/Npms
   TH1D* hStatUncert = new TH1D("statuncert","statuncert",500,0,1);
   
   // Portion of light beyond 90 degrees
   TH2D* hUnmeasurableLightAngle = new TH2D("lightbeyondninety","Light Measured With Incidence Angle > 90 Degrees;Npho Inner Face Total;Npho Inner Face > 90 Degrees",1000,0,100000,1000,0,100000);
   
   
   // Plots of ratio contributions vs parameters
   
   // setup output
   TFile* fout = new TFile("metaSortedCalibrationDataNewMethodFinal.root","RECREATE");
   TTree* pairwiseTree = new TTree("pairs","pairwise measurements for calibration");
   
   Int_t numeratorPM,denominatorPM;
   Int_t NumeratorPMRow, NumeratorPMColumn;
   Double_t PairwiseMean, PairwiseVariance;
   Int_t PairwiseNmeasurements;
   pairwiseTree->Branch("numeratorIndex",&numeratorPM,"numeratorIndex/I");
   pairwiseTree->Branch("denominatorIndex",&denominatorPM,"denominatorIndex/I");
   pairwiseTree->Branch("numeratorRow",&NumeratorPMRow,"numeratorRow/I");
   pairwiseTree->Branch("numeratorColumn",&NumeratorPMColumn,"numeratorColumn/I");
   pairwiseTree->Branch("pairwiseMean",&PairwiseMean,"pairwiseMean/D");
   pairwiseTree->Branch("pairwiseVariance",&PairwiseVariance,"pairwiseVariance/D");
   pairwiseTree->Branch("pairwiseN",&PairwiseNmeasurements,"pairwiseN/I");
   
   
   // Print length of chain
   cout << "Chain contains " << NinChain << " entries" << endl;
   
   for (int iChain=0;iChain<NinChain;iChain++){
      if (iChain%2000==0){
         cout << "Now analysing entry " << iChain << endl;
      }
      numInChain = fChain->LoadTree(iChain);             // load entry in chain
      if ( fChain->GetTreeNumber()!=currenttree )  {
         tree = fChain->GetTree();
         currenttree = fChain->GetTreeNumber();
         lastEntryForEvent = -1;
         
         // event info
         tree->SetBranchAddress("runNumber",&runNumber);
         tree->SetBranchAddress("Npms",&nPMsInEvent);
         tree->SetBranchAddress("NpmsInPredE",&NpmsInPredE);
         tree->SetBranchAddress("event",&EventNumber);
         tree->SetBranchAddress("uvw",FitUVW);
         tree->SetBranchAddress("xyz",FitXYZ);
         tree->SetBranchAddress("reducedChiSq",PositionReducedChiSq);
         tree->SetBranchAddress("fitRange",&ProfitRange);
         tree->SetBranchAddress("EGamma",&EGamma);
         tree->SetBranchAddress("TimeFit",&TimeFit);
         
         // MPPC specific info
         tree->SetBranchAddress("Npho",&Npho);
         tree->SetBranchAddress("Nphe",&Nphe);
         tree->SetBranchAddress("Charge",&Charge);
         tree->SetBranchAddress("PeakAmplitude",&PeakAmplitude);
         tree->SetBranchAddress("CFtime",&CFtime);
         tree->SetBranchAddress("PeakTime",&PeakTime);
         tree->SetBranchAddress("LeadingEdgeTime",&LeadingEdgeTime);
         tree->SetBranchAddress("SolidAngle",&SolidAngle);
         tree->SetBranchAddress("IncidenceAngle",&IncidenceAngle);
         tree->SetBranchAddress("UDistance",&UDistance);
         tree->SetBranchAddress("VDistance",&VDistance);
         tree->SetBranchAddress("UVDistance",&UVDistance);
         tree->SetBranchAddress("DistanceToEvent",&DistanceToEvent);
         tree->SetBranchAddress("EGammaFromPM",&PredictedEnergy);
         
         tree->SetBranchAddress("Channel",&ChannelNumber);
         tree->SetBranchAddress("Lot",&ProductionLot);
         tree->SetBranchAddress("Row",&RowInFace);
         tree->SetBranchAddress("Column",&NumberInRow);
         tree->SetBranchAddress("WDGain",&WDGain);
         tree->SetBranchAddress("Gain",&Gain);
         tree->SetBranchAddress("QE",&QE);
         tree->SetBranchAddress("CE",&CE);
         tree->SetBranchAddress("CTAP",&CTAP);
         tree->SetBranchAddress("XYZPM",&XYZPM);
         tree->SetBranchAddress("UVWPM",&UVWPM);
         tree->SetBranchAddress("PMDirection",Direction);
         tree->SetBranchAddress("CableTimeOffset",&CableTimeOffset);
         
         tree->SetBranchAddress("stdrmspmenergies",&stdepmpredicted);
         tree->SetBranchAddress("ucirmspmenergies",&uciepmpredicted);
         tree->SetBranchAddress("stdmeanpmenergies",&stdepmpredictedmean);
         tree->SetBranchAddress("ucimeanpmenergies",&uciepmpredictedmean);
         tree->SetBranchAddress("ninner",&NphoInner);
         tree->SetBranchAddress("indirectnpho",&NphoUnmeasureableAngle);
      }
      
      tree->GetEntry(numInChain);
      
      //Fill event selection plots
      preSelectionPlots->hMinEnergy->Fill(EGamma*1.e3);
      preSelectionPlots->hURange->Fill(FitUVW[0]);
      preSelectionPlots->hVRange->Fill(FitUVW[1]);
      preSelectionPlots->hUVRange->Fill(FitUVW[1],FitUVW[0]);
      preSelectionPlots->hWRange->Fill(FitUVW[2]);
      preSelectionPlots->hShowerTime->Fill(TimeFit*1.e9);
      preSelectionPlots->hChiSqU->Fill(PositionReducedChiSq[0]);
      preSelectionPlots->hChiSqV->Fill(PositionReducedChiSq[1]);
      preSelectionPlots->hChiSq->Fill(PositionReducedChiSq[0],PositionReducedChiSq[1]);
      eneVsVtest->Fill(FitUVW[1],EGamma*1.e3);
      eneVsUtest->Fill(FitUVW[0],EGamma*1.e3);
      
      // Fill E_{PM} event based plots
      hStdEneSpread->Fill(stdepmpredicted);
      hUCIEneSpread->Fill(uciepmpredicted);
      hStdEne->Fill(stdepmpredictedmean);
      hUCIEne->Fill(uciepmpredictedmean);
      hStdEneSpreadVsU->Fill(FitUVW[0],stdepmpredicted);
      hStdEneSpreadVsV->Fill(FitUVW[1],stdepmpredicted);
      hStdEneSpreadVsW->Fill(FitUVW[2],stdepmpredicted);
      hUCIEneSpreadVsU->Fill(FitUVW[0],uciepmpredicted);
      hUCIEneSpreadVsV->Fill(FitUVW[1],uciepmpredicted);
      hUCIEneSpreadVsW->Fill(FitUVW[2],uciepmpredicted);
      hStdEneSpreadMuVsU->Fill(FitUVW[0],stdepmpredictedmean);
      hStdEneSpreadMuVsV->Fill(FitUVW[1],stdepmpredictedmean);
      hStdEneSpreadMuVsW->Fill(FitUVW[2],stdepmpredictedmean);
      hUCIEneSpreadMuVsU->Fill(FitUVW[0],uciepmpredictedmean);
      hUCIEneSpreadMuVsV->Fill(FitUVW[1],uciepmpredictedmean);
      hUCIEneSpreadMuVsW->Fill(FitUVW[2],uciepmpredictedmean);
      hStdEneSpreadVsChiSqU->Fill(PositionReducedChiSq[0],stdepmpredicted);
      hStdEneSpreadVsChiSqV->Fill(PositionReducedChiSq[1],stdepmpredicted);
      hUCIEneSpreadVsChiSqU->Fill(PositionReducedChiSq[0],uciepmpredicted);
      hUCIEneSpreadVsChiSqV->Fill(PositionReducedChiSq[1],uciepmpredicted);
      hStdEneSpreadVsEgamma->Fill(EGamma*1e3,stdepmpredicted);
      hUCIEneSpreadVsEgamma->Fill(EGamma*1e3,uciepmpredicted);
      hStatUncert->Fill(stdepmpredicted/sqrt(NpmsInPredE));
      hUnmeasurableLightAngle->Fill(NphoInner,NphoUnmeasureableAngle);
      
      for (int iPMentry=0;iPMentry<nPMsInEvent;iPMentry++){
         
         diagnosticPlots->setofPMplots[ChannelNumber[iPMentry]]->hChargeAmplitude->Fill(Charge[iPMentry]/PeakAmplitude[iPMentry]);
         diagnosticPlots->setofPMplots[ChannelNumber[iPMentry]]->hNpho->Fill(Npho[iPMentry]);
         diagnosticPlots->setofPMplots[ChannelNumber[iPMentry]]->hCFtimes->Fill(CFtime[iPMentry]*1.e9);
         
         //Fill PM specific selection plots
         preSelectionPlots->hIncidenceAngle->Fill(IncidenceAngle[iPMentry]);
         if (PredictedEnergy[iPMentry]>0 )  preSelectionPlots->hPMEnergy->Fill(PredictedEnergy[iPMentry]*1.e3);
         preSelectionPlots->hNpho->Fill(Npho[iPMentry]);
         preSelectionPlots->hNphe->Fill(Nphe[iPMentry]);
         preSelectionPlots->hSolidAngle->Fill(SolidAngle[iPMentry]);
         preSelectionPlots->hCFtimeFromShower->Fill((CFtime[iPMentry]-TimeFit-CableTimeOffset[iPMentry])*1.e9);
         preSelectionPlots->hEventDistance->Fill(DistanceToEvent[iPMentry]);
         
         //Fill Normalized PM E_predicted diagnostics
         if ((EGamma/PredictedEnergy[iPMentry])>0 && (EGamma/PredictedEnergy[iPMentry])<10 && (Nphe[iPMentry]>50) && (IncidenceAngle[iPMentry]<TMath::Pi()/3.) && (newgains[ChannelNumber[iPMentry]])){
            hIncidenceAngleNormed->Fill(IncidenceAngle[iPMentry],1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
            hTimeOffsetNormed->Fill((CFtime[iPMentry]-CableTimeOffset[iPMentry]-TimeFit)*1e9,1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
            hDistanceNormed->Fill(DistanceToEvent[iPMentry],1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
            hDUNormed->Fill(FitUVW[0]-UVWPM[iPMentry][0],1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
            hDVNormed->Fill(FitUVW[1]-UVWPM[iPMentry][1],1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
            hSANormed->Fill(SolidAngle[iPMentry],1e3*PredictedEnergy[iPMentry]/stdepmpredictedmean);
         }
         
         for (int jPMentry=0;jPMentry<nPMsInEvent;jPMentry++){
            Int_t aPMentry = ChannelNumber[iPMentry];
            Int_t bPMentry = ChannelNumber[jPMentry];
            if (aPMentry==bPMentry) continue;
            
            Double_t solidAngleRatioBetweenPMs = SolidAngle[iPMentry]/SolidAngle[jPMentry];
            Bool_t cftimeRangei, cftimeRangej, deltacftime;
            cftimeRangei = (CFtime[iPMentry]-CableTimeOffset[iPMentry]>-620e-9) && (CFtime[iPMentry]-CableTimeOffset[iPMentry]<-590e-9);
            cftimeRangej = (CFtime[jPMentry]-CableTimeOffset[jPMentry]>-620e-9) && (CFtime[jPMentry]-CableTimeOffset[jPMentry]<-590e-9);
            deltacftime = abs(CFtime[iPMentry]-CFtime[jPMentry]-CableTimeOffset[iPMentry]+CableTimeOffset[jPMentry])<10e-9;
            Bool_t incidenceAngMaxi, incidenceAngMaxj;
            incidenceAngMaxi = IncidenceAngle[iPMentry]<TMath::Pi()/4.;
            incidenceAngMaxj = IncidenceAngle[jPMentry]<TMath::Pi()/4.;
            if (solidAngleRatioBetweenPMs< Cuts->fMaxPMSolidAngleRatio && solidAngleRatioBetweenPMs> Cuts->fMinPMSolidAngleRatio && cftimeRangei && cftimeRangej && deltacftime && incidenceAngMaxi && incidenceAngMaxj){
               
               Double_t NphoRatio = (Npho[iPMentry]/Npho[jPMentry])/solidAngleRatioBetweenPMs;
               Double_t varianceWeight = 1./((1./Nphe[iPMentry]) + (1./Nphe[jPMentry]));//pow(NphoRatio,2);
               
               SumOfWeights[aPMentry][bPMentry] += varianceWeight;
               MeanRatio[aPMentry][bPMentry] += varianceWeight*NphoRatio;
               nRatioMeasurements[aPMentry][bPMentry] += 1;
               
               if (jPMentry>iPMentry){
                  anglecompare->Fill(IncidenceAngle[iPMentry],IncidenceAngle[jPMentry]);
                  distancecompare->Fill(DistanceToEvent[iPMentry],DistanceToEvent[jPMentry]);
                  timecompare->Fill(CFtime[iPMentry]*1.e9,CFtime[jPMentry]*1.e9);
               }
            }
         }
      }
   }
   
   hStdEneSpreadMuVsU->FitSlicesY();
   hStdEneSpreadMuVsV->FitSlicesY();
   hStdEneSpreadMuVsW->FitSlicesY();
   hUCIEneSpreadMuVsU->FitSlicesY();
   hUCIEneSpreadMuVsV->FitSlicesY();
   hUCIEneSpreadMuVsW->FitSlicesY();
   
   TH1D* hStdEneSpreadMuVsU_1 = (TH1D*)gDirectory->Get("epmmustdvsu_1");
   TH1D* hUCIEneSpreadMuVsU_1 = (TH1D*)gDirectory->Get("epmmuucivsu_1");
   TH1D* hStdEneSpreadMuVsV_1 = (TH1D*)gDirectory->Get("epmmustdvsv_1");
   TH1D* hUCIEneSpreadMuVsV_1 = (TH1D*)gDirectory->Get("epmmuucivsv_1");
   TH1D* hStdEneSpreadMuVsW_1 = (TH1D*)gDirectory->Get("epmmustdvsw_1");
   TH1D* hUCIEneSpreadMuVsW_1 = (TH1D*)gDirectory->Get("epmmuucivsw_1");
   
   
   for (int iPM=0;iPM<4092;iPM++){
      for (int jPM=0;jPM<4092;jPM++){
         if (MeanRatio[iPM][jPM] && nRatioMeasurements[iPM][jPM] && SumOfWeights[iPM][jPM]){
            MeanRatio[iPM][jPM] /= SumOfWeights[iPM][jPM];
         }
         else{
            MeanRatio[iPM][jPM] = 0;
            nRatioMeasurements[iPM][jPM] = 0;
            SumOfWeights[iPM][jPM] = 0;
         }
      }
   }
   
   
   currenttree = -1;
   
   // Reloop through for variance calculation
   for (int iChain=0;iChain<NinChain;iChain++){
      if (iChain%2000==0){
         cout << "Now analysing entry " << iChain << endl;
      }
      numInChain = fChain->LoadTree(iChain);             // load entry in chain
      if ( fChain->GetTreeNumber()!=currenttree )  {
         tree = fChain->GetTree();
         currenttree = fChain->GetTreeNumber();
         lastEntryForEvent = -1;
         
         // event info
         tree->SetBranchAddress("runNumber",&runNumber);
         tree->SetBranchAddress("Npms",&nPMsInEvent);
         tree->SetBranchAddress("event",&EventNumber);
         tree->SetBranchAddress("uvw",FitUVW);
         tree->SetBranchAddress("xyz",FitXYZ);
         tree->SetBranchAddress("reducedChiSq",PositionReducedChiSq);
         tree->SetBranchAddress("fitRange",&ProfitRange);
         tree->SetBranchAddress("EGamma",&EGamma);
         tree->SetBranchAddress("TimeFit",&TimeFit);
         
         // MPPC specific info
         tree->SetBranchAddress("Npho",&Npho);
         tree->SetBranchAddress("Nphe",&Nphe);
         tree->SetBranchAddress("Charge",&Charge);
         tree->SetBranchAddress("PeakAmplitude",&PeakAmplitude);
         tree->SetBranchAddress("CFtime",&CFtime);
         tree->SetBranchAddress("PeakTime",&PeakTime);
         tree->SetBranchAddress("LeadingEdgeTime",&LeadingEdgeTime);
         tree->SetBranchAddress("SolidAngle",&SolidAngle);
         tree->SetBranchAddress("IncidenceAngle",&IncidenceAngle);
         tree->SetBranchAddress("UDistance",&UDistance);
         tree->SetBranchAddress("VDistance",&VDistance);
         tree->SetBranchAddress("UVDistance",&UVDistance);
         tree->SetBranchAddress("DistanceToEvent",&DistanceToEvent);
         tree->SetBranchAddress("EGammaFromPM",&PredictedEnergy);
         
         tree->SetBranchAddress("Channel",&ChannelNumber);
         tree->SetBranchAddress("Lot",&ProductionLot);
         tree->SetBranchAddress("Row",&RowInFace);
         tree->SetBranchAddress("Column",&NumberInRow);
         tree->SetBranchAddress("WDGain",&WDGain);
         tree->SetBranchAddress("Gain",&Gain);
         tree->SetBranchAddress("QE",&QE);
         tree->SetBranchAddress("CE",&CE);
         tree->SetBranchAddress("CTAP",&CTAP);
         tree->SetBranchAddress("XYZPM",&XYZPM);
         tree->SetBranchAddress("UVWPM",&UVWPM);
         tree->SetBranchAddress("PMDirection",Direction);
         tree->SetBranchAddress("CableTimeOffset",&CableTimeOffset);
         
         tree->SetBranchAddress("stdrmspmenergies",&stdepmpredicted);
         tree->SetBranchAddress("ucirmspmenergies",&uciepmpredicted);
         tree->SetBranchAddress("stdmeanpmenergies",&stdepmpredictedmean);
         tree->SetBranchAddress("ucimeanpmenergies",&uciepmpredictedmean);
      }
      
      tree->GetEntry(numInChain);
      
      // Fill PM Predicted energy after position correction
      
      Double_t StdUCorr = hStdEneSpreadMuVsU_1->GetBinContent(hStdEneSpreadMuVsU_1->FindBin(FitUVW[0]))/hStdEneSpreadMuVsU->GetMean(2);
      Double_t UCIUCorr = hUCIEneSpreadMuVsU_1->GetBinContent(hUCIEneSpreadMuVsU_1->FindBin(FitUVW[0]))/hUCIEneSpreadMuVsU->GetMean(2);
      Double_t StdVCorr = hStdEneSpreadMuVsV_1->GetBinContent(hStdEneSpreadMuVsV_1->FindBin(FitUVW[1]))/hStdEneSpreadMuVsV->GetMean(2);
      Double_t UCIVCorr = hUCIEneSpreadMuVsV_1->GetBinContent(hUCIEneSpreadMuVsV_1->FindBin(FitUVW[1]))/hUCIEneSpreadMuVsV->GetMean(2);
      Double_t StdWCorr = hStdEneSpreadMuVsW_1->GetBinContent(hStdEneSpreadMuVsW_1->FindBin(FitUVW[2]))/hStdEneSpreadMuVsW->GetMean(2);
      Double_t UCIWCorr = hUCIEneSpreadMuVsW_1->GetBinContent(hUCIEneSpreadMuVsW_1->FindBin(FitUVW[2]))/hUCIEneSpreadMuVsW->GetMean(2);
      
      Double_t StdUVWCorr = StdUCorr*StdVCorr*StdWCorr;
      Double_t UCIUVWCorr = UCIUCorr*UCIVCorr*UCIWCorr;
      
      hStdEneSpreadMuVsUCorr->Fill(FitUVW[0],stdepmpredictedmean/StdUVWCorr);
      hStdEneSpreadMuVsVCorr->Fill(FitUVW[1],stdepmpredictedmean/StdUVWCorr);
      hStdEneSpreadMuVsWCorr->Fill(FitUVW[2],stdepmpredictedmean/StdUVWCorr);
      hUCIEneSpreadMuVsUCorr->Fill(FitUVW[0],uciepmpredictedmean/UCIUVWCorr);
      hUCIEneSpreadMuVsVCorr->Fill(FitUVW[1],uciepmpredictedmean/UCIUVWCorr);
      hUCIEneSpreadMuVsWCorr->Fill(FitUVW[2],uciepmpredictedmean/UCIUVWCorr);
      
      for (int iPMentry=0;iPMentry<nPMsInEvent;iPMentry++){
         for (int jPMentry=0;jPMentry<nPMsInEvent;jPMentry++){
            Int_t aPMentry = ChannelNumber[iPMentry];
            Int_t bPMentry = ChannelNumber[jPMentry];
            if (aPMentry==bPMentry) continue;
            Double_t solidAngleRatioBetweenPMs = SolidAngle[iPMentry]/SolidAngle[jPMentry];
            if (solidAngleRatioBetweenPMs< Cuts->fMaxPMSolidAngleRatio && solidAngleRatioBetweenPMs> Cuts->fMinPMSolidAngleRatio){
               
               Double_t NphoRatio = (Npho[iPMentry]/Npho[jPMentry])/solidAngleRatioBetweenPMs;
               Double_t varianceWeight = 1./((1./Nphe[iPMentry]) + (1./Nphe[jPMentry]));//pow(NphoRatio,2);
               
               VarianceOfRatioDistribution[aPMentry][bPMentry] += varianceWeight * (pow(NphoRatio/solidAngleRatioBetweenPMs - MeanRatio[aPMentry][bPMentry],2));
               
            }
         }
      }
   }
   
   for (int iPM=0;iPM<4092;iPM++){
      for (int jPM=0;jPM<4092;jPM++){
         if (VarianceOfRatioDistribution[iPM][jPM] && nRatioMeasurements[iPM][jPM] && SumOfWeights[iPM][jPM]){
            VarianceOfRatioDistribution[iPM][jPM] /= SumOfWeights[iPM][jPM];
         }
         else{
            VarianceOfRatioDistribution[iPM][jPM] = 0;
         }
      }
   }
   
   
   for (int iPM=0;iPM<4092;iPM++){
      for (int jPM=0;jPM<4092;jPM++){
         if (nRatioMeasurements[iPM][jPM]){
            
            numeratorPM = iPM;
            denominatorPM = jPM;
            NumeratorPMColumn= iPM%44;
            NumeratorPMRow = TMath::Floor(iPM/44.);
            PairwiseMean = MeanRatio[iPM][jPM];
            PairwiseVariance = VarianceOfRatioDistribution[iPM][jPM];
            PairwiseNmeasurements = nRatioMeasurements[iPM][jPM];
            pairwiseTree->Fill();
         }
      }
   }
   
   // Write data
   fout->cd();
   pairwiseTree->Write();
   anglecompare->Write();
   distancecompare->Write();
   timecompare->Write();
   
   hIncidenceAngleNormed->Write();
   hTimeOffsetNormed->Write();
   hDistanceNormed->Write();
   hDUNormed->Write();
   hDVNormed->Write();
   hSANormed->Write();
   
   hStdEneSpread->Write();
   hUCIEneSpread->Write();
   hStdEne->Write();
   hUCIEne->Write();
   
   hStdEneSpreadVsU->Write();
   hStdEneSpreadVsV->Write();
   hStdEneSpreadVsW->Write();
   hUCIEneSpreadVsU->Write();
   hUCIEneSpreadVsV->Write();
   hUCIEneSpreadVsW->Write();
   hStdEneSpreadMuVsU->Write();
   hStdEneSpreadMuVsV->Write();
   hStdEneSpreadMuVsW->Write();
   hUCIEneSpreadMuVsU->Write();
   hUCIEneSpreadMuVsV->Write();
   hUCIEneSpreadMuVsW->Write();
   hStdEneSpreadVsChiSqU->Write();
   hStdEneSpreadVsChiSqV->Write();
   hUCIEneSpreadVsChiSqU->Write();
   hUCIEneSpreadVsChiSqV->Write();
   hStdEneSpreadVsEgamma->Write();
   hUCIEneSpreadVsEgamma->Write();
   
   hStdEneSpreadMuVsUCorr->Write();
   hStdEneSpreadMuVsVCorr->Write();
   hStdEneSpreadMuVsWCorr->Write();
   hUCIEneSpreadMuVsUCorr->Write();
   hUCIEneSpreadMuVsVCorr->Write();
   hUCIEneSpreadMuVsWCorr->Write();
   
   hStatUncert->Write();
   hUnmeasurableLightAngle->Write();
   
   fout->Close();
   
   TFile* hIndividualPlotsFile = new TFile("metaSortOutPMDiagnosticsFinal.root","RECREATE");
   
   hIndividualPlotsFile->cd();
   for (auto pm : diagnosticPlots->setofPMplots){
      pm->SaveIndividualPlots();
   }
   hIndividualPlotsFile->Close();
   
   TFile* fpreSelHists = new TFile("preFinalSelectionDiagnosticHistsFinal.root","RECREATE");
   fpreSelHists->cd();
   preSelectionPlots->SavePlots();
   eneVsVtest->Write();
   eneVsUtest->Write();
   fpreSelHists->Close();
   
   
   return;
}
