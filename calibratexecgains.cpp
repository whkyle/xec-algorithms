//
//  calibratexecgains.cpp
//  
//
//  Created by Will Kyle on 1/27/22.
//

#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"
#include "UCIPMCalibration.h"
#include "UCICalibration.h"


void calibratexecgains()   {
   
   // Open and setup file which contains the pairwise measurements
   TFile* pairwiseMeasurementFile = new TFile("metaSortedCalibrationDataNewMethodFinal.root","READ");
   TTree* pairs = (TTree*)pairwiseMeasurementFile->Get("pairs");
   Int_t numeratorPM,denominatorPM;
   Int_t numeratorRow,numeratorColumn;
   Double_t PairMean,PairVariance;
   Int_t PairNmeasurements;
   pairs->SetBranchAddress("numeratorIndex",&numeratorPM);
   pairs->SetBranchAddress("denominatorIndex",&denominatorPM);
   pairs->SetBranchAddress("numeratorRow",&numeratorRow);
   pairs->SetBranchAddress("numeratorColumn",&numeratorColumn);
   pairs->SetBranchAddress("pairwiseMean",&PairMean);
   pairs->SetBranchAddress("pairwiseVariance",&PairVariance);
   pairs->SetBranchAddress("pairwiseN",&PairNmeasurements);
   
   // Make file to hold calibration results
   TFile* calibrationResult = new TFile("uciCalibrationResultTunedBTGradientDescent.root","RECREATE");
   TTree* calibrationTree = new TTree("response","Results of uci calibration algorithm");
   Int_t pmChannel,pmRow,pmColumn;
   Double_t pmResponse,pmResponseRMS;
   Double_t ChiSqTotal, ChiSqNDF;
   Int_t regressionIteration=0;
   calibrationTree->Branch("iteration",&regressionIteration,"iteration/I");
   calibrationTree->Branch("index",&pmChannel,"index/I");
   calibrationTree->Branch("signalsize",&pmResponse,"signalsize/D");
   calibrationTree->Branch("uncertainty",&pmResponseRMS,"uncertainty/D");
   calibrationTree->Branch("Row",&pmRow,"Row/I");
   calibrationTree->Branch("Column",&pmColumn,"Column/I");
   calibrationTree->Branch("ChiSq",&ChiSqTotal,"ChiSq/D");
   calibrationTree->Branch("NDF",&ChiSqNDF,"NDF/D");
   
   // Initialize structure which will hold all the calibration related data
   UCICalibration* Calibration = new UCICalibration();
   Calibration->InitialiseUCICalibrationSet();
   Calibration->SetOfPMs.resize(4092);
   Calibration->LineSearchSetOfPMs.resize(4092);
   for (Int_t iPM=0;iPM<4092;iPM++){
      UCIPMCalibration* aPM = new UCIPMCalibration(iPM);
      Calibration->SetOfPMs[iPM] = aPM;
      Calibration->LineSearchSetOfPMs[iPM] = aPM;
   }
   
   
   // Loop through the entries in the pairwise file to fill the Calibration object
   Int_t NtotalPairwiseMeasurements = pairs->GetEntries();
   for (int iEntry=0;iEntry<NtotalPairwiseMeasurements;iEntry++)  {
      pairs->GetEntry(iEntry);
      Calibration->SetOfPMs[numeratorPM]->FillPairwiseArrays(denominatorPM, PairMean, PairVariance, PairNmeasurements);
      Calibration->LineSearchSetOfPMs[numeratorPM]->FillPairwiseArrays(denominatorPM, PairMean, PairVariance, PairNmeasurements);
   }
   
   Calibration->CalculateInitialValues();
   Calibration->CalculateUncertainty();
   Calibration->CalculateChiSq();
   cout<< "Initialization complete, Chi Sq / NDF is " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   regressionIteration = Calibration->fCalibrationIteration;
   ChiSqTotal = Calibration->fChiSq;
   ChiSqNDF = (Double_t) Calibration->fNDF;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmResponseRMS = pm->fRelativeResponseUncertainty;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   Double_t pastThreeChiSquareValues[3];
   pastThreeChiSquareValues[0] = Calibration->fChiSq;
   pastThreeChiSquareValues[1] = 2*Calibration->fChiSq;
   pastThreeChiSquareValues[2] = Calibration->fChiSq;
   Int_t lastNDF = Calibration->fNDF;
   Double_t firstChiSquared = Calibration->fChiSq;
   for (int iIter=1;iIter<500;iIter++){
      //Calibration->CalculateIteration();
      //Calibration->CalculateGradientDescent();
      Calibration->CalculateBackTrackingLineSearch();
      Calibration->CalculateUncertainty();
      cout << "Finished Iteration "<<endl;
      Calibration->CalculateChiSq();
      pastThreeChiSquareValues[0] = pastThreeChiSquareValues[1];
      pastThreeChiSquareValues[1] = pastThreeChiSquareValues[2];
      pastThreeChiSquareValues[2] = Calibration->fChiSq;
      cout << iIter <<" iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
      regressionIteration = Calibration->fCalibrationIteration;
      ChiSqTotal = Calibration->fChiSq;
      ChiSqNDF = (Double_t) Calibration->fNDF;
      for (auto pm : Calibration->SetOfPMs){
         if (pm->fPMNDF){
            pmResponse = pm->fRelativeResponse;
            pmResponseRMS = pm->fRelativeResponseUncertainty;
            pmChannel = pm->fChannelNumber;
            pmRow = pm->fRow;
            pmColumn = pm->fColumn;
            calibrationTree->Fill();
         }
      }
      if (Calibration->fChiSq>pastThreeChiSquareValues[0] && Calibration->fChiSq>pastThreeChiSquareValues[1])  break;
      if (Calibration->fNDF<lastNDF && iIter>1) break;
      lastNDF = Calibration->fNDF;
      cout << ((Calibration->fChiSq/Calibration->fNDF)/*1.e-3*/) << endl;
      cout << ((Calibration->fPreviousChiSq-Calibration->fChiSq)/Calibration->fNDF) << endl;
      if ((((Calibration->fPreviousChiSq)-(Calibration->fChiSq))/Calibration->fNDF)<(1.e-3))   break;
   }
   
   //Calibration->CalculateRowCorrection();
   Calibration->CalculateColumnCorrection();
   Calibration->CalculateUncertainty();
   Calibration->CalculateChiSq();
   cout << Calibration->fCalibrationIteration <<" iterations complete with Chi Sq / NDF of " << Calibration->fChiSq << " / " << Calibration->fNDF << endl;
   
   regressionIteration = Calibration->fCalibrationIteration;
   ChiSqTotal = Calibration->fChiSq;
   ChiSqNDF = (Double_t) Calibration->fNDF;
   for (auto pm : Calibration->SetOfPMs){
      if (pm->fPMNDF){
         pmResponse = pm->fRelativeResponse;
         pmResponseRMS = pm->fRelativeResponseUncertainty;
         pmChannel = pm->fChannelNumber;
         pmRow = pm->fRow;
         pmColumn = pm->fColumn;
         calibrationTree->Fill();
      }
   }
   
   // Fill Data
   calibrationResult->cd();
   calibrationTree->Write();
   calibrationResult->Close();
   
   
   return;
}
