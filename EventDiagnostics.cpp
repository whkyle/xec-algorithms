//
//  EventDiagnostics.cpp
//  
//
//  Created by Will Kyle on 5/3/22.
//

#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"

using namespace std;
const int Nmppc = 4092;         //Total number of MPPC channels in XEC

void EventDiagnostics(Int_t runNumber){
   XECTOOLS::InitXECGeometryParameters(64.84, 106.27, 67.03, 96., 125.52, 0, 0, 0);
   
   TFile* recfile = new TFile(Form("/meg/data1/offline/run/%03ixxx/rec%06i.root", runNumber/1000, runNumber), "READ");
   
   UCIXECSP* Cuts = new UCIXECSP();
   Cuts->SetCourseSelectionCriteria();
   EventData* inputData = new EventData();
   inputData->InitializeEventPlots();
   
   inputData->eventMeasurementArray.resize(200);
   inputData->nphovsPredictedEnergy.resize(200);
   inputData->timeoffsetArray.resize(200);
   inputData->effectiveVelocity.resize(200);
   for (int iHist=0;iHist<200;iHist++){
      inputData->eventMeasurementArray[iHist] = new TH2D(Form("event%inpho",iHist*10),"Npho distribution;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      inputData->nphovsPredictedEnergy[iHist] = new TH2D(Form("event%inphovssaprediction",iHist*10),"Predicted Energy vs Reconstructed Energy;Row;Column",93,-.5,92.5,44,-.5,43.5);
      inputData->timeoffsetArray[iHist] = new TH2D(Form("event%itime",iHist*10),"TReco - CFTime distribution;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
      inputData->effectiveVelocity[iHist] = new TH2D(Form("event%iveff",iHist*10),"Distance To shower divided by shower TReco - cftime;Row;Column",93,-0.5,92.5,44,-0.5,43.5);
   }
   inputData->pmdatavec.resize(Nmppc);
   TClonesArray* pmrhArray = (TClonesArray*)recfile->Get("XECPMRunHeader");
   
   for (Int_t iPM=0; iPM<Nmppc; iPM++){
      PMEventData* currentPM = new PMEventData(iPM);
      currentPM->LoadFromPMRH( (MEGXECPMRunHeader*)(pmrhArray->At(iPM)) );
      inputData->pmdatavec[iPM] = currentPM;
   }
   
   TTree* rec = (TTree*)recfile->Get("rec");
   TBranch*    bxecfastrec;
   TBranch*    bxecwfcl;
   TBranch*    bxeccl;
   TBranch*    breco;
   TBranch*    bposlres;
   TBranch*    binfo;
   TBranch*    beventheader;
   TClonesArray*     XECFastRecResult           = new TClonesArray("MEGXECFastRecResult");
   TClonesArray*     XECWaveformAnalysisResult  = new TClonesArray("MEGXECWaveformAnalysisResult");
   TClonesArray*     XECPMCluster               = new TClonesArray("MEGXECPMCluster");
   MEGRecData*       RecoData                   = new MEGRecData();
   TClonesArray*     poslres                    = new TClonesArray("MEGXECPosLocalFitResult");
   ROMETreeInfo*     runinfo                    = new ROMETreeInfo();
   MEGEventHeader*   eventheader                = new MEGEventHeader();
   
   bxecfastrec    = rec->GetBranch("xecfastrec");  // holds fastrec results for photon event
   bxecwfcl       = rec->GetBranch("xecwfcl");     // holds ana results for all the XEC pm channels' waveforms
   bxeccl         = rec->GetBranch("xeccl");       // WFanaRes converted to more physically meaningful quantities
   bposlres       = rec->GetBranch("xecposlfit");  // fitted XEC position results for local region of high signal MPPCs
   binfo          = rec->GetBranch("Info.");
   breco          = rec->GetBranch("reco.");
   beventheader   = rec->GetBranch("eventheader.");
   bxecfastrec    ->SetAddress(&XECFastRecResult);
   bxecwfcl       ->SetAddress(&XECWaveformAnalysisResult);
   bxeccl         ->SetAddress(&XECPMCluster);
   bposlres       ->SetAddress(&poslres);
   binfo          ->SetAddress(&runinfo);
   breco          ->SetAddress(&RecoData);
   beventheader   ->SetAddress(&eventheader);
   
   // Make the output file
   TFile* outputFile = new TFile(Form("/meg/data1/shared/subprojects/xec/Gammamc/ReducedRecData/calibrationEventDiagnostics%06i.root",runNumber),"RECREATE");
   TTree* tout = new TTree("tree","sorted data from rec files for UCI MPPC calibrations");
   
   // Npms: number of pms saved for event
   Int_t Npms, EventNumber, ProfitRange;
   Double_t FitUVW[3], FitXYZ[3], PositionReducedChiSq[2], EGamma, TimeFit;
   tout->Branch("Npms",&Npms,"Npms/I");
   tout->Branch("runNumber",&runNumber,"runNumber/I");
   tout->Branch("event",&EventNumber,"eventNumber/I");
   tout->Branch("uvw",FitUVW,"uvw[3]/D");
   tout->Branch("xyz",FitXYZ,"xyz[3]/D");
   tout->Branch("reducedChiSq",PositionReducedChiSq,"reducedChiSq[2]/D");
   tout->Branch("fitRange",&ProfitRange,"fitRange/I");
   tout->Branch("EGamma",&EGamma,"EGamma/D");
   tout->Branch("TimeFit",&TimeFit,"TimeFit/D");
   
   
   
   
   
   
   Int_t nEvents = rec->GetEntries();
   for (Int_t iEvent=0; iEvent<nEvents; iEvent++){
      if (iEvent%100==0){
         cout << "On Event " << iEvent << endl;
      }
      Npms=0;
      Bool_t saveThisPM[4092]{};
      for (int iPM=0; iPM<4092; iPM++){
         saveThisPM[iPM] = 0;
      }
      EventNumber = iEvent;
      rec->GetEntry(iEvent);
      beventheader->GetEntry(iEvent);
      Int_t triggerType = eventheader->Getmask();
      if (triggerType)  continue; // 0 == MEG trigger; skip other triggers
      bxecwfcl->GetEntry(iEvent);
      bxeccl->GetEntry(iEvent);
      bposlres->GetEntry(iEvent);
      binfo->GetEntry(iEvent);
      breco->GetEntry(iEvent);
      beventheader->GetEntry(iEvent);
      
      inputData->GetReconstructedQuantities(RecoData);
      inputData->fProfitRange = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->Getprofitrangeused();
      if (inputData->fProfitRange<0)   continue;
      inputData->fPositionReducedChiSq[0] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(inputData->fProfitRange,0);
      inputData->fPositionReducedChiSq[1] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitChisqAt(inputData->fProfitRange,1);
      inputData->fFitUVW[0] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,0);
      inputData->fFitUVW[1] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,1);
      inputData->fFitUVW[2] = ((MEGXECPosLocalFitResult*)(poslres->At(0)))->GetuvwprofitUncorrAt(inputData->fProfitRange,2);
      inputData->fFitXYZ[0] = XECTOOLS::UVW2X(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      inputData->fFitXYZ[1] = XECTOOLS::UVW2Y(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      inputData->fFitXYZ[2] = XECTOOLS::UVW2Z(inputData->fFitUVW[0],inputData->fFitUVW[1],inputData->fFitUVW[2]);
      
      if (!Cuts->IsEventWorthAnalysing(inputData)) continue;
      
      // Event wide data
      FitUVW[0] = (Double_t) inputData->fFitUVW[0];
      FitUVW[1] = (Double_t) inputData->fFitUVW[1];
      FitUVW[2] = (Double_t) inputData->fFitUVW[2];
      FitXYZ[0] = (Double_t) inputData->fFitXYZ[0];
      FitXYZ[1] = (Double_t) inputData->fFitXYZ[1];
      FitXYZ[2] = (Double_t) inputData->fFitXYZ[2];
      PositionReducedChiSq[0] = inputData->fPositionReducedChiSq[0];
      PositionReducedChiSq[1] = inputData->fPositionReducedChiSq[1];
      ProfitRange = (Int_t) inputData->fProfitRange;
      EGamma = inputData->fEGamma;
      TimeFit = inputData->fTimeFit;
      
      
      // loops through PMs; fills measured waveform quantities and parameters of PM relative to
      // the reconstructed parameters (e.g. SolidAngle of PM per fitted position of the shower); fills
      // array for which PMs to save data from using cuts that depend upon only a single MPPC.
      for (auto pm : inputData->pmdatavec){
         pm->GetWaveformMeasurements((MEGXECWaveformAnalysisResult*)
                                     (XECWaveformAnalysisResult->At(pm->fChannelNumber)));
         pm->GetConvertedWFQuantities((MEGXECPMCluster*)(XECPMCluster->At(pm->fChannelNumber)));
         pm->FillCalculatedQuantities(inputData->fFitXYZ,inputData->fEGamma);
         if (iEvent%10==0 && iEvent<2000 ){
            int iHist = iEvent/10;
            inputData->eventMeasurementArray[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,pm->fNpho);
            if (pm->fNphe>50){
               inputData->timeoffsetArray[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,( pm->fCFtime - pm->fCableTimeOffset - TimeFit)*1.e9);
               inputData->effectiveVelocity[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,(pm->fDistanceToEvent)/((pm->fCFtime - pm->fCableTimeOffset - TimeFit )*1.e9));
            }
            if ((EGamma/pm->fPredictedEnergy)>0 && (EGamma/pm->fPredictedEnergy)<10){
               inputData->nphovsPredictedEnergy[iHist]->Fill(pm->fRowInFace,pm->fNumberInRow,( EGamma/pm->fPredictedEnergy));
            }
         }
         saveThisPM[pm->fChannelNumber] = Cuts->IsPMWorthAnalysing(pm);
      }
      
   }
   
   
}
