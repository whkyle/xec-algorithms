#include <TMinuit.h>
#include <TF1.h>
#include <TF2.h>
#include <TROOT.h>
#include <TCanvas.h>
#include <TEfficiency.h>
#include <TH1.h>
#include <TSpline.h>
#include <TH2.h>
#include <TMath.h>
#include <TLegend.h>
#include <TMarker.h>
#include <TRandom.h>
#include <TCut.h>
#include <TChain.h>
#include <TStyle.h>
#include <TPostScript.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TVector3.h>
#include <iomanip>
#include <vector>
#include <map>
#include <set>
#include <fstream>

using namespace std;

Int_t nMPPCs = 4092;

// Make array for chi sq error calculations

//Double_t modifiedPMvaluesO6[4092][101]{};
//Double_t modifiedChiSquaredO6[4092][101]{};
//Double_t modifiedPMvaluesO5[4092][101]{};
//Double_t modifiedChiSquaredO5[4092][101]{};
//Double_t modifiedPMvaluesO4[4092][101]{};
//Double_t modifiedChiSquaredO4[4092][101]{};
//Double_t modifiedPMvaluesO3[4092][101]{};
//Double_t modifiedChiSquaredO3[4092][101]{};
Double_t modifiedPMvaluesO2[4092][101]{};
Double_t modifiedChiSquaredO2[4092][101]{};
Double_t modifiedPMvalues[4092][101]{};
Double_t modifiedChiSquared[4092][101]{};

Double_t modifiedPMvaluesToFindOrder[4092][6][2]{};
Double_t modifiedChiSquaredToFindOrder[4092][6][2]{};
Int_t    orderToSearchForError[4092][2]{};

Double_t measuredRatios[4092][169]{};
Double_t inverseOfMeasuredRatios[4092][169]{};
Double_t gainFitParameters[4092]{};
Double_t gainFitParameterWeights[4092]{};
Double_t measuredRMSerrors[4092][169]{};
Double_t measuredNentries[4092][169]{};
Int_t    measuredVsWhichPM[4092][169]{};


Double_t gradientValue[4092]{};
Double_t gradientNorm = 0;
Double_t chiSquaredValues[4092]{};
Double_t totalChiSq=0;
Double_t chiSquaredNDF[4092]{};
Double_t totalChiSqNDF=0;

Double_t initialLearningRate = 1e-12;
Double_t initialGradientNorm = 1e9;

Double_t calcGradientContribution(double ratioij, double ratioji, double gaini, double gainj, double errorij, double errorji) {
   
   Double_t dChiSq_dgaini_jcontribution   =  (-(ratioij - (gaini/gainj))/pow(errorij,2)) +
                                             ((gainj/pow(gaini,2))*(ratioji - (gainj/gaini))/pow(errorji,2));
   return   dChiSq_dgaini_jcontribution;
}

Double_t calcGradientContributionIJ(double ratioij, double gaini, double gainj, double errorij) {
   
   Double_t dChiSq_dgaini   =  -(ratioij - (gaini/gainj))/(pow(errorij,2));
   return   dChiSq_dgaini;
}

Double_t calcGradientContributionJI(double ratioji, double gaini, double gainj, double errorji) {
   
   Double_t dChiSq_dgaini   =  ((gainj/(pow(gaini,2)))*(ratioji - (gainj/gaini))/(pow(errorji,2)));
   return   dChiSq_dgaini;
}

Double_t calcChiSq(double ratioij, double gaini, double gainj, double errorij)   {
   Double_t ChiValue = (ratioij - (gaini/gainj))/errorij;
   Double_t ChiSqValue = 0.5 * pow(ChiValue,2);
   return ChiSqValue;
}

void clearFitParameterErrorArrays(){
   
   for (int iPM=0;iPM<nMPPCs;iPM++) {
      for (int iStep=0;iStep<101;iStep++) {
         modifiedPMvalues[iPM][iStep]  = 0;
         modifiedPMvaluesO2[iPM][iStep]= 0;
         modifiedChiSquared[iPM][iStep]   = 0;
         modifiedChiSquaredO2[iPM][iStep] = 0;
      }
   }
   return;
}

void fillModifiedGains()   {
   Double_t maxLogRatio = TMath::Log(1.2);
   Float_t nsteps = 100;
   Double_t stepsize = maxLogRatio/nsteps; // log(1)=0, so no subtraction required
   
   for (int iStep=0;iStep<nsteps;iStep++){
      Double_t step_ratio = TMath::Exp(stepsize*(iStep+1));
      for (int iPM=0;iPM<nMPPCs;iPM++){
         modifiedPMvalues[iPM][iStep] = gainFitParameters[iPM]*step_ratio;
         modifiedPMvaluesO2[iPM][iStep]= gainFitParameters[iPM]/step_ratio;
      }
   }
   return;
}

void gradDescent()  {
   
   //-- input file with results of regression algorithm
   TFile* infile = new TFile("optPropCorr-out-tg-w2.root","READ");
   auto regressionTree = infile->Get<TTree>("res");
   
   //-- get relevant info from regression tree
   Int_t nRegressionIterations = regressionTree->GetEntries();
   Double_t setGains[nMPPCs],
            setCEs[nMPPCs],
            setQEs[nMPPCs],
            setCTAPs[nMPPCs],
            setWDGains[nMPPCs],
            algCorrections[nMPPCs],
            algReducedChiSq,
            algReducedChiSqByPM[nMPPCs],
            algNDFByPM[nMPPCs];
   Bool_t   isGoodPM[nMPPCs];
   
   regressionTree->SetBranchAddress("corr",algCorrections);
   regressionTree->SetBranchAddress("truegain",setGains);
   regressionTree->SetBranchAddress("truece",setCEs);
   regressionTree->SetBranchAddress("trueqe",setQEs);
   regressionTree->SetBranchAddress("truectap",setCTAPs);
   regressionTree->SetBranchAddress("truewdgain",setWDGains);
   regressionTree->SetBranchAddress("redXsq",&algReducedChiSq);
   regressionTree->SetBranchAddress("rXpm",algReducedChiSqByPM);
   regressionTree->SetBranchAddress("ndfpm",algNDFByPM);
   regressionTree->SetBranchAddress("chgood",isGoodPM);
   
   regressionTree->GetEntry(0);
   
   
   //-- loop over pm trees
   for (Int_t iPM=0;iPM<nMPPCs;iPM++){
      char pmTreeName[200];
      sprintf(pmTreeName,"pms%i",iPM);
      auto pmTree = infile->Get<TTree>(pmTreeName);
      gainFitParameters[iPM]=0;
      gainFitParameterWeights[iPM] = 0;
      if (!(pmTree->GetEntries()))  continue;
      //if (!pmHasData[iPM]) continue;
      Double_t pairMeans[nMPPCs],
               pairRMS[nMPPCs],
               pairEvents[nMPPCs];
      Bool_t   isPairUsed[nMPPCs];
      
      pmTree->SetBranchAddress("ratios",pairMeans);
      pmTree->SetBranchAddress("rms",pairRMS);
      pmTree->SetBranchAddress("nmeas",pairEvents);
      pmTree->SetBranchAddress("usepair",isPairUsed);
      pmTree->GetEntry(0);
      
      int indexInArrays=0;
      for (int jPM=0;jPM<nMPPCs;jPM++){
         if (pairMeans[jPM]!=0 && pairRMS[jPM]!=0 && isPairUsed[jPM]){
            
            measuredRatios[iPM][indexInArrays]  = pairMeans[jPM];
            measuredNentries[iPM][indexInArrays]= pairEvents[jPM];
            measuredRMSerrors[iPM][indexInArrays]= pairRMS[jPM]/sqrt(pairEvents[jPM]);
            measuredVsWhichPM[iPM][indexInArrays]= jPM;
            gainFitParameters[iPM]+= (pairMeans[jPM])*pairEvents[jPM]*pow(pairRMS[jPM],-2);
            gainFitParameterWeights[iPM]+= (pairEvents[jPM]*pow(pairRMS[jPM],-2));
            //gainFitParameters[iPM]+= (pairMeans[jPM]);//* pairEvents[jPM]/pow(pairRMS[jPM],2);
            //gainFitParameterWeights[iPM]+= 1;//pow(pairRMS[jPM],2)/pairEvents[jPM];
            
            //cout<<measuredRMSerrors[iPM][indexInArrays]<<endl;
            indexInArrays++;
         }
      }
      if (gainFitParameters[iPM]>1e-2) gainFitParameters[iPM]/=gainFitParameterWeights[iPM];
   }
   /*
   for (int iPM=0;iPM<nMPPCs;iPM++){
      for (int iInd=0;iInd<169;iInd++){
         if (measuredRatios[iPM][iInd]>0){
            cout<<measuredRatios[iPM][iInd]<<endl;
         }
      }
   }
    */
   //cout<<*measuredRatios<<endl;
   
   Int_t regIter = 0;
   Double_t learningRate = initialLearningRate;
   Int_t pmIndex[4092],pmRowNum[4092],pmColNum[4092];
   
   for (int iPM=0;iPM<4092;iPM++){
      pmIndex[iPM] = iPM;
      pmRowNum[iPM] = TMath::Floor(iPM/44);
      pmColNum[iPM] = iPM % 44;
   }
   
   TFile* fout = new TFile("calcGainAndErrByGradDescent100000.root","RECREATE");
   TTree* tree = new TTree("gradres","gradient descent results");
   
   tree->Branch("iter",&regIter,"iter/I");
   tree->Branch("lr",&learningRate,"lr/D");
   tree->Branch("gain",gainFitParameters,"gain[4092]/D");
   tree->Branch("truegain",setGains,"truegain[4092]/D");
   tree->Branch("truece",setCEs,"truece[4092]/D");
   tree->Branch("trueqe",setQEs,"trueqe[4092]/D");
   tree->Branch("truectap",setCTAPs,"truectap[4092]/D");
   tree->Branch("truewdgain",setWDGains,"truewdgain[4092]/D");
   tree->Branch("gradient",gradientValue,"gradient[4092]/D");
   tree->Branch("chisq",&totalChiSq,"chisq/D");
   tree->Branch("ndf",&totalChiSqNDF,"ndf/D");
   tree->Branch("pmchisq",chiSquaredValues,"pmchisq[4092]/D");
   tree->Branch("pmndf",chiSquaredNDF,"pmndf[4092]/D");
   tree->Branch("index",pmIndex,"index[4092]/I");
   tree->Branch("col",pmColNum,"col[4092]/I");
   tree->Branch("row",pmRowNum,"row[4092]/I");
   tree->Branch("gradnorm",&gradientNorm,"gradnorm/D");
   
   //tree->Fill();
   cout << "Starting Gradient Descent Algorithm" << endl;
   for (int iIter=0;iIter<100000;iIter++)  {
      if (iIter%1000==0)   cout << "On the " << iIter <<"th iteration" <<endl;
      
      regIter=iIter;
      totalChiSq = 0;
      totalChiSqNDF = 0;
      gradientNorm   =0;
      for (int iPM=0;iPM<nMPPCs;iPM++){
         gradientValue[iPM] = 0;
         chiSquaredNDF[iPM] = 0;
         chiSquaredValues[iPM] = 0;
      }
      
      for (int iPM=0;iPM<nMPPCs;iPM++){
         for (int iInd=0;iInd<169;iInd++){
            int jPM = (int) measuredVsWhichPM[iPM][iInd];
            
            if (jPM>0){
               //cout<<gainFitParameters[jPM]<<endl;
               if (measuredRatios[iPM][iInd]>0 && measuredRMSerrors[iPM][iInd]>0){
                  if (abs(gainFitParameters[iPM])>1e-3 && abs(gainFitParameters[jPM])>1e-3){
                     //cout<<"calculating a contribution"<<endl;
                     if (!iInd)  {
                        totalChiSqNDF--;
                        chiSquaredNDF[iPM]--;
                     }
                     gradientValue[iPM] += calcGradientContributionIJ(measuredRatios[iPM][iInd],  gainFitParameters[iPM], gainFitParameters[jPM], measuredRMSerrors[iPM][iInd]);
                     gradientValue[jPM] += calcGradientContributionJI(measuredRatios[iPM][iInd], gainFitParameters[jPM], gainFitParameters[iPM], measuredRMSerrors[iPM][iInd]);
                     chiSquaredValues[iPM] += calcChiSq(measuredRatios[iPM][iInd], gainFitParameters[iPM], gainFitParameters[jPM], measuredRMSerrors[iPM][iInd]);
                     chiSquaredValues[jPM] += calcChiSq(measuredRatios[iPM][iInd], gainFitParameters[iPM], gainFitParameters[jPM], measuredRMSerrors[iPM][iInd]);
                     totalChiSq  += calcChiSq(measuredRatios[iPM][iInd], gainFitParameters[iPM], gainFitParameters[jPM], measuredRMSerrors[iPM][iInd]);
                     chiSquaredNDF[iPM]++;
                     chiSquaredNDF[jPM]++;
                     totalChiSqNDF++;
                     //cout<<gradientValue[iPM]<<endl;
                  }
               }
            }
         }
      }
      
      for (int iPM=0;iPM<nMPPCs;iPM++)   {
         gradientNorm += pow(gradientValue[iPM],2);
      }
      gradientNorm = sqrt(gradientNorm);
      
      tree->Fill();
      
      for (int iPM=0;iPM<nMPPCs;iPM++) {
         if ( gainFitParameters[iPM] ) {
            gainFitParameters[iPM] -= gradientValue[iPM]*learningRate;
         }
      }
      
   }
   
   fout->Write();
   
   
   // calculate errors for the fit parameters
   
   Double_t maxLogRatio = TMath::Log(1.2);
   Float_t nsteps = 100;
   Double_t stepsize = maxLogRatio/nsteps; // log(1)=0, so no subtraction required
   
   /*
   for (int iStep=0;iStep<nsteps;iStep++){
      Double_t step_ratio = TMath::Exp(stepsize*(iStep+1));
      for (int iPM=0;iPM<nMPPCs;iPM++){
         modifiedPMvalues[iPM][iStep] = gainFitParameters[iPM]*step_ratio;
         modifiedPMvaluesO2[iPM][iStep]= gainFitParameters[iPM]/step_ratio;
      }
   }
   */
   clearFitParameterErrorArrays();
   fillModifiedGains();
   
   cout << "Done Initializing Arrays for Error Calculation" << endl;
   
   Double_t lowErrBound[4092]{};
   Double_t highErrBound[4092]{};
   
   for (int iPM=0;iPM<nMPPCs;iPM++){
      for (int iStep=0;iStep<nsteps;iStep++){
         for (int jPM=0;jPM<nMPPCs;jPM++){
            for (int iInd=0;iInd<169;iInd++){
               int kPM = (int) measuredVsWhichPM[jPM][iInd];
               if (kPM>0)  {
                  if (measuredRatios[jPM][iInd]>0 && measuredRMSerrors[jPM][iInd]>0
                      && abs(gainFitParameters[jPM])>1e-3 && abs(gainFitParameters[kPM])>1e-3){
                     if (!iStep && !iInd && (iPM%10 == 0)) {
                        cout << "Currently setting ChiSq values for PM " << iPM << endl;
                     }
                     if (iPM==jPM){
                        modifiedChiSquared[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], modifiedPMvalues[jPM][iStep], gainFitParameters[kPM], measuredRMSerrors[jPM][iInd]);
                        modifiedChiSquaredO2[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], modifiedPMvaluesO2[jPM][iStep], gainFitParameters[kPM], measuredRMSerrors[jPM][iInd]);
                     }
                     else if (iPM==kPM) {
                        modifiedChiSquared[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], gainFitParameters[jPM], modifiedPMvalues[kPM][iStep], measuredRMSerrors[jPM][iInd]);
                        modifiedChiSquaredO2[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], gainFitParameters[jPM], modifiedPMvaluesO2[kPM][iStep], measuredRMSerrors[jPM][iInd]);
                     }
                     else{
                        modifiedChiSquared[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], gainFitParameters[jPM], gainFitParameters[kPM], measuredRMSerrors[jPM][iInd]);
                        modifiedChiSquaredO2[iPM][iStep] += calcChiSq(measuredRatios[jPM][iInd], gainFitParameters[jPM], gainFitParameters[kPM], measuredRMSerrors[jPM][iInd]);
                     }
                  }
               }
            }
         }
      }
   }
   
   Double_t startingTotalChiSquared = totalChiSq;
   for (int iPM=0; iPM<nMPPCs;iPM++)   {
      if ((iPM%50 == 0)){
         cout << "Calculating Error for PM " << iPM << endl;
      }
      
      for (int iLowErr=0;iLowErr<100;iLowErr++) {
         if (modifiedChiSquaredO2[iPM][iLowErr]<0.1) continue;
         Double_t deltaChiSq = modifiedChiSquaredO2[iPM][iLowErr] - startingTotalChiSquared;
         if (deltaChiSq>1){
            lowErrBound[iPM] = abs(modifiedPMvaluesO2[iPM][iLowErr]-gainFitParameters[iPM]);
            break;
         }
      }
      
      for (int iHighErr=0;iHighErr<100;iHighErr++) {
         if (modifiedChiSquared[iPM][iHighErr]<0.1) continue;
         Double_t deltaChiSq = modifiedChiSquared[iPM][iHighErr] - startingTotalChiSquared;
         if (deltaChiSq>1){
            highErrBound[iPM] = abs(modifiedPMvalues[iPM][iHighErr]-gainFitParameters[iPM]);
            break;
         }
      }
      
   }
   
   
   TTree* errtree = new TTree("errors","errors");
   
   errtree->Branch("gain",gainFitParameters,"gain[4092]/D");
   errtree->Branch("errorlow",lowErrBound,"errorlow[4092]/D");
   errtree->Branch("errorhigh",highErrBound,"errorhigh[4092]/D");
   
   errtree->Fill();
   
   fout->Write();
}
   
   
   
   /*
   
   Bool_t pmHasData[nMPPCs];  // indicates which pms have data to plot
   
   regressionTree->GetEntry(nRegressionIterations-1);
   Double_t sumOfSetParams=0,
            nPMsInSetParams=0;
   Double_t totalSetCorrectionParameter;
   for (int iPM=0;iPM<nMPPCs;iPM++) {
      pmHasData[iPM] = kFALSE;
      if ((algCorrections[iPM]<=0)||(setQEs[iPM]<0.001)||(!isGoodPM[iPM]))   continue;
      pmHasData[iPM] = kTRUE;
      totalSetCorrectionParameter = setGains[iPM]*setQEs[iPM]*setCEs[iPM]*setCTAPs[iPM]*setWDGains[iPM];
      sumOfSetParams += totalSetCorrectionParameter;
      nPMsInSetParams++;
   }
   Double_t averageOfSetParams = sumOfSetParams/nPMsInSetParams;
   
   //-- declare histograms relevant to regression results
   
   TObjArray hlistRegression(0);
   TObjArray hlistRegressionByPM(0);
   
   TH1F* reducedChiSqEvolution = new TH1F("reducedChiSqvsIteration", "Reduced #chi^{2}/NDF vs Iteration;Iteration;#chi^{2}/NDF", nRegressionIterations,0,nRegressionIterations);
   hlistRegression.Add(reducedChiSqEvolution);
   
   
   
   TH1F* evolutionOfPMCorrections[4092];
   
   for (Int_t iPM=0;iPM<nMPPCs;iPM++)  {
      char  hName[200],
            hTitle[200];
      sprintf(hName,"accuracyPM%i",iPM);
      sprintf(hTitle,"Algorithm Correction / Existing Correction for PM %i; Iteration; Alg Param/Existing Param",iPM);
      evolutionOfPMCorrections[iPM] = new TH1F(hName,hTitle,nRegressionIterations,0,nRegressionIterations);
      if (pmHasData[iPM])  {
         hlistRegressionByPM.Add(evolutionOfPMCorrections[iPM]);
      }
   }
   
   //-- loop over regression tree
   Double_t pmCorrectionVsExisting;
   for (Int_t iIter=0;iIter<nRegressionIterations;iIter++)  {
      regressionTree->GetEntry(iIter);
      reducedChiSqEvolution->Fill(iIter,algReducedChiSq);
      for (Int_t iPM=0;iPM<nMPPCs;iPM++)  {
         if (pmHasData[iPM]){
            pmCorrectionVsExisting = algCorrections[iPM]/((setGains[iPM]*setQEs[iPM]*setCTAPs[iPM]*setCEs[iPM]*setWDGains[iPM])/averageOfSetParams);
            evolutionOfPMCorrections[iPM]->Fill(iIter,pmCorrectionVsExisting);
         }
      }
   }
   
   TH1D* hAlgAcc = new TH1D("hAlgAcc","hAlgAcc;Alg Param/Existing Param;Entries",100,.5,1.5);
   TH2D* hAlgAccVsPosition = new TH2D("AlgAcc2d","AlgAcc2d;Z Column;#phi Row",44,0,44,93,0,93);
   TH2D* hNDFdistribution = new TH2D("NDFdist","NDFdist;Zcolumn;#phi Row",44,0,44,93,0,93);
   TH2D* hAlgAccVsZ = new TH2D("AlgAccvsZ","AlgAccvsZ;Z Column;Alg Param/Existing Param",44,0,44,100,.5,1.5);
   TH2D* hAlgAccVsPhi = new TH2D("AlgAccvsPhi","AlgAccvsPhi;#phi Row;Alg Param/Existing Param",93,0,93,100,.5,1.5);
   TH2D* hAlgResVsPosition = new TH2D("AlgParam2d","AlgParam2d;Z Column;#phi Row",44,0,44,93,0,93);
   hlistRegression.Add(hAlgAcc);
   hlistRegression.Add(hAlgAccVsPosition);
   hlistRegression.Add(hNDFdistribution);
   hlistRegression.Add(hAlgAccVsZ);
   hlistRegression.Add(hAlgAccVsPhi);
   hlistRegression.Add(hAlgResVsPosition);
   
   for (Int_t iPM=0;iPM<nMPPCs;iPM++){
      regressionTree->GetEntry(nRegressionIterations-1);
      Int_t PMColumn = iPM % 44;
      Int_t PMRow = TMath::Floor(iPM/44);
      if (pmHasData[iPM])  {
         // sets up varied values of gains for various orders of magnitude in testing error of fit params
         for (int iGain=0;iGain<101;iGain++){
            double gainMultiplier = 1+ ((iGain-50)*2.e-2);
            modifiedPMvalues[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
            gainMultiplier = 1+ ((iGain-50)*2.e-3);
            modifiedPMvaluesO2[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
            gainMultiplier = 1+ ((iGain-50)*2.e-4);
            modifiedPMvaluesO3[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
            gainMultiplier = 1+ ((iGain-50)*2.e-5);
            modifiedPMvaluesO4[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
            gainMultiplier = 1+ ((iGain-50)*2.e-6);
            modifiedPMvaluesO5[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
            gainMultiplier = 1+ ((iGain-50)*2.e-7);
            modifiedPMvaluesO6[iPM][iGain] = algCorrections[iPM]*gainMultiplier;
         }
         if (algNDFByPM[iPM]>0){
            pmCorrectionVsExisting = algCorrections[iPM]/((setGains[iPM]*setQEs[iPM]*setCTAPs[iPM]*setCEs[iPM]*setWDGains[iPM])/averageOfSetParams);
            hAlgAcc->Fill(pmCorrectionVsExisting);
            hAlgAccVsPosition->Fill(PMColumn,PMRow,pmCorrectionVsExisting);
            hNDFdistribution->Fill(PMColumn,PMRow,algNDFByPM[iPM]);
            hAlgAccVsZ->Fill(PMColumn,pmCorrectionVsExisting);
            hAlgAccVsPhi->Fill(PMRow,pmCorrectionVsExisting);
         }
         hAlgResVsPosition->Fill(PMColumn,PMRow,algCorrections[iPM]);
         
      }
   }
   
   //-- declare histograms relevant to individual MPPC pair based measurement trees
   
   TObjArray hlistPM(0);
   
   TH2D *eventsInPair[4092];
   TH2D *sigmaOfPair[4092];
   for (Int_t iPM=0;iPM<nMPPCs;iPM++)  {
      char  hName[200],
            hTitle[200];
      sprintf(hName,"nEventsPM%i",iPM);
      sprintf(hTitle,"nEventsPM%i;Z Column;#phi Row",iPM);
      Int_t PMColumn = iPM % 44;
      Int_t PMRow = TMath::Floor(iPM/44);
      eventsInPair[iPM] = new TH2D(hName,hTitle,13,PMColumn-6.5,PMColumn+6.5,13,PMRow-6.5,PMRow+6.5);
      sprintf(hName,"sigmaPM%i",iPM);
      sprintf(hTitle,"sigmaPM%i;Z Column;#phi Row",iPM);
      sigmaOfPair[iPM] = new TH2D(hName,hTitle,13,PMColumn-6.5,PMColumn+6.5,13,PMRow-6.5,PMRow+6.5);
      if (pmHasData[iPM]){
         hlistPM.Add(eventsInPair[iPM]);
         hlistPM.Add(sigmaOfPair[iPM]);
      }
   }
   
   
   //-- loop over pm trees
   for (Int_t iPM=0;iPM<nMPPCs;iPM++){
      char pmTreeName[200];
      sprintf(pmTreeName,"pms%i",iPM);
      auto pmTree = infile->Get<TTree>(pmTreeName);
      if (!(pmTree->GetEntries()))  continue;
      if (!pmHasData[iPM]) continue;
      Double_t pairMeans[nMPPCs],
               pairRMS[nMPPCs],
               pairEvents[nMPPCs];
      Bool_t   isPairUsed[nMPPCs];
      
      pmTree->SetBranchAddress("ratios",pairMeans);
      pmTree->SetBranchAddress("rms",pairRMS);
      pmTree->SetBranchAddress("nmeas",pairEvents);
      pmTree->SetBranchAddress("usepair",isPairUsed);
      pmTree->GetEntry(0);
      
      Double_t pairPercentUncertainty;
      
      for (Int_t jPM=0;jPM<nMPPCs;jPM++)  {
         if (!pmHasData[jPM]) continue;
         Int_t jPMColumn = jPM % 44;
         Int_t jPMRow    = TMath::Floor(jPM/44);
         if (!isPairUsed[jPM] || !pairEvents[jPM] || !pairMeans[jPM] || !pairRMS[jPM]) continue;
         
         pairPercentUncertainty = pairRMS[jPM]/pairMeans[jPM];
         
         sigmaOfPair[iPM]->Fill(jPMColumn,jPMRow,pairPercentUncertainty);
         eventsInPair[iPM]->Fill(jPMColumn,jPMRow,pairEvents[jPM]);
         
         //chisq = pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
         
         
         for (int kPM=0;kPM<4092;kPM++){
            if (!pmHasData[kPM]) continue;
            for (int iGain=0;iGain<101;iGain++){
               if (iPM==kPM){
                  modifiedChiSquared[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvalues[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO2[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvaluesO2[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO3[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvaluesO3[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO4[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvaluesO4[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO5[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvaluesO5[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO6[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(modifiedPMvaluesO6[iPM][iGain]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
               }
               else if (jPM==kPM){
                  modifiedChiSquared[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvalues[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO2[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvaluesO2[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO3[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvaluesO3[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO4[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvaluesO4[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO5[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvaluesO5[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO6[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/modifiedPMvaluesO6[jPM][iGain]), 2) / pow(pairRMS[jPM], 2);
               }
               else {
                  modifiedChiSquared[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO2[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO3[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO4[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO5[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
                  modifiedChiSquaredO6[kPM][iGain] += pairEvents[jPM] * pow(pairMeans[jPM]-(algCorrections[iPM]/algCorrections[jPM]), 2) / pow(pairRMS[jPM], 2);
               }
            }
         }
         
      }
      
   }
   
   Double_t lowerErrorBound[4092]{};
   Double_t upperErrorBound[4092]{};
   Double_t averageError[4092]{};
   
   for (int iPM=0;iPM<nMPPCs;iPM++) {
      //Bool_t
      Double_t centerXsq = modifiedChiSquared[iPM][50];
      if (!modifiedChiSquared[iPM][50])   continue;
      
      //Find order to test,
      
      
      for (int iError=0;iError<50;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (!lowerErrorBound[iPM]){
            lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
            break;
         }
      }
      if (!lowerErrorBound[iPM]){
         for (int iError=0;iError<50;iError++){
            if (!modifiedChiSquaredO2[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO2[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM]){
               lowerErrorBound[iPM] = (modifiedPMvaluesO2[iPM][iError]);
               break;
            }
         }
      }
      if (!lowerErrorBound[iPM]){
         for (int iError=0;iError<50;iError++){
            if (!modifiedChiSquaredO3[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO3[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM]){
               lowerErrorBound[iPM] = (modifiedPMvaluesO3[iPM][iError]);
               break;
            }
         }
      }
      if (!lowerErrorBound[iPM]){
         for (int iError=0;iError<50;iError++){
            if (!modifiedChiSquaredO4[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO4[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM]){
               lowerErrorBound[iPM] = (modifiedPMvaluesO4[iPM][iError]);
               break;
            }
         }
      }
      if (!lowerErrorBound[iPM]){
         for (int iError=0;iError<50;iError++){
            if (!modifiedChiSquaredO5[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO5[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM]){
               lowerErrorBound[iPM] = (modifiedPMvaluesO5[iPM][iError]);
               break;
            }
         }
      }
      if (!lowerErrorBound[iPM]){
         for (int iError=0;iError<50;iError++){
            if (!modifiedChiSquaredO6[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO6[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM]){
               lowerErrorBound[iPM] = (modifiedPMvaluesO6[iPM][iError]);
               break;
            }
         }
      }
      
      // upper error limit
      
      for (int iError=100;iError>50;iError--){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (!upperErrorBound[iPM]){
            upperErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
            break;
         }
      }
      if (!upperErrorBound[iPM]){
         for (int iError=100;iError>50;iError--){
            if (!modifiedChiSquaredO2[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO2[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!upperErrorBound[iPM]){
               upperErrorBound[iPM] = (modifiedPMvaluesO2[iPM][iError]);
               break;
            }
         }
      }
      if (!upperErrorBound[iPM]){
         for (int iError=100;iError>50;iError--){
            if (!modifiedChiSquaredO3[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO3[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!upperErrorBound[iPM]){
               upperErrorBound[iPM] = (modifiedPMvaluesO3[iPM][iError]);
               break;
            }
         }
      }
      if (!upperErrorBound[iPM]){
         for (int iError=100;iError>50;iError--){
            if (!modifiedChiSquaredO4[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO4[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!upperErrorBound[iPM]){
               upperErrorBound[iPM] = (modifiedPMvaluesO4[iPM][iError]);
               break;
            }
         }
      }
      if (!upperErrorBound[iPM]){
         for (int iError=100;iError>50;iError--){
            if (!modifiedChiSquaredO5[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO5[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!upperErrorBound[iPM]){
               upperErrorBound[iPM] = (modifiedPMvaluesO5[iPM][iError]);
               break;
            }
         }
      }
      if (!upperErrorBound[iPM]){
         for (int iError=100;iError>50;iError--){
            if (!modifiedChiSquaredO6[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO6[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!upperErrorBound[iPM]){
               upperErrorBound[iPM] = (modifiedPMvaluesO6[iPM][iError]);
               break;
            }
         }
      }
    */
      /*
      for (int iError=0;iError<101;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (!lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
         upperErrorBound[iPM] = modifiedPMvalues[iPM][iError];
         if (iError==100)   upperErrorBound[iPM] =0;
      }
      if (!(lowerErrorBound[iPM])||(!upperErrorBound[iPM])){
         for (int iError=0;iError<101;iError++){
            if (!modifiedChiSquaredO2[iPM][iError])  continue;
            Double_t deltaChiSquared = modifiedChiSquaredO2[iPM][iError]-centerXsq;
            if (deltaChiSquared>1)  continue;
            if (!lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvaluesO2[iPM][iError]);
            if (upperErrorBound[iPM]>modifiedPMvaluesO2[iPM][iError])  upperErrorBound[iPM] = modifiedPMvaluesO2[iPM][iError];
            if (iError==100)   upperErrorBound[iPM] =0;
         }
      }
      for (int iError=0;iError<101;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
         upperErrorBound[iPM] = modifiedPMvalues[iPM][iError];
         if (iError==100)   upperErrorBound[iPM] =0;
      }
      for (int iError=0;iError<101;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
         upperErrorBound[iPM] = modifiedPMvalues[iPM][iError];
         if (iError==100)   upperErrorBound[iPM] =0;
      }
      for (int iError=0;iError<101;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
         upperErrorBound[iPM] = modifiedPMvalues[iPM][iError];
         if (iError==100)   upperErrorBound[iPM] =0;
      }
      for (int iError=0;iError<101;iError++){
         if (!modifiedChiSquared[iPM][iError])  continue;
         Double_t deltaChiSquared = modifiedChiSquared[iPM][iError]-centerXsq;
         if (deltaChiSquared>1)  continue;
         if (lowerErrorBound[iPM])  lowerErrorBound[iPM] = (modifiedPMvalues[iPM][iError]);
         upperErrorBound[iPM] = modifiedPMvalues[iPM][iError];
         if (iError==100)   upperErrorBound[iPM] =0;
      }
      */
       /*
      if (upperErrorBound[iPM] && lowerErrorBound[iPM]){
         averageError[iPM] = (upperErrorBound[iPM]-lowerErrorBound[iPM])*.5;
      }
      else if (upperErrorBound[iPM]){
         averageError[iPM] = (upperErrorBound[iPM]-modifiedPMvalues[iPM][50]);
      }
      else if (lowerErrorBound[iPM]){
         averageError[iPM] = (modifiedPMvalues[iPM][50]-lowerErrorBound[iPM]);
      }
   }
   
   
   //-- Create the output root file
   TFile f("algorithmDiagnostics.root","RECREATE");
   hlistRegressionByPM.Write();
   hlistPM.Write();
   hlistRegression.Write();
   
   TTree* terrout = new TTree("gainerror","gain uncertainty");
   terrout->Branch("gain",algCorrections,"gain[4092]/D");
   terrout->Branch("err",averageError, "err[4092]/D");
   terrout->Fill();
   f.cd();
   terrout->Write();
   f.Close();
   
}
*/
