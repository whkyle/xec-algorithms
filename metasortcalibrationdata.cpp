//
//  metasortcalibrationdata.cpp
//  
//
//  Created by Will Kyle on 11/2/21.
//

#include <stdio.h>
#include "TROOT.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TString.h"
#include "TClonesArray.h"
#include "TVector3.h"
#include "Riostream.h"
#include "RConfig.h"
#ifndef __CINT__
#include "ROMETreeInfo.h"
#endif
#include "CalibrationFunctions.h"
#include "../common/include/xec/xectools.h"
#include "include/xec/PMSolidAngle.h"
#include "UCIPMdata.h"
#include "PMEventData.h"
#include "EventData.h"
#include "UCIXECSP.h"
#include "PMPlots.h"
#include "EventPlots.h"

Double_t SumOfWeights[4092][4092]{};
Double_t MeanRatio[4092][4092]{};
Int_t nRatioMeasurements[4092][4092]{};
Int_t indexOfOtherPM[4092][4092]{};
Double_t VarianceOfRatioDistribution[4092][4092]{};


void metasortcalibrationdata()   {
   
   Int_t runNumber = 406661;//396750;//396622;
   
   Int_t firstRun = 406661;//396750;//396622;
   Int_t lastRun = 406664;//396832;//396985;//396832;//396623;
   TChain* fChain = new TChain("tree","sorted data from rec files for UCI MPPC calibrations");
   TChain* fOtherChain = new TChain("tree","other sorted data from rec files for UCI MPPC calibrations");
   
   for (int iRun=firstRun;iRun<=lastRun;iRun++){
      fChain->Add(Form("/meg/data1/shared/subprojects/xec/Gammamc/ReducedRecData/calibrationData%06i.root",iRun));
      fOtherChain->Add(Form("/meg/data1/shared/subprojects/xec/Gammamc/ReducedRecData/calibrationData%06i.root",iRun));
   }
   /*
   EventPlots* diagnosticPlots = new EventPlots();
   diagnosticPlots->setofPMplots.resize(4092);
   
   for (int iPM=0;iPM<4092;iPM++){
      PMPlots* pm = new PMPlots(iPM);
      pm->InitializeIndividualPlots();
      pm->pairwiseRatioDistributions.resize(400);
      diagnosticPlots->setofPMplots[iPM] = pm;
   }
   */
   Int_t NinChain = fChain->GetEntries();
   
   UCIXECSP* Cuts = new UCIXECSP();
   //Cuts->SetFineSelectionCuts();
   Cuts->SetCourseSelectionCriteria();
   
   TTree* tree;
   Int_t numInChain=0;
   Int_t currenttree=-1;
   
   Int_t EventNumber;
   Int_t nPMsInEvent;
   Double_t FitUVW[3];
   Double_t FitXYZ[3];
   Double_t PositionReducedChiSq[2];
   Int_t  ProfitRange;
   Double_t EGamma;
   Double_t TimeFit;
   
   Double_t Npho;
   Double_t Nphe;
   Double_t Charge;
   Double_t PeakAmplitude;
   Double_t CFtime;
   Double_t PeakTime;
   Double_t LeadingEdgeTime;
   Double_t SolidAngle;
   Double_t IncidenceAngle;
   Double_t UDistance;
   Double_t VDistance;
   Double_t UVDistance;
   Double_t DistanceToEvent;
   Double_t PredictedEnergy;
   
   Int_t ChannelNumber;
   Int_t ProductionLot;
   Int_t RowInFace;
   Int_t NumberInRow;
   Double_t WDGain;
   Double_t Gain;
   Double_t QE;
   Double_t CE;
   Double_t CTAP;
   Double_t XYZPM[3];
   Double_t UVWPM[3];
   Double_t Direction[3];
   Double_t CableTimeOffset;
   
   TTree* othertree;
   Int_t othernumInChain=0;
   Int_t othercurrenttree=-1;
   
   Int_t otherrunNumber;
   Int_t otherEventNumber;
   Int_t othernPMsInEvent;
   Double_t otherFitUVW[3];
   Double_t otherFitXYZ[3];
   Double_t otherPositionReducedChiSq[2];
   Int_t  otherProfitRange;
   Double_t otherEGamma;
   Double_t otherTimeFit;
   
   Double_t otherNpho;
   Double_t otherNphe;
   Double_t otherCharge;
   Double_t otherPeakAmplitude;
   Double_t otherCFtime;
   Double_t otherPeakTime;
   Double_t otherLeadingEdgeTime;
   Double_t otherSolidAngle;
   Double_t otherIncidenceAngle;
   Double_t otherUDistance;
   Double_t otherVDistance;
   Double_t otherUVDistance;
   Double_t otherDistanceToEvent;
   Double_t otherPredictedEnergy;
   
   Int_t otherChannelNumber;
   Int_t otherProductionLot;
   Int_t otherRowInFace;
   Int_t otherNumberInRow;
   Double_t otherWDGain;
   Double_t otherGain;
   Double_t otherQE;
   Double_t otherCE;
   Double_t otherCTAP;
   Double_t otherXYZPM[3];
   Double_t otherUVWPM[3];
   Double_t otherDirection[3];
   Double_t otherCableTimeOffset;
   
   Int_t lastEntryForEvent = -1;
   Int_t runID = -1;
   
   Int_t numberOfPairwiseHists[4092]{};
   
   //PMEventData* currentPM = new PMEventData(0);
   cout << "Chain contains " << NinChain << " entries" << endl;
   
   
   for (int iChain=0;iChain<NinChain;iChain++){
      if (iChain%5000==0){
         cout << "Now analysing entry " << iChain << endl;
      }
      numInChain = fChain->LoadTree(iChain);
      if (fChain->GetTreeNumber() != currenttree){
         tree = fChain->GetTree();
         currenttree = fChain->GetTreeNumber();
         lastEntryForEvent = -1;
         
         tree->SetBranchAddress("runNumber",&runNumber);
         tree->SetBranchAddress("nPMs",&nPMsInEvent);
         tree->SetBranchAddress("event",&EventNumber);
         tree->SetBranchAddress("uvw",FitUVW);
         tree->SetBranchAddress("xyz",FitXYZ);
         tree->SetBranchAddress("reducedChiSq",PositionReducedChiSq);
         tree->SetBranchAddress("fitRange",&ProfitRange);
         tree->SetBranchAddress("EGamma",&EGamma);
         tree->SetBranchAddress("TimeFit",&TimeFit);
         
         tree->SetBranchAddress("Npho",&Npho);
         tree->SetBranchAddress("Nphe",&Nphe);
         tree->SetBranchAddress("Charge",&Charge);
         tree->SetBranchAddress("PeakAmplitude",&PeakAmplitude);
         tree->SetBranchAddress("CFtime",&CFtime);
         tree->SetBranchAddress("PeakTime",&PeakTime);
         tree->SetBranchAddress("LeadingEdgeTime",&LeadingEdgeTime);
         tree->SetBranchAddress("SolidAngle",&SolidAngle);
         tree->SetBranchAddress("IncidenceAngle",&IncidenceAngle);
         tree->SetBranchAddress("UDistance",&UDistance);
         tree->SetBranchAddress("VDistance",&VDistance);
         tree->SetBranchAddress("UVDistance",&UVDistance);
         tree->SetBranchAddress("DistanceToEvent",&DistanceToEvent);
         tree->SetBranchAddress("EGammaFromPM",&PredictedEnergy);
         
         tree->SetBranchAddress("Channel",&ChannelNumber);
         tree->SetBranchAddress("Lot",&ProductionLot);
         tree->SetBranchAddress("Row",&RowInFace);
         tree->SetBranchAddress("Column",&NumberInRow);
         tree->SetBranchAddress("WDGain",&WDGain);
         tree->SetBranchAddress("Gain",&Gain);
         tree->SetBranchAddress("QE",&QE);
         tree->SetBranchAddress("CE",&CE);
         tree->SetBranchAddress("CTAP",&CTAP);
         tree->SetBranchAddress("XYZPM",&XYZPM);
         tree->SetBranchAddress("UVWPM",&UVWPM);
         tree->SetBranchAddress("PMDirection",Direction);
         tree->SetBranchAddress("CableTimeOffset",&CableTimeOffset);
         
      }
      
      tree->GetEntry(numInChain);
      if (numInChain > lastEntryForEvent){
         lastEntryForEvent = numInChain + nPMsInEvent - 1;
      }
      
      
      if (EGamma<Cuts->fMinEGamma || EGamma>Cuts->fMaxEGamma)  continue;
      if (abs(FitUVW[0])>Cuts->fRangeInU || abs(FitUVW[1])>Cuts->fRangeInV)   continue;
      if (FitUVW[2]<Cuts->fMinW || FitUVW[2]>Cuts->fMaxW)   continue;
      if (PositionReducedChiSq[0]>Cuts->fMaxPoslfitReducedChiSq)  continue;
      if (PositionReducedChiSq[1]>Cuts->fMaxPoslfitReducedChiSq)  continue;
      
      if (SolidAngle<Cuts->fMinPMSolidAngle) continue;
      if (IncidenceAngle>Cuts->fMaxIncidenceAngleOnPM)   continue;
      
      for (int jChain=iChain+1;jChain<lastEntryForEvent;jChain++){
         othernumInChain = fOtherChain->LoadTree(jChain);
         if (fOtherChain->GetTreeNumber() != othercurrenttree){
            othertree = fOtherChain->GetTree();
            othercurrenttree = fOtherChain->GetTreeNumber();
            
            othertree->SetBranchAddress("runNumber",&otherrunNumber);
            othertree->SetBranchAddress("nPMs",&othernPMsInEvent);
            othertree->SetBranchAddress("event",&otherEventNumber);
            othertree->SetBranchAddress("uvw",otherFitUVW);
            othertree->SetBranchAddress("xyz",otherFitXYZ);
            othertree->SetBranchAddress("reducedChiSq",otherPositionReducedChiSq);
            othertree->SetBranchAddress("fitRange",&otherProfitRange);
            othertree->SetBranchAddress("EGamma",&otherEGamma);
            othertree->SetBranchAddress("TimeFit",&otherTimeFit);
            
            othertree->SetBranchAddress("Npho",&otherNpho);
            othertree->SetBranchAddress("Nphe",&otherNphe);
            othertree->SetBranchAddress("Charge",&otherCharge);
            othertree->SetBranchAddress("PeakAmplitude",&otherPeakAmplitude);
            othertree->SetBranchAddress("CFtime",&otherCFtime);
            othertree->SetBranchAddress("PeakTime",&otherPeakTime);
            othertree->SetBranchAddress("LeadingEdgeTime",&otherLeadingEdgeTime);
            othertree->SetBranchAddress("SolidAngle",&otherSolidAngle);
            othertree->SetBranchAddress("IncidenceAngle",&otherIncidenceAngle);
            othertree->SetBranchAddress("UDistance",&otherUDistance);
            othertree->SetBranchAddress("VDistance",&otherVDistance);
            othertree->SetBranchAddress("UVDistance",&otherUVDistance);
            othertree->SetBranchAddress("DistanceToEvent",&otherDistanceToEvent);
            othertree->SetBranchAddress("EGammaFromPM",&otherPredictedEnergy);
            
            othertree->SetBranchAddress("Channel",&otherChannelNumber);
            othertree->SetBranchAddress("Lot",&otherProductionLot);
            othertree->SetBranchAddress("Row",&otherRowInFace);
            othertree->SetBranchAddress("Column",&otherNumberInRow);
            othertree->SetBranchAddress("WDGain",&otherWDGain);
            othertree->SetBranchAddress("Gain",&otherGain);
            othertree->SetBranchAddress("QE",&otherQE);
            othertree->SetBranchAddress("CE",&otherCE);
            othertree->SetBranchAddress("CTAP",&otherCTAP);
            othertree->SetBranchAddress("XYZPM",&otherXYZPM);
            othertree->SetBranchAddress("UVWPM",&otherUVWPM);
            othertree->SetBranchAddress("PMDirection",otherDirection);
            othertree->SetBranchAddress("CableTimeOffset",&otherCableTimeOffset);
            
         }
         
         //if (othercurrenttree!=currenttree)  continue;
         
         othertree->GetEntry(othernumInChain);
         
         if (runNumber!=otherrunNumber || EventNumber!=otherEventNumber)   continue;
         
         if (otherSolidAngle<Cuts->fMinPMSolidAngle) continue;
         if (otherIncidenceAngle>Cuts->fMaxIncidenceAngleOnPM)   continue;
         Double_t solidAngleRatio = SolidAngle/otherSolidAngle;
         if (solidAngleRatio >Cuts->fMaxPMSolidAngleRatio)  continue;
         if (solidAngleRatio <Cuts->fMinPMSolidAngleRatio)  continue;
         if (sqrt(pow(UVWPM[0] -otherUVWPM[0],2)+pow(UVWPM[1] -otherUVWPM[1],2))>Cuts->fMaxPMSeperation)   continue;
         
         Double_t varianceWeightScale = (1./Nphe + 1./otherNphe);
         
         for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
            if (indexOfOtherPM[ChannelNumber][iRatioArrayIndex]==otherChannelNumber ||
                nRatioMeasurements[ChannelNumber][iRatioArrayIndex]==0){
               /*if (nRatioMeasurements[ChannelNumber][iRatioArrayIndex]==0){
                diagnosticPlots->setofPMplots[ChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex] = new TH1D(Form("Ratio%ito%i",ChannelNumber,otherChannelNumber),"Ratio Distribution;Pairwise Npho Ratio",200,0.5,1.5);
                numberOfPairwiseHists[ChannelNumber]++;
                }*/
               indexOfOtherPM[ChannelNumber][iRatioArrayIndex] = otherChannelNumber;
               Double_t NphoRatio = (Npho/otherNpho)/solidAngleRatio;
               Double_t varianceWeight = 1./(pow(NphoRatio,2)*varianceWeightScale);
               
               SumOfWeights[ChannelNumber][iRatioArrayIndex] += varianceWeight;
               //Double_t previousMean = MeanRatio[ChannelNumber][iRatioArrayIndex];
               MeanRatio[ChannelNumber][iRatioArrayIndex] += varianceWeight * NphoRatio; //;//(varianceWeight/SumOfWeights[ChannelNumber][iRatioArrayIndex]) *(NphoRatio-previousMean);
               nRatioMeasurements[ChannelNumber][iRatioArrayIndex]++;
               //VarianceOfRatioDistribution[ChannelNumber][iRatioArrayIndex] += varianceWeight* (NphoRatio - previousMean)*(NphoRatio - MeanRatio[ChannelNumber][iRatioArrayIndex]);
               break;
            }
         }
         for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
            if (indexOfOtherPM[otherChannelNumber][iRatioArrayIndex]==ChannelNumber ||
                nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]==0){
               /*if (nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]==0){
                diagnosticPlots->setofPMplots[otherChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex] = new TH1D(Form("Ratio%ito%i",otherChannelNumber,ChannelNumber),"Ratio Distribution;Pairwise Npho Ratio",200,0.5,1.5);
                numberOfPairwiseHists[otherChannelNumber]++;
                }*/
               indexOfOtherPM[otherChannelNumber][iRatioArrayIndex] = ChannelNumber;
               Double_t NphoRatio = (otherNpho/Npho)*solidAngleRatio;
               Double_t varianceWeight = 1./(pow(NphoRatio,2)*varianceWeightScale);
               
               SumOfWeights[otherChannelNumber][iRatioArrayIndex] += varianceWeight;
               //Double_t previousMean = MeanRatio[otherChannelNumber][iRatioArrayIndex];
               MeanRatio[otherChannelNumber][iRatioArrayIndex] += varianceWeight * NphoRatio ;// (varianceWeight/SumOfWeights[otherChannelNumber][iRatioArrayIndex])*( NphoRatio - previousMean);
               nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]++;
               //VarianceOfRatioDistribution[otherChannelNumber][iRatioArrayIndex] += varianceWeight* (NphoRatio - previousMean)*(NphoRatio - MeanRatio[otherChannelNumber][iRatioArrayIndex]);
               break;
            }
         }
         
      }
   }
   
   for (int iPM=0;iPM<4092;iPM++){
      for (int iIndex=0;iIndex<400;iIndex++){
         if (SumOfWeights[iPM][iIndex]>0){
            MeanRatio[iPM][iIndex] /= SumOfWeights[iPM][iIndex];
         }
      }
   }
   
   
   
   
   
   
   
   
   
   
   /*
   for (int meanOrVariance=0;meanOrVariance<2;meanOrVariance++){
      if (meanOrVariance){
         lastEntryForEvent = -1;
         runID = -1;
         for (int iPM=0;iPM<4092;iPM++){
            for (int iIndex=0;iIndex<400;iIndex++){
               if (SumOfWeights[iPM][iIndex]>0){
                  MeanRatio[iPM][iIndex] /= SumOfWeights[iPM][iIndex];
               }
            }
         }
      }
      for (int iChain=0;iChain<NinChain;iChain++){
         if (iChain%5000==0){
            cout << "Now analysing entry " << iChain << endl;
         }
         numInChain = fChain->LoadTree(iChain);
         if (fChain->GetTreeNumber() != currenttree){
            tree = fChain->GetTree();
            currenttree = fChain->GetTreeNumber();
            lastEntryForEvent = -1;
            
            tree->SetBranchAddress("runNumber",&runNumber);
            tree->SetBranchAddress("nPMs",&nPMsInEvent);
            tree->SetBranchAddress("event",&EventNumber);
            tree->SetBranchAddress("uvw",FitUVW);
            tree->SetBranchAddress("xyz",FitXYZ);
            tree->SetBranchAddress("reducedChiSq",PositionReducedChiSq);
            tree->SetBranchAddress("fitRange",&ProfitRange);
            tree->SetBranchAddress("EGamma",&EGamma);
            tree->SetBranchAddress("TimeFit",&TimeFit);
            
            tree->SetBranchAddress("Npho",&Npho);
            tree->SetBranchAddress("Nphe",&Nphe);
            tree->SetBranchAddress("Charge",&Charge);
            tree->SetBranchAddress("PeakAmplitude",&PeakAmplitude);
            tree->SetBranchAddress("CFtime",&CFtime);
            tree->SetBranchAddress("PeakTime",&PeakTime);
            tree->SetBranchAddress("LeadingEdgeTime",&LeadingEdgeTime);
            tree->SetBranchAddress("SolidAngle",&SolidAngle);
            tree->SetBranchAddress("IncidenceAngle",&IncidenceAngle);
            tree->SetBranchAddress("UDistance",&UDistance);
            tree->SetBranchAddress("VDistance",&VDistance);
            tree->SetBranchAddress("UVDistance",&UVDistance);
            tree->SetBranchAddress("DistanceToEvent",&DistanceToEvent);
            tree->SetBranchAddress("EGammaFromPM",&PredictedEnergy);
            
            tree->SetBranchAddress("Channel",&ChannelNumber);
            tree->SetBranchAddress("Lot",&ProductionLot);
            tree->SetBranchAddress("Row",&RowInFace);
            tree->SetBranchAddress("Column",&NumberInRow);
            tree->SetBranchAddress("WDGain",&WDGain);
            tree->SetBranchAddress("Gain",&Gain);
            tree->SetBranchAddress("QE",&QE);
            tree->SetBranchAddress("CE",&CE);
            tree->SetBranchAddress("CTAP",&CTAP);
            tree->SetBranchAddress("XYZPM",&XYZPM);
            tree->SetBranchAddress("UVWPM",&UVWPM);
            tree->SetBranchAddress("PMDirection",Direction);
            tree->SetBranchAddress("CableTimeOffset",&CableTimeOffset);
            
         }
         
         tree->GetEntry(numInChain);
         if (numInChain > lastEntryForEvent){
            lastEntryForEvent = numInChain + nPMsInEvent - 1;
         }
         
         
         if (EGamma<Cuts->fMinEGamma || EGamma>Cuts->fMaxEGamma)  continue;
         if (abs(FitUVW[0])>Cuts->fRangeInU || abs(FitUVW[1])>Cuts->fRangeInV)   continue;
         if (FitUVW[2]<Cuts->fMinW || FitUVW[2]>Cuts->fMaxW)   continue;
         if (PositionReducedChiSq[0]>Cuts->fMaxPoslfitReducedChiSq)  continue;
         if (PositionReducedChiSq[1]>Cuts->fMaxPoslfitReducedChiSq)  continue;
         
         if (SolidAngle<Cuts->fMinPMSolidAngle) continue;
         if (IncidenceAngle>Cuts->fMaxIncidenceAngleOnPM)   continue;
         
         // fill in diagnostic plots which only pertain to one PM
         if (meanOrVariance==0 && (EGamma/PredictedEnergy)>0 && (EGamma/PredictedEnergy)<5){
            diagnosticPlots->setofPMplots[ChannelNumber]->predictedEvsIncAngle->Fill(IncidenceAngle,EGamma/ PredictedEnergy);
            diagnosticPlots->setofPMplots[ChannelNumber]->predictedEvsSolidAngle->Fill(SolidAngle,EGamma/ PredictedEnergy);
            diagnosticPlots->setofPMplots[ChannelNumber]->predictedEvsDepth->Fill(FitUVW[2],EGamma/ PredictedEnergy);
            diagnosticPlots->setofPMplots[ChannelNumber]->predictedEvsU->Fill(FitUVW[0],EGamma/ PredictedEnergy);
            diagnosticPlots->setofPMplots[ChannelNumber]->predictedEvsV->Fill(FitUVW[1],EGamma/ PredictedEnergy);
            
            diagnosticPlots->setofPMplots[ChannelNumber]->hChargeAmplitude->Fill(Charge/PeakAmplitude);
            diagnosticPlots->setofPMplots[ChannelNumber]->hNpho->Fill(Npho);
            diagnosticPlots->setofPMplots[ChannelNumber]->hCFtimes->Fill(CFtime*1.e9);
         }
         
         for (int jChain=iChain+1;jChain<lastEntryForEvent;jChain++){
            othernumInChain = fOtherChain->LoadTree(jChain);
            if (fOtherChain->GetTreeNumber() != othercurrenttree){
               othertree = fOtherChain->GetTree();
               othercurrenttree = fOtherChain->GetTreeNumber();
               
               othertree->SetBranchAddress("runNumber",&otherrunNumber);
               othertree->SetBranchAddress("nPMs",&othernPMsInEvent);
               othertree->SetBranchAddress("event",&otherEventNumber);
               othertree->SetBranchAddress("uvw",otherFitUVW);
               othertree->SetBranchAddress("xyz",otherFitXYZ);
               othertree->SetBranchAddress("reducedChiSq",otherPositionReducedChiSq);
               othertree->SetBranchAddress("fitRange",&otherProfitRange);
               othertree->SetBranchAddress("EGamma",&otherEGamma);
               othertree->SetBranchAddress("TimeFit",&otherTimeFit);
               
               othertree->SetBranchAddress("Npho",&otherNpho);
               othertree->SetBranchAddress("Nphe",&otherNphe);
               othertree->SetBranchAddress("Charge",&otherCharge);
               othertree->SetBranchAddress("PeakAmplitude",&otherPeakAmplitude);
               othertree->SetBranchAddress("CFtime",&otherCFtime);
               othertree->SetBranchAddress("PeakTime",&otherPeakTime);
               othertree->SetBranchAddress("LeadingEdgeTime",&otherLeadingEdgeTime);
               othertree->SetBranchAddress("SolidAngle",&otherSolidAngle);
               othertree->SetBranchAddress("IncidenceAngle",&otherIncidenceAngle);
               othertree->SetBranchAddress("UDistance",&otherUDistance);
               othertree->SetBranchAddress("VDistance",&otherVDistance);
               othertree->SetBranchAddress("UVDistance",&otherUVDistance);
               othertree->SetBranchAddress("DistanceToEvent",&otherDistanceToEvent);
               othertree->SetBranchAddress("EGammaFromPM",&otherPredictedEnergy);
               
               othertree->SetBranchAddress("Channel",&otherChannelNumber);
               othertree->SetBranchAddress("Lot",&otherProductionLot);
               othertree->SetBranchAddress("Row",&otherRowInFace);
               othertree->SetBranchAddress("Column",&otherNumberInRow);
               othertree->SetBranchAddress("WDGain",&otherWDGain);
               othertree->SetBranchAddress("Gain",&otherGain);
               othertree->SetBranchAddress("QE",&otherQE);
               othertree->SetBranchAddress("CE",&otherCE);
               othertree->SetBranchAddress("CTAP",&otherCTAP);
               othertree->SetBranchAddress("XYZPM",&otherXYZPM);
               othertree->SetBranchAddress("UVWPM",&otherUVWPM);
               othertree->SetBranchAddress("PMDirection",otherDirection);
               othertree->SetBranchAddress("CableTimeOffset",&otherCableTimeOffset);
               
            }
            
            othertree->GetEntry(othernumInChain);
            
            if (runNumber!=otherrunNumber || EventNumber!=otherEventNumber)   continue;
            
            if (otherSolidAngle<Cuts->fMinPMSolidAngle) continue;
            if (otherIncidenceAngle>Cuts->fMaxIncidenceAngleOnPM)   continue;
            Double_t solidAngleRatio = SolidAngle/otherSolidAngle;
            if (solidAngleRatio >Cuts->fMaxPMSolidAngleRatio)  continue;
            if (solidAngleRatio <Cuts->fMinPMSolidAngleRatio)  continue;
            if (sqrt(pow(UVWPM[0] -otherUVWPM[0],2)+pow(UVWPM[1] -otherUVWPM[1],2))>Cuts->fMaxPMSeperation)   continue;
            
            Double_t varianceWeight = Nphe + otherNphe;
            if (!meanOrVariance){
               for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
                  if (indexOfOtherPM[ChannelNumber][iRatioArrayIndex]==otherChannelNumber ||
                      nRatioMeasurements[ChannelNumber][iRatioArrayIndex]==0){
                     if (nRatioMeasurements[ChannelNumber][iRatioArrayIndex]==0){
                        diagnosticPlots->setofPMplots[ChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex] = new TH1D(Form("Ratio%ito%i",ChannelNumber,otherChannelNumber),"Ratio Distribution;Pairwise Npho Ratio",200,0.5,1.5);
                        numberOfPairwiseHists[ChannelNumber]++;
                     }
                     indexOfOtherPM[ChannelNumber][iRatioArrayIndex] = otherChannelNumber;
                     Double_t NphoRatio = (Npho/otherNpho)/solidAngleRatio;
                     diagnosticPlots->setofPMplots[ChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex]->Fill(NphoRatio,varianceWeight);
                     SumOfWeights[ChannelNumber][iRatioArrayIndex] += varianceWeight;
                     //Double_t previousMean = MeanRatio[ChannelNumber][iRatioArrayIndex];
                     MeanRatio[ChannelNumber][iRatioArrayIndex] += varianceWeight * NphoRatio; //;//(varianceWeight/SumOfWeights[ChannelNumber][iRatioArrayIndex]) *(NphoRatio-previousMean);
                     nRatioMeasurements[ChannelNumber][iRatioArrayIndex]++;
                     //VarianceOfRatioDistribution[ChannelNumber][iRatioArrayIndex] += varianceWeight* (NphoRatio - previousMean)*(NphoRatio - MeanRatio[ChannelNumber][iRatioArrayIndex]);
                     break;
                  }
               }
               for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
                  if (indexOfOtherPM[otherChannelNumber][iRatioArrayIndex]==ChannelNumber ||
                      nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]==0){
                     if (nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]==0){
                        diagnosticPlots->setofPMplots[otherChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex] = new TH1D(Form("Ratio%ito%i",otherChannelNumber,ChannelNumber),"Ratio Distribution;Pairwise Npho Ratio",200,0.5,1.5);
                        numberOfPairwiseHists[otherChannelNumber]++;
                     }
                     indexOfOtherPM[otherChannelNumber][iRatioArrayIndex] = ChannelNumber;
                     Double_t NphoRatio = (otherNpho/Npho)*solidAngleRatio;
                     diagnosticPlots->setofPMplots[otherChannelNumber]->pairwiseRatioDistributions[iRatioArrayIndex]->Fill(NphoRatio,varianceWeight);
                     SumOfWeights[otherChannelNumber][iRatioArrayIndex] += varianceWeight;
                     //Double_t previousMean = MeanRatio[otherChannelNumber][iRatioArrayIndex];
                     MeanRatio[otherChannelNumber][iRatioArrayIndex] += varianceWeight * NphoRatio ;// (varianceWeight/SumOfWeights[otherChannelNumber][iRatioArrayIndex])*( NphoRatio - previousMean);
                     nRatioMeasurements[otherChannelNumber][iRatioArrayIndex]++;
                     //VarianceOfRatioDistribution[otherChannelNumber][iRatioArrayIndex] += varianceWeight* (NphoRatio - previousMean)*(NphoRatio - MeanRatio[otherChannelNumber][iRatioArrayIndex]);
                     break;
                  }
               }
            }
            else{
               for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
                  if (indexOfOtherPM[ChannelNumber][iRatioArrayIndex]==otherChannelNumber){
                     Double_t NphoRatio = Npho/otherNpho;
                     VarianceOfRatioDistribution[ChannelNumber][iRatioArrayIndex] += varianceWeight * pow(((NphoRatio/solidAngleRatio) - MeanRatio[ChannelNumber][iRatioArrayIndex]),2);
                     break;
                  }
               }
               for (int iRatioArrayIndex=0;iRatioArrayIndex<400;iRatioArrayIndex++){
                  if (indexOfOtherPM[otherChannelNumber][iRatioArrayIndex]==ChannelNumber){
                     Double_t NphoRatio = otherNpho/Npho;
                     VarianceOfRatioDistribution[otherChannelNumber][iRatioArrayIndex] += varianceWeight * pow(((NphoRatio*solidAngleRatio) - MeanRatio[otherChannelNumber][iRatioArrayIndex]),2);
                     break;
                  }
               }
            }
         }
         
         
      }
   }
   
   for (int iPM=0;iPM<4092;iPM++){
      //diagnosticPlots->setofPMplots[iPM]->pairwiseRatioDistributions.resize(numberOfPairwiseHists[iPM]);
      for (int iIndex=0;iIndex<400;iIndex++){
         if (SumOfWeights[iPM][iIndex]>0){
            VarianceOfRatioDistribution[iPM][iIndex] /= SumOfWeights[iPM][iIndex];
         }
      }
   }
   */
   
   TFile* fout = new TFile("metaSortedCalibrationData.root","RECREATE");
   TTree* pairwiseTree = new TTree("pairs","pairwise measurements for calibration");
   
   Int_t numeratorPM,denominatorPM;
   Int_t NumeratorPMRow, NumeratorPMColumn;
   Double_t PairwiseMean, PairwiseVariance;
   Int_t PairwiseNmeasurements;
   
   pairwiseTree->Branch("numeratorIndex",&numeratorPM,"numeratorIndex/I");
   pairwiseTree->Branch("denominatorIndex",&denominatorPM,"denominatorIndex/I");
   pairwiseTree->Branch("numeratorRow",&NumeratorPMRow,"numeratorRow/I");
   pairwiseTree->Branch("numeratorColumn",&NumeratorPMColumn,"numeratorColumn/I");
   pairwiseTree->Branch("pairwiseMean",&PairwiseMean,"pairwiseMean/D");
   pairwiseTree->Branch("pairwiseVariance",&PairwiseVariance,"pairwiseVariance/D");
   pairwiseTree->Branch("pairwiseN",&PairwiseNmeasurements,"pairwiseN/I");
   
   for (int iPM=0;iPM<4092;iPM++){
      for (int indexInRatioArray=0;indexInRatioArray<400;indexInRatioArray++){
         if (!nRatioMeasurements[iPM][indexInRatioArray])   continue;
         
         numeratorPM = iPM;
         denominatorPM = indexOfOtherPM[iPM][indexInRatioArray];
         NumeratorPMRow= iPM%44;
         NumeratorPMColumn = TMath::Floor(iPM/44);
         PairwiseMean = MeanRatio[iPM][indexInRatioArray];
         PairwiseVariance = 0;//VarianceOfRatioDistribution[iPM][indexInRatioArray]/SumOfWeights[iPM][indexInRatioArray];
         PairwiseNmeasurements = (Int_t) nRatioMeasurements[iPM][indexInRatioArray];
         pairwiseTree->Fill();
      }
   }
   
   fout->cd();
   pairwiseTree->Write();
   fout->Close();
   /*
   TFile* hIndividualPlotsFile = new TFile("metaSortPMDiagnostics.root","RECREATE");
   TFile* hRatioPlotsFile = new TFile("metaSortRatioPlots.root","RECREATE");
   
   hIndividualPlotsFile->cd();
   for (auto pm : diagnosticPlots->setofPMplots){
      pm->SaveIndividualPlots();
   }
   hIndividualPlotsFile->Close();
   hRatioPlotsFile->cd();
   for (auto pm : diagnosticPlots->setofPMplots){
      pm->SavePairwisePlots();
   }
   hRatioPlotsFile->Close();
    */
   return;
}

