//
//  PMPlots.h
//  
//
//  Created by Will Kyle on 12/8/21.
//

#ifndef PMPlots_h
#define PMPlots_h

class PMPlots
{
public:
   Int_t fChannelNumber;
   Int_t fRow;
   Int_t fColumn;
   
   TProfile* predictedEvsIncAngle;
   TProfile* predictedEvsSolidAngle;
   TProfile* predictedEvsDepth;
   TProfile* predictedEvsU;
   TProfile* predictedEvsV;
   
   TH1D* hChargeAmplitude;
   TH1D* hNpho;
   TH1D* hCFtimes;
   
   std::vector<TH1D*> pairwiseRatioDistributions;
   
   PMPlots(){};
   PMPlots(Int_t channel){
      fChannelNumber=channel;
      fRow = channel%44;
      fColumn = channel/44;
   };
   
   void InitializeIndividualPlots(){
      predictedEvsIncAngle = new TProfile(Form("%ipmEpredictionVsIncAngle",fChannelNumber),"E Gamma From PM Measurement Vs Incidence Angle; Incidence Angle [rad]; E_{Reco}/E_{PM}",100,0,TMath::Pi()/2.);
      predictedEvsSolidAngle = new TProfile(Form("%ipmEpredictionVsSolidAngle",fChannelNumber),"E Gamma From PM Measurement Vs Solid Angle; Solid Angle [1/4#pi ster]; E_{Reco}/E_{PM}",500,0,5e-3);
      predictedEvsDepth = new TProfile(Form("%ipmEpredictionVsDepth",fChannelNumber),"E Gamma From PM Measurement Vs Depth;Depth [cm];E_{PM}/E_{Reco}",30,0,15);
      predictedEvsU = new TProfile(Form("%ipmEpredictionVsU",fChannelNumber),"E Gamma From PM Measurement Vs U;U [cm];E_{Reco}/E_{PM}",70,-35,35);
      predictedEvsV = new TProfile(Form("%ipmEpredictionVsV",fChannelNumber),"E Gamma From PM Measurement Vs V;V [cm];E_{Reco}/E_{PM}",150,-75,75);
      
      hChargeAmplitude = new TH1D(Form("%iqaRatio",fChannelNumber),"Ratio of Charge to Peak Amplitude; Charge/Amplitude [pC/V]",120,0,30);
      hNpho = new TH1D(Form("%iNpho",fChannelNumber),"Distribution of Photons in PM; N_{#gamma}",200,0,2000);
      hCFtimes = new TH1D(Form("%icftime",fChannelNumber),"Distribution of CF Times in PM; CF Time [ns]",150,-800,-500);
   };
   
   void SaveIndividualPlots(){
      predictedEvsIncAngle->Write();
      predictedEvsSolidAngle->Write();
      predictedEvsDepth->Write();
      predictedEvsU->Write();
      predictedEvsV->Write();
      
      hChargeAmplitude->Write();
      hNpho->Write();
      hCFtimes->Write();
      
   };
   
   void SavePairwisePlots(){
      for (auto pmpair : pairwiseRatioDistributions){
         pmpair->Write();
      }
   };
   
};

#endif /* PMPlots_h */
