//
//  UCIPMCalibration.h
//  
//
//  Created by Will Kyle on 11/12/21.
//

#ifndef UCIPMCalibration_h
#define UCIPMCalibration_h

class UCIPMCalibration{
public:
   Int_t fChannelNumber;
   Int_t fRow;
   Int_t fColumn;
   Double_t fRelativeResponse;
   Double_t fUpdateToRelativeResponse;
   Double_t fChiSqContribution;
   Int_t fNumberOfPairwiseMeasurements;
   Double_t fSumWeightForCorrection;
   Int_t fPMNDF;
   Double_t fLocalCorrectionMeasuredAgainst;
   Double_t fSumWeightForLocalCorr;
   Double_t fRelativeResponseUncertainty;
   Double_t fRelativeResponseUncertaintyWeight;
   
   // For Gradient Descent or Backtracking Line Search
   Double_t fStepSize;
   Double_t fGradient;
   
   
   std::vector<Int_t> ChannelsMeasuredAgainst;
   std::vector<Double_t> PairwiseMeans;
   std::vector<Double_t> PairwiseVariances;
   std::vector<Int_t> NinPairwiseMeasurement;
   
   UCIPMCalibration(){};
   UCIPMCalibration(Int_t channel){
      fChannelNumber=channel;
      fRow = channel/44;
      fColumn = channel%44;
      fRelativeResponse=0;
      fUpdateToRelativeResponse=0;
      fChiSqContribution=0;
      fNumberOfPairwiseMeasurements=0;
      fSumWeightForCorrection=0;
      fPMNDF = 0;
      fLocalCorrectionMeasuredAgainst = 0;
      fSumWeightForLocalCorr = 0;
      
      fStepSize=0;
      fGradient=0;
   };
   
   void FillPairwiseArrays(Int_t otherPMinPair, Double_t Mean, Double_t Variance, Int_t Nmeas){
      ChannelsMeasuredAgainst.push_back(otherPMinPair);
      PairwiseMeans.push_back(Mean);
      PairwiseVariances.push_back(Variance);
      NinPairwiseMeasurement.push_back(Nmeas);
      fNumberOfPairwiseMeasurements++;
   };
   
};

#endif /* UCIPMCalibration_h */
