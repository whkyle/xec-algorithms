//
//  UCIPMdata.h
//  
//
//  Created by Will Kyle on 10/15/21.
//

#ifndef UCIPMdata_h
#define UCIPMdata_h

class UCIPMdata
{
public:
   Int_t fIsSiPM;
   Int_t fIsBad;
   Int_t fIsNoisy;
   Int_t fChannelNumber;
   Int_t fProductionLot;
   Int_t fRowInFace;
   Int_t fNumberInRow;
   Double_t fWDGain;
   Double_t fGain;
   Double_t fQE;
   Double_t fCE;
   Double_t fCTAP;
   TVector3 fXYZ;
   TVector3 fUVW;
   TVector3 fDirection;
   Double_t fCableTimeOffset;
   Int_t fFace;
   
   UCIPMdata(){};
   UCIPMdata(Int_t channel){fChannelNumber = channel;};
   void LoadFromPMRH(MEGXECPMRunHeader* pmrh);
};

void UCIPMdata::LoadFromPMRH(MEGXECPMRunHeader* pmrh){
   fIsBad            = pmrh->GetIsBad();
   fIsSiPM           = pmrh->GetIsSiPM();
   fIsNoisy          = pmrh->GetIsNoisy();
   fProductionLot    = pmrh->GetProductionLot()-(int)'A';
   fRowInFace        = pmrh->GetRowInFace();
   fNumberInRow      = pmrh->GetNumberInRow();
   fWDGain           = pmrh->GetWDGain();
   fGain             = pmrh->GetGain();
   fQE               = pmrh->GetQE();
   fCE               = pmrh->GetCE();
   fCTAP             = pmrh->GetCTAP();
   fCableTimeOffset  = pmrh->GetCableTimeOffset();
   fFace             = pmrh->GetFace();
   for (Int_t ix=0; ix<3; ix++){
      fXYZ[ix] =pmrh->GetXYZAt(ix);
      fUVW[ix] =pmrh->GetUVWAt(ix);
      fDirection[ix]=pmrh->GetDirectionAt(ix);
   }
}









#endif /* UCIPMdata_h */
